package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Mass;
import edu.ntnu.idatt210602.matsvinnbackend.repo.FridgeRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.IngredientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class FridgeServiceTest {

    @Mock
    FridgeRepository fridgeRepository;

    @Mock
    IngredientRepository ingredientRepository;

    @InjectMocks
    FridgeService fridgeService;

    Account account;
    Fridge fridge;
    Item item;

    @BeforeEach
    void setup() {
        account = new Account("peter","pete@gmail.com","duck");
        fridge = new Fridge();
        fridge.setId(1);
        account.setFridge(fridge);

        item = new Item("banyanBox","1",null,7,new Amount(200, Mass.GRAM).serialize(),null,null);
        item.setId(1);
    }

    @Test
    public void reserveIngredientsWorks() {

        double amountToReserve = 400;
        Ingredient fridgeIngredient = new Ingredient(item, new Amount(500, Mass.GRAM).serialize());
        fridgeIngredient.setStatus(Ingredient.Status.FREE);
        Ingredient remainderIngredient = new Ingredient(item, new Amount(100, Mass.GRAM).serialize());
        remainderIngredient.setStatus(Ingredient.Status.FREE);
        account.getFridge().addIngredient(fridgeIngredient);

        when(ingredientRepository.save(any())).thenReturn(remainderIngredient);
        fridgeService.reserveIngredients(item,amountToReserve,account);
        assertEquals(2, account.getFridge().getIngredientList().size());
        assertEquals(1, account.getFridge().getFreeIngredients().size());
    }

    @Test
    public void reserveFullIngredientWorks() {

        double amountToReserve = 400;
        Ingredient fridgeIngredient = new Ingredient(item, new Amount(400, Mass.GRAM).serialize());
        fridgeIngredient.setStatus(Ingredient.Status.FREE);
        account.getFridge().addIngredient(fridgeIngredient);
        fridgeService.reserveIngredients(item,amountToReserve,account);
        assertEquals(1, account.getFridge().getIngredientList().size());
        assertEquals(0, account.getFridge().getFreeIngredients().size());
    }

    @Test
    public void FreeAllIngredientsWorks() {

        when(fridgeRepository.findById(any())).thenReturn(Optional.of(fridge));
        Ingredient fridgeIngredient = new Ingredient(item, new Amount(400, Mass.GRAM).serialize());
        Ingredient fridgeIngredient2 = new Ingredient(item, new Amount(600, Mass.GRAM).serialize());
        Ingredient fridgeIngredient3 = new Ingredient(item, new Amount(100, Mass.GRAM).serialize());
        fridgeIngredient.setIngredient_id(1);
        fridgeIngredient2.setIngredient_id(2);
        fridgeIngredient3.setIngredient_id(3);
        fridgeIngredient.setStatus(Ingredient.Status.RESERVED);
        fridgeIngredient2.setStatus(Ingredient.Status.RESERVED);
        fridgeIngredient3.setStatus(Ingredient.Status.FREE);
        Date date = new Date();
        fridgeIngredient.setExp_date(date);
        fridgeIngredient2.setExp_date(date);
        fridgeIngredient3.setExp_date(date);
        account.getFridge().addIngredient(fridgeIngredient);
        account.getFridge().addIngredient(fridgeIngredient2);
        account.getFridge().addIngredient(fridgeIngredient3);
        assertEquals(3, account.getFridge().getIngredientList().size());
        fridgeService.freeAllIngredients(account);
        assertEquals(1100, account.getFridge().getIngredientList().get(0).getAmount().getQuantity());
        assertEquals(1, account.getFridge().getIngredientList().size());
        assertEquals(Ingredient.Status.FREE,account.getFridge().getIngredientList().get(0).getStatus());
    }

    @Test
    public void combineFreeIngredientsWorks() {

        Ingredient fridgeIngredient = new Ingredient(item, new Amount(400, Mass.GRAM).serialize());
        Ingredient fridgeIngredient2 = new Ingredient(item, new Amount(600, Mass.GRAM).serialize());
        fridgeIngredient.setIngredient_id(1);
        fridgeIngredient2.setIngredient_id(2);
        fridgeIngredient.setStatus(Ingredient.Status.FREE);
        fridgeIngredient2.setStatus(Ingredient.Status.FREE);
        Date date = new Date();
        fridgeIngredient.setExp_date(date);
        fridgeIngredient2.setExp_date(date);
        account.getFridge().addIngredient(fridgeIngredient);
        account.getFridge().addIngredient(fridgeIngredient2);
        assertEquals(2, account.getFridge().getIngredientList().size());
        fridgeService.combineFreeIngredients(account);
        assertEquals(1000, account.getFridge().getIngredientList().get(0).getAmount().getQuantity());
        assertEquals(1, account.getFridge().getIngredientList().size());
    }

    @Test
    public void combineReservedIngredientsWorks() {

        Ingredient fridgeIngredient = new Ingredient(item, new Amount(400, Mass.GRAM).serialize());
        Ingredient fridgeIngredient2 = new Ingredient(item, new Amount(600, Mass.GRAM).serialize());
        fridgeIngredient.setIngredient_id(1);
        fridgeIngredient2.setIngredient_id(2);
        fridgeIngredient.setStatus(Ingredient.Status.RESERVED);
        fridgeIngredient2.setStatus(Ingredient.Status.RESERVED);
        Date date = new Date();
        fridgeIngredient.setExp_date(date);
        fridgeIngredient2.setExp_date(date);
        account.getFridge().addIngredient(fridgeIngredient);
        account.getFridge().addIngredient(fridgeIngredient2);
        assertEquals(2, account.getFridge().getIngredientList().size());
        fridgeService.combineReservedIngredients(account);
        assertEquals(1000, account.getFridge().getIngredientList().get(0).getAmount().getQuantity());
        assertEquals(1, account.getFridge().getIngredientList().size());
    }

}
