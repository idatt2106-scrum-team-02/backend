package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Count;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Mass;
import edu.ntnu.idatt210602.matsvinnbackend.repo.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ShoppingListServiceTest {

    @Mock
    FridgeRepository fridgeRepository;

    @Mock
    FridgeService fridgeService;

    @Mock
    ShoppingListRepository shoppingListRepository;

    @Mock
    IngredientRepository ingredientRepository;

    @InjectMocks
    ShoppingListService shoppingListService;

    @Mock
    ItemRepository itemRepository;

    @Mock
    RecipeRepository recipeRepository;

    Account account;
    Item item;
    ShoppingList shoppingList;
    Item item2;
    Fridge fridge;

    @BeforeEach
    void setup() {
        account = new Account("peter","pete@gmail.com","duck");
        shoppingList = new ShoppingList(1, new HashSet<>(),new HashSet<>());
        account.setShoppingList(shoppingList);
        fridge = new Fridge();
        fridge.setId(1);
        account.setFridge(fridge);

        item = new Item("banyanBox","1",null,7,new Amount(200, Mass.GRAM).serialize(),null,null);
        item.setId(1);
        item2 = new Item("banyan2","2",null,7,new Amount(2, Count.COUNT).serialize(),null,null);
        item2.setId(2);
    }

    @Test
    public void getShoppingListWorks() {

        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        assertEquals(shoppingList, shoppingListService.getShoppingList(account));
    }

    @Test
    public void addItemWithCorrectIdAndAccount() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        shoppingListService.addItem(1,5, account);
        assertEquals(1, shoppingList.getIngredientList().size());
    }

    @Test
    public void addSuggestionWithCorrectIdAndAccount() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        assertEquals(0, shoppingList.getSuggestionList().size());
        assertDoesNotThrow(() -> shoppingListService.addSuggestion(1,5, account));
        assertEquals(1, shoppingList.getSuggestionList().size());
    }

    @Test
    public void addItemWithIncorrectItemId() {
        when(itemRepository.findById(eq(2))).thenReturn(Optional.empty());
        when(shoppingListRepository.findById(eq(1))).thenReturn(Optional.of(shoppingList));
        assertThrows(IllegalArgumentException.class, () -> shoppingListService.addItem(2,5, account));
        assertEquals(0, shoppingList.getIngredientList().size());
    }

    @Test
    public void addSuggestionWithIncorrectItemId() {
        when(itemRepository.findById(eq(2))).thenReturn(Optional.empty());
        when(shoppingListRepository.findById(eq(1))).thenReturn(Optional.of(shoppingList));
        assertThrows(IllegalArgumentException.class, () -> shoppingListService.addSuggestion(2,5, account));
        assertEquals(0, shoppingList.getSuggestionList().size());
    }

    @Test
    public void removeItemWithCorrectIdWorks() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(5,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        shoppingListService.addItem(1,5, account);
        assertTrue(shoppingList.getIngredientList().stream().findFirst().isPresent());
        Ingredient ingredient1 = shoppingList.getIngredientList().stream().findFirst().get();
        ingredient1.setIngredient_id(1);
        when(ingredientRepository.findById(eq(1))).thenReturn(Optional.of(ingredient1));
        assertEquals(1, shoppingList.getIngredientList().size());
        assertTrue(shoppingList.IsIngredientInList(1));
        shoppingListService.removeItem(1,account);
        assertEquals(0, shoppingList.getIngredientList().size());
    }

    @Test
    public void removeItemWithIncorrectIdFails() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(5,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        shoppingListService.addItem(1,5, account);
        assertTrue(shoppingList.getIngredientList().stream().findFirst().isPresent());
        Ingredient ingredient1 = shoppingList.getIngredientList().stream().findFirst().get();
        ingredient1.setIngredient_id(1);
        when(ingredientRepository.findById(eq(2))).thenReturn(Optional.empty());
        assertEquals(1, shoppingList.getIngredientList().size());
        assertTrue(shoppingList.IsIngredientInList(1));
        assertThrows(IllegalArgumentException.class,()->shoppingListService.removeItem(2,account));
        assertEquals(1, shoppingList.getIngredientList().size());
    }

    @Test
    public void removeSuggestionWithCorrectIdWorks() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(5,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        shoppingListService.addSuggestion(1,5, account);
        assertTrue(shoppingList.getSuggestionList().stream().findFirst().isPresent());
        Ingredient ingredient1 = shoppingList.getSuggestionList().stream().findFirst().get();
        ingredient1.setIngredient_id(1);
        when(ingredientRepository.findById(eq(1))).thenReturn(Optional.of(ingredient1));
        assertEquals(1, shoppingList.getSuggestionList().size());
        assertTrue(shoppingList.IsIngredientInSuggestions(1));
        shoppingListService.removeSuggestion(1,account);
        assertEquals(0, shoppingList.getSuggestionList().size());
    }

    @Test
    public void removeSuggestionWithIncorrectIdFails() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(5,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        shoppingListService.addSuggestion(1,5, account);
        assertTrue(shoppingList.getSuggestionList().stream().findFirst().isPresent());
        Ingredient ingredient1 = shoppingList.getSuggestionList().stream().findFirst().get();
        ingredient1.setIngredient_id(1);
        when(ingredientRepository.findById(eq(2))).thenReturn(Optional.empty());
        assertEquals(1, shoppingList.getSuggestionList().size());
        assertTrue(shoppingList.IsIngredientInSuggestions(1));
        assertThrows(IllegalArgumentException.class,()->shoppingListService.removeSuggestion(2,account));
        assertEquals(1, shoppingList.getSuggestionList().size());
    }

    @Test
    public void acceptSuggestionWithCorrectIdSucceeds() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(5,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        shoppingListService.addSuggestion(1,5, account);
        assertTrue(shoppingList.getSuggestionList().stream().findFirst().isPresent());
        Ingredient ingredient1 = shoppingList.getSuggestionList().stream().findFirst().get();
        ingredient1.setIngredient_id(1);
        when(ingredientRepository.findById(eq(1))).thenReturn(Optional.of(ingredient1));
        assertEquals(1, shoppingList.getSuggestionList().size());
        assertTrue(shoppingList.IsIngredientInSuggestions(1));
        shoppingListService.acceptSuggestion(1,account);
        assertEquals(0, shoppingList.getSuggestionList().size());
        assertEquals(1, shoppingList.getIngredientList().size());
        assertTrue(shoppingList.IsIngredientInList(1));
    }

    @Test
    public void acceptSuggestionWithIncorrectIdFails() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(5,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        shoppingListService.addSuggestion(1,5, account);
        assertTrue(shoppingList.getSuggestionList().stream().findFirst().isPresent());
        Ingredient ingredient1 = shoppingList.getSuggestionList().stream().findFirst().get();
        ingredient1.setIngredient_id(1);
        when(ingredientRepository.findById(eq(2))).thenReturn(Optional.empty());
        assertEquals(1, shoppingList.getSuggestionList().size());
        assertTrue(shoppingList.IsIngredientInSuggestions(1));
        assertThrows(IllegalArgumentException.class,()->shoppingListService.acceptSuggestion(2,account));
        assertEquals(1, shoppingList.getSuggestionList().size());
        assertEquals(0, shoppingList.getIngredientList().size());
    }

    @Test
    public void changeItemAmountWithCorrectIdSucceeds() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(3,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        shoppingListService.addItem(1,3, account);
        assertTrue(shoppingList.getIngredientList().stream().findFirst().isPresent());
        Ingredient ingredient1 = shoppingList.getIngredientList().stream().findFirst().get();
        ingredient1.setIngredient_id(1);
        when(ingredientRepository.findById(eq(1))).thenReturn(Optional.of(ingredient1));
        assertEquals(1, shoppingList.getIngredientList().size());
        assertTrue(shoppingList.IsIngredientInList(1));
        assertEquals(3, shoppingList.getIngredientFromList(1).getAmount().getQuantity());
        shoppingListService.changeItemAmount(1, 5, account);
        assertEquals(1, shoppingList.getIngredientList().size());
        assertTrue(shoppingList.IsIngredientInList(1));
        assertEquals(5, shoppingList.getIngredientFromList(1).getAmount().getQuantity());
        shoppingListService.changeItemAmount(1, 0, account);
        assertEquals(0, shoppingList.getIngredientList().size());
        assertThrows(IllegalArgumentException.class, ()-> shoppingListService.changeItemAmount(1,-1,account));
    }

    @Test
    public void changeItemAmountWithIncorrectIdFails() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(3,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        shoppingListService.addItem(1,3, account);
        assertTrue(shoppingList.getIngredientList().stream().findFirst().isPresent());
        Ingredient ingredient1 = shoppingList.getIngredientList().stream().findFirst().get();
        ingredient1.setIngredient_id(1);
        when(ingredientRepository.findById(eq(2))).thenReturn(Optional.empty());
        assertEquals(1, shoppingList.getIngredientList().size());
        assertTrue(shoppingList.IsIngredientInList(1));
        assertEquals(3, shoppingList.getIngredientFromList(1).getAmount().getQuantity());
        assertThrows(IllegalArgumentException.class, ()-> shoppingListService.changeItemAmount(2, 5, account));
        assertEquals(1, shoppingList.getIngredientList().size());
        assertTrue(shoppingList.IsIngredientInList(1));
        assertEquals(3, shoppingList.getIngredientFromList(1).getAmount().getQuantity());
        assertThrows(IllegalArgumentException.class, ()-> shoppingListService.changeItemAmount(2, 0, account));
        assertEquals(1, shoppingList.getIngredientList().size());
        assertEquals(3, shoppingList.getIngredientFromList(1).getAmount().getQuantity());
    }

    @Test
    public void changeSuggestionAmountWithCorrectIdSucceeds() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(3,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        shoppingListService.addSuggestion(1,3, account);
        assertTrue(shoppingList.getSuggestionList().stream().findFirst().isPresent());
        Ingredient ingredient1 = shoppingList.getSuggestionList().stream().findFirst().get();
        ingredient1.setIngredient_id(1);
        when(ingredientRepository.findById(eq(1))).thenReturn(Optional.of(ingredient1));
        assertEquals(1, shoppingList.getSuggestionList().size());
        assertTrue(shoppingList.IsIngredientInSuggestions(1));
        assertEquals(3, shoppingList.getSuggestionFromList(1).getAmount().getQuantity());
        shoppingListService.changeSuggestionAmount(1, 5, account);
        assertEquals(1, shoppingList.getSuggestionList().size());
        assertTrue(shoppingList.IsIngredientInSuggestions(1));
        assertEquals(5, shoppingList.getSuggestionFromList(1).getAmount().getQuantity());
        shoppingListService.changeSuggestionAmount(1, 0, account);
        assertEquals(0, shoppingList.getSuggestionList().size());
        assertThrows(IllegalArgumentException.class, ()-> shoppingListService.changeSuggestionAmount(1,-1,account));
    }

    @Test
    public void changeSuggestionAmountWithInCorrectIdFails() {
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(3,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        shoppingListService.addSuggestion(1,3, account);
        assertTrue(shoppingList.getSuggestionList().stream().findFirst().isPresent());
        Ingredient ingredient1 = shoppingList.getSuggestionList().stream().findFirst().get();
        ingredient1.setIngredient_id(1);
        when(ingredientRepository.findById(eq(2))).thenReturn(Optional.empty());
        assertEquals(1, shoppingList.getSuggestionList().size());
        assertTrue(shoppingList.IsIngredientInSuggestions(1));
        assertEquals(3, shoppingList.getSuggestionFromList(1).getAmount().getQuantity());
        assertThrows(IllegalArgumentException.class, ()-> shoppingListService.changeSuggestionAmount(2, 5, account));
        assertEquals(1, shoppingList.getSuggestionList().size());
        assertTrue(shoppingList.IsIngredientInSuggestions(1));
        assertEquals(3, shoppingList.getSuggestionFromList(1).getAmount().getQuantity());
        assertThrows(IllegalArgumentException.class, ()-> shoppingListService.changeSuggestionAmount(2, 0, account));
        assertEquals(1, shoppingList.getSuggestionList().size());
        assertEquals(3, shoppingList.getSuggestionFromList(1).getAmount().getQuantity());
    }

    @Test
    public void buyAllWorks() {
        //add items to shopping list
        when(itemRepository.findById(eq(1))).thenReturn(Optional.of(item));
        when(shoppingListRepository.findById(anyInt())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        Ingredient ingredient = new Ingredient(item,new Amount(3,Count.COUNT).serialize());
        ingredient.setIngredient_id(1);
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(ingredient);
        when(ingredientRepository.findById(eq(1))).thenReturn(Optional.of(ingredient));
        when(ingredientRepository.existsById(eq(1))).thenReturn(true);
        shoppingListService.addItem(1,3, account);

        List<Ingredient> ingredientList = new ArrayList<>();
        ingredientList.add(ingredient);

        when(fridgeRepository.findById(anyInt())).thenReturn(Optional.of(account.getFridge()));
        when(fridgeService.addIngredients(any(Account.class),anyList())).thenReturn(ingredientList);
        assertEquals(1,shoppingList.getIngredientList().size());

        shoppingListService.buyAll(ingredientList,account);

        assertEquals(0,shoppingList.getIngredientList().size());
    }

    @Test
    public void addAllItemsFromRecipe() {
        Ingredient newIngredient = new Ingredient(item,new Amount(4,Count.COUNT).serialize());
        newIngredient.setIngredient_id(1);

        Ingredient recipeIngredient = new Ingredient(item, new Amount(800, Mass.GRAM).serialize());
        Ingredient fridgeIngredient = new Ingredient(item, new Amount(300, Mass.GRAM).serialize());
        fridgeIngredient.setStatus(Ingredient.Status.FREE);

        List<Ingredient> recipeIngredientList = new ArrayList<>();
        recipeIngredientList.add(recipeIngredient);
        account.getFridge().addIngredient(fridgeIngredient);
        Recipe recipe = new Recipe("kak","big cake","20", recipeIngredientList,"Bake the cake");
        when(recipeRepository.findById(eq(1))).thenReturn(Optional.of(recipe));
        when(shoppingListRepository.findById(any())).thenReturn(Optional.of(shoppingList));
        when(ingredientRepository.save(any(Ingredient.class))).thenReturn(newIngredient);
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        assertEquals(300,shoppingListService.getAmountOfItemFromFridge(item,account));
        shoppingListService.addAllItemsFromRecipe(1,account, 1);
        assertEquals(1, shoppingList.getIngredientList().size());
        Optional<Ingredient> ingredientSaved = shoppingList.getIngredientList().stream().findAny();
        assertTrue(ingredientSaved.isPresent());
        assertEquals(3, ingredientSaved.get().getAmount().getQuantity());
    }

    @Test
    public void addAllItemsFromRecipeWhenIngredientsForRecipeAlreadyInFridge() {
        List<Ingredient> recipeIngredientList = new ArrayList<>();

        Ingredient fridgeIngredient = new Ingredient(item, new Amount(800, Mass.GRAM).serialize());
        fridgeIngredient.setStatus(Ingredient.Status.FREE);

        recipeIngredientList.add(new Ingredient(item, new Amount(800, Mass.GRAM).serialize()));
        account.getFridge().addIngredient(fridgeIngredient);
        Recipe recipe = new Recipe("kak","big cake","20", recipeIngredientList,"Bake the cake");
        when(recipeRepository.findById(eq(1))).thenReturn(Optional.of(recipe));
        when(shoppingListRepository.findById(any())).thenReturn(Optional.of(shoppingList));
        when(shoppingListRepository.save(any(ShoppingList.class))).thenReturn(shoppingList);
        shoppingListService.addAllItemsFromRecipe(1,account, 1);
        assertEquals(0, shoppingList.getIngredientList().size());
    }

    @Test
    public void removeAllRequestedWorks() {
        when(shoppingListRepository.findById(any())).thenReturn(Optional.of(shoppingList));

        Ingredient shopIngredient = new Ingredient(item,new Amount(3,Count.COUNT).serialize());
        Ingredient shopIngredient2 = new Ingredient(item,new Amount(4,Count.COUNT).serialize());
        shopIngredient.setIngredient_id(1);
        shopIngredient2.setIngredient_id(2);
        shopIngredient.setStatus(Ingredient.Status.REQUESTED);
        shopIngredient2.setStatus(Ingredient.Status.PENDING);
        shoppingList.addIngredient(shopIngredient);
        shoppingList.addIngredient(shopIngredient2);

        assertEquals(2, shoppingList.getIngredientList().size());
        shoppingListService.removeAllRequestedFromShoppingList(account);
        assertEquals(1, shoppingList.getIngredientList().size());
        assertTrue(shoppingList.getIngredientList().stream().findFirst().isPresent());
        assertEquals(Ingredient.Status.PENDING, shoppingList.getIngredientList().stream().findFirst().get().getStatus());
    }
}
