package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.repo.LogRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class LogServiceTest {

    @Mock
    LogRepository logRepository;
    @InjectMocks
    LogService logService = new LogService(logRepository);
    @Mock
    Account account;
    @Mock Ingredient testIngredient;
    Log testLog = new Log(new Date(), Log.Action.DISCARDED, testIngredient, account);

    @Test
    void logActionLogsAction() {
        when(logService.logAction(testLog)).thenReturn(new Log());
        assertEquals(new Log().getLog_id(), logService.logAction(testLog).getLog_id());

    }
}