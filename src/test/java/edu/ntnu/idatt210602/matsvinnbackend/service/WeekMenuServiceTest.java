package edu.ntnu.idatt210602.matsvinnbackend.service;


import edu.ntnu.idatt210602.matsvinnbackend.model.Account;
import edu.ntnu.idatt210602.matsvinnbackend.model.DayMenu;
import edu.ntnu.idatt210602.matsvinnbackend.model.Recipe;
import edu.ntnu.idatt210602.matsvinnbackend.model.WeekMenu;
import edu.ntnu.idatt210602.matsvinnbackend.repo.DayMenuRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.RecipeRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.WeekMenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class WeekMenuServiceTest {

    @Mock
    WeekMenuRepository weekMenuRepository;

    @InjectMocks
    WeekMenuService weekMenuService;

    @Mock
    RecipeSearchService recipeSearchService;

    @Mock
    RecipeRepository recipeRepository;

    @Mock
    DayMenuRepository dayMenuRepository;

    @Mock
    FridgeService fridgeService;

    @Mock
    ShoppingListService shoppingListService;

    Account account;
    Recipe recipe;
    Recipe recipe2;
    Recipe recipe3;
    Recipe recipe4;
    Recipe recipe5;
    Recipe recipe6;
    Recipe recipe7;
    Recipe recipe8;
    List<Recipe> recipes;

    @BeforeEach
    void setup() {

        recipes = new ArrayList<>();
        recipe = new Recipe("title","desc","20m", new ArrayList<>(), "cook");
        recipe2 = new Recipe("title2","desc","20m", new ArrayList<>(), "cook");
        recipe3 = new Recipe("title3","desc","20m", new ArrayList<>(), "cook");
        recipe4 = new Recipe("title4","desc","20m", new ArrayList<>(), "cook");
        recipe5 = new Recipe("title5","desc","20m", new ArrayList<>(), "cook");
        recipe6 = new Recipe("title6","desc","20m", new ArrayList<>(), "cook");
        recipe7 = new Recipe("title7","desc","20m", new ArrayList<>(), "cook");
        recipe8 = new Recipe("title8","desc","20m", new ArrayList<>(), "cook");

        recipes.add(recipe);
        recipes.add(recipe2);
        recipes.add(recipe3);
        recipes.add(recipe4);
        recipes.add(recipe5);
        recipes.add(recipe6);
        recipes.add(recipe7);
        recipes.add(recipe8);

        account = new Account("peter","pete@gmail.com","duck");
    }

    @Test
    public void getWeekMenuWorks() {
        when(weekMenuRepository.findById(any())).thenReturn(Optional.of(account.getWeekMenu()));
        assertDoesNotThrow(() -> weekMenuService.getWeekMenu(account));
    }

    @Test
    public void updateWeekMenuWorks() {
        when(weekMenuRepository.existsById(any())).thenReturn(true);
        assertDoesNotThrow(() -> weekMenuService.updateWeekMenu(new WeekMenu()));
    }

    @Test
    public void generateWeekMenuWorks() {

        when(weekMenuRepository.findById(any())).thenReturn(Optional.of(account.getWeekMenu()));
        when(weekMenuRepository.existsById(any())).thenReturn(true);
        when(recipeSearchService.getRecipesByIngExpiry(any(),any())).thenReturn(new ArrayList<>());
        when(recipeSearchService.getRecipeSuggestions(any(),any(),any(),any(),any())).thenReturn(new ArrayList<>());
        when(recipeRepository.findAll()).thenReturn(recipes);
        when(dayMenuRepository.save(any())).thenReturn(new DayMenu(null, recipe, "Mandag"));
        assertDoesNotThrow(() -> weekMenuService.generateWeekMenu(account,1));
        assertEquals(7, weekMenuService.generateWeekMenu(account,1).getDayMenus().size());
    }
}
