package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.repo.ItemRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.RecipeRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RecipeSearchServiceTest {


    @Mock
    RecipeRepository recipeRepository;
    @Mock
    ItemRepository itemRepo;

    @InjectMocks
    RecipeSearchService recipeSearchService;

    Item item = new Item(
        "Vegansk Krone-Is Jordbær",
        "7041012550001",
        "https://bilder.ngdata.no/7041012550001/meny/large.jpg",
        30,
        new SerializedAmount(4, "stk"),
        new ArrayList<>(),
        new ArrayList<>());
    Item item2 = new Item(
        "Jarlsberg Gulost",
                "7038010053368",
                "https://bilder.ngdata.no/7038010053368/kmh/large.jpg",
                14,
                new SerializedAmount(
                700,
                "g"),
        new ArrayList<>() {},
        new ArrayList<>());

    Item item3 = new Item(
        "Fiskekaker",
                "",
                "https://bilder.kolonial.no/local_products/38597dee-915a-44e8-99db-5a5fc9a6926c.jpeg?auto=format&fit=max&w=376&s=45418fa0a305b733b154badc667fcee6",
                21,
                new SerializedAmount(1,"kg"),
        new ArrayList<>(),
        new ArrayList<>());
    @BeforeAll
    static void createTestRecipies() {




        //when(recipeRepository.findById(1)).thenReturn(Optional.of());

    }
    @Test
    void getRecipesWithIngredient() {
        Ingredient ingredient = new Ingredient(item, new SerializedAmount(1, "g"));
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);
        List<Ingredient> dummy = new ArrayList<>();
        Recipe recipe1 = new Recipe("Cheese", "buy chees", "1", dummy,"1. go to store\n2. buy chees" );
        Recipe recipe2 = new Recipe("Ice cream", "buy icekrem", "1", ingredients,"1. go to store\n2. buy icekrem" );
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(recipe1);
        recipes.add(recipe2);
        //Positive tests
        assertEquals("Ice cream", recipeSearchService.getRecipesWithIngredient(ingredient, recipes).get(0).getTitle());
        assertEquals(1,recipeSearchService.getRecipesWithIngredient(ingredient, recipes).size());
        //Negative tests
        assertNotEquals("Cheese", recipeSearchService.getRecipesWithIngredient(ingredient, recipes).get(0).getTitle());
    }

    @Test
    void getRecipesWithIngredients() {
        Ingredient ingredient = new Ingredient(item, new SerializedAmount(1, "g"));
        Ingredient ingredient2 = new Ingredient(item2, new SerializedAmount(1, "g"));
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);
        ingredients.add(ingredient2);

        List<Ingredient> dummy = new ArrayList<>();
        dummy.add(ingredient2);
        Recipe recipe1 = new Recipe("Cheese", "buy chees", "1", dummy,"1. go to store\n2. buy chees" );
        Recipe recipe2 = new Recipe("Ice cream and Cheese", "buy icekrem", "1", ingredients,"1. go to store\n2. buy icekrem" );
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(recipe1);
        recipes.add(recipe2);
        //Positive tests
        assertEquals("Ice cream and Cheese", recipeSearchService.getRecipesWithIngredients(ingredients, recipes).get(1).getTitle());
        assertEquals(2,recipeSearchService.getRecipesWithIngredients(ingredients, recipes).size());

    }
    @Test
    void getRecipesWithIngredientsNegative(){
        Ingredient ingredient = new Ingredient(item, new SerializedAmount(1, "g"));
        Ingredient ingredient2 = new Ingredient(item2, new SerializedAmount(1, "g"));
        Ingredient ingredient3 = new Ingredient(item3, new SerializedAmount(1, "g"));
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);
        ingredients.add(ingredient2);
        ingredients.add(ingredient3);
        Recipe recipe = new Recipe("Ice cream and Cheese", "buy icekrem", "1", ingredients,"1. go to store\n2. buy icekrem" );
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(recipe);
        List<Ingredient> testing = new ArrayList<>();
        testing.add(ingredient2);
        assertEquals(0, recipeSearchService.getRecipesWithIngredients(testing, recipes).size());
    }

    @Test
    void getMostMatches() {
        Ingredient ingredient = new Ingredient(item, new SerializedAmount(1, "g"));
        Ingredient ingredient2 = new Ingredient(item2, new SerializedAmount(1, "g"));
        Ingredient ingredient3 = new Ingredient(item3, new SerializedAmount(1, "g"));

        ingredient.setStatus(Ingredient.Status.FREE);
        ingredient2.setStatus(Ingredient.Status.FREE);
        ingredient3.setStatus(Ingredient.Status.FREE);

        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);
        List<Ingredient> ingredients2 = new ArrayList<>();
        ingredients2.add(ingredient);
        ingredients2.add(ingredient2);
        List<Ingredient> ingredients3 = new ArrayList<>();
        ingredients3.add(ingredient);
        ingredients3.add(ingredient2);
        ingredients3.add(ingredient3);
        Recipe recipe = new Recipe("Ice cream and Cheese", "buy icekrem", "1", ingredients,"1. go to store\n2. buy icekrem" );
        Recipe recipe2 = new Recipe("Ice cream", "buy icekrem", "1", ingredients2,"1. go to store\n2. buy icekrem" );
        Recipe recipe3 = new Recipe("Cheese", "buy chees", "1", ingredients3,"1. go to store\n2. buy chees" );

        List<Recipe> recipes = new ArrayList<>();
        recipes.add(recipe);
        recipes.add(recipe2);
        recipes.add(recipe3);
        List<Ingredient> testing = new ArrayList<>();
        testing.add(ingredient2);
        testing.add(ingredient3);
        assertEquals(3, recipeSearchService.getMostMatches(testing, recipes, 1).size());
        assertEquals(recipe3.getTitle(),recipeSearchService.getMostMatches(testing, recipes, 1).get(0).getTitle() );
    }
}