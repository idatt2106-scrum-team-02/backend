package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.model.FoodPreference;
import edu.ntnu.idatt210602.matsvinnbackend.repo.FoodPreferencesRepository;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class FoodPreferencesServiceTest {

    @Mock
    private FoodPreferencesRepository foodPreferencesRepository;

    @InjectMocks
    FoodPreferenceService foodPreferenceService;

    FoodPreference testPreference = new FoodPreference(null,"Vegan");
    FoodPreference testPreference2 = new FoodPreference(null,"Vegetarian");

    @Test
    public void getAllPreferencesReturnsAllSavedPreferences() {
        List<FoodPreference> preferences = new ArrayList<>();
        preferences.add(testPreference);
        preferences.add(testPreference2);
        foodPreferencesRepository.save(testPreference);
        foodPreferencesRepository.save(testPreference2);
        when(foodPreferencesRepository.findAll()).thenReturn(preferences);
        assertEquals(preferences, foodPreferenceService.getAllFoodPreferences());
    }
}
