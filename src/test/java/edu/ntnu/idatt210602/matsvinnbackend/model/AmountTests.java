package edu.ntnu.idatt210602.matsvinnbackend.model;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import edu.ntnu.idatt210602.matsvinnbackend.model.units.Count;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Mass;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Volume;

public class AmountTests {

    static List<List<Amount>> amountMatrix;

    @BeforeAll
    static void createTestMatrix() {
        ArrayList<Amount> masses = new ArrayList<>() {
            {
                add(new Amount(1.0, Mass.KILOGRAM));
                add(new Amount(500, Mass.GRAM));
            }
        };

        ArrayList<Amount> volumes = new ArrayList<>() {
            {
                add(new Amount(1.0, Volume.LITER));
                add(new Amount(5.0, Volume.DESILITER));
                add(new Amount(100.0, Volume.MILLILITER));
            }
        };

        ArrayList<Amount> counts = new ArrayList<>() {
            {
                add(new Amount(1.0, Count.COUNT));
                add(new Amount(2.0, Count.COUNT));
            }
        };

        amountMatrix = new ArrayList<>() {
            {
                add(masses);
                add(volumes);
                add(counts);
            }
        };
    }

    @Test
    void arithmeticWithAmountsOfSameUnit() {
        for (List<Amount> amountList : amountMatrix) {
            for (Amount amount : amountList) {
                Amount sum = amount.add(amount);
                Amount difference = amount.subtract(amount);
                Amount multiplied = amount.multiply(3);
                Amount divided = amount.divideBy(3);

                double ratio = amount.divideBy(amount);

                assertEquals(amount.quantity * 2, sum.quantity);
                assertEquals(amount.unit, sum.unit);

                assertEquals(0, difference.quantity);
                assertEquals(amount.unit, difference.unit);

                assertEquals(amount.quantity * 3, multiplied.quantity);
                assertEquals(amount.unit, multiplied.unit);

                assertEquals(amount.quantity / 3, divided.quantity);
                assertEquals(amount.unit, divided.unit);

                assertEquals(1.0, ratio);
            }
        }
    }

    @Test
    void arithmeticsWithAmountsOfCompatibleUnits() {
        for (List<Amount> amountList : amountMatrix) {
            for (int i = 0; i < amountList.size() - 1; i++) {
                Amount first = amountList.get(i);
                Amount second = amountList.get(i + 1);

                Amount sum = first.add(second);
                Amount difference = first.subtract(second);

                assertDoesNotThrow(() -> {
                    first.divideBy(second);
                });

                assertDoesNotThrow(() -> {
                    second.divideBy(first);
                });

                assertTrue(sum.quantity > first.quantity);
                assertEquals(first.unit, sum.unit);

                sum = second.add(first);

                assertTrue(sum.quantity > second.quantity);
                assertEquals(second.unit, sum.unit);

                assertTrue(difference.quantity < first.quantity);
                assertEquals(first.unit, difference.unit);

                difference = second.subtract(first);

                assertTrue(difference.quantity < second.quantity);
                assertEquals(second.unit, difference.unit);

            }
        }
    }

    @Test
    void arithmeticsWithAmountsOfIncompatibleUnits() {
        for (int i = 0; i < amountMatrix.size() - 1; i++) {
            Amount first = amountMatrix.get(i).get(0);
            Amount second = amountMatrix.get(i + 1).get(0);

            assertThrows(IllegalArgumentException.class, () -> {
                first.add(second);
            });

            assertThrows(IllegalArgumentException.class, () -> {
                second.add(first);
            });

            assertThrows(IllegalArgumentException.class, () -> {
                first.subtract(second);
            });

            assertThrows(IllegalArgumentException.class, () -> {
                second.subtract(first);
            });
            
            assertThrows(IllegalArgumentException.class, () -> {
                first.divideBy(second);
            });

            assertThrows(IllegalArgumentException.class, () -> {
                second.divideBy(first);
            });
        }
    }
}
