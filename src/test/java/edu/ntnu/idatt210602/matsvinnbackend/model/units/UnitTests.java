package edu.ntnu.idatt210602.matsvinnbackend.model.units;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class UnitTests {

    static List<List<Unit>> unitMatrix;

    @BeforeAll
    static void createTestMatrix() {
        List<Unit> masses = new ArrayList<>() {
            {
                add(Mass.GRAM);
                add(Mass.KILOGRAM);
            }
        };

        List<Unit> volumes = new ArrayList<>() {
            {
                add(Volume.MILLILITER);
                add(Volume.DESILITER);
                add(Volume.LITER);
            }
        };

        List<Unit> counts = new ArrayList<>() {
            {
                add(Count.COUNT);
                add(Count.COUNT);
            }
        };

        unitMatrix = new ArrayList<>() {
            {
                add(masses);
                add(volumes);
                add(counts);
            }
        };
    }

    @Test
    void allValidConversions() {
        for (List<Unit> unitList : unitMatrix) {
            for (int i = 0; i < unitList.size() - 1; i++) {
                final int j = i;

                for (Unit unit : unitList) {

                    assertDoesNotThrow(() -> {
                        unitList.get(j).getRatio(unit);
                    });

                    assertDoesNotThrow(() -> {
                        unitList.get(j + 1).getRatio(unit);
                    });
                }
            }
        }
    }
}
