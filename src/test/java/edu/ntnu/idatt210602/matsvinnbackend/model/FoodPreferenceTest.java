package edu.ntnu.idatt210602.matsvinnbackend.model;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class FoodPreferenceTest {

    @Test
    public void WhenConstructingAnObjectItIsConstructed() {
        FoodPreference foodPreference = new FoodPreference(null,"vegan");
        assertEquals("vegan", foodPreference.getType());
        assertNull(foodPreference.getId());

        foodPreference.setType("vegetarian");
        assertEquals("vegetarian", foodPreference.getType());
    }

    @Test
    public void WhenUsingEmptyConstructorFieldsAreNull() {
        FoodPreference foodPreference = new FoodPreference();
        assertNull(foodPreference.getType());
        assertNull(foodPreference.getId());
    }
}
