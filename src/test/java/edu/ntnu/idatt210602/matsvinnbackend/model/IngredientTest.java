package edu.ntnu.idatt210602.matsvinnbackend.model;

import edu.ntnu.idatt210602.matsvinnbackend.repo.IngredientRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest //@DataJpaTest //?
@ExtendWith(MockitoExtension.class)
public class IngredientTest {

    @Mock
    IngredientRepository ingredientRepository;

    Ingredient testIngredient = new Ingredient();
    @Test
    public void saveIngredientWorks(){
        when(ingredientRepository.save(testIngredient)).thenReturn(new Ingredient());
        assertEquals(new Ingredient().getIngredient_id(), ingredientRepository.save(testIngredient).getIngredient_id());
    }

    @Test
    public void saveIngredientWithConsWorks(){
        when(ingredientRepository.save(testIngredient)).thenReturn(new Ingredient(new Item(), new SerializedAmount()));
        assertEquals(new Ingredient(new Item(), new SerializedAmount()).getIngredient_id(), ingredientRepository.save(testIngredient).getIngredient_id());
    }
}
