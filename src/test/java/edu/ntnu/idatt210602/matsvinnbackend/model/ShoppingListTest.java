package edu.ntnu.idatt210602.matsvinnbackend.model;

import edu.ntnu.idatt210602.matsvinnbackend.model.units.Count;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ShoppingListTest {

    Ingredient ingredient;
    @BeforeEach
    public void setup() {
        ingredient = new Ingredient(null,new Amount(20, Count.COUNT).serialize());
    }

    @Test
    public void GeneratedMethodsWorks() {
        ShoppingList shoppingList = new ShoppingList();
        shoppingList.setIngredientList(null);
        shoppingList.setSuggestionList(null);
        assertNull(shoppingList.getSuggestionList());
        assertNull(shoppingList.getIngredientList());
        assertNull(shoppingList.getId());
    }

    @Test
    public void whenAddingIngredientToListItGetsAdded() {
        ShoppingList shoppingList = new ShoppingList(null, new HashSet<>(), new HashSet<>());
        assertEquals(0, shoppingList.getIngredientList().size());
        shoppingList.addIngredient(ingredient);
        assertEquals(1, shoppingList.getIngredientList().size());
    }

    @Test
    public void whenRemovingIngredientFromListItGetsRemoved() {
        ShoppingList shoppingList = new ShoppingList(null, new HashSet<>(), new HashSet<>());
        shoppingList.addIngredient(ingredient);
        assertEquals(1, shoppingList.getIngredientList().size());
        shoppingList.removeIngredient(ingredient.getIngredient_id());
        assertEquals(0, shoppingList.getIngredientList().size());
    }

    @Test
    public void whenAddingSuggestionToListItGetsAdded() {
        ShoppingList shoppingList = new ShoppingList();
        assertEquals(0, shoppingList.getSuggestionList().size());
        shoppingList.addSuggestion(ingredient);
        assertEquals(1, shoppingList.getSuggestionList().size());
    }

    @Test
    public void whenRemovingSuggestionFromListItGetsRemoved() {
        ShoppingList shoppingList = new ShoppingList(null, new HashSet<>(), new HashSet<>());
        shoppingList.addSuggestion(ingredient);
        assertEquals(1, shoppingList.getSuggestionList().size());
        shoppingList.removeSuggestion(ingredient.getIngredient_id());
        assertEquals(0, shoppingList.getSuggestionList().size());
    }

    @Test
    public void isIngredientInShoppingListWorks() {
        ShoppingList shoppingList = new ShoppingList(null, new HashSet<>(), new HashSet<>());
        ingredient.setIngredient_id(1);
        shoppingList.addIngredient(ingredient);
        assertFalse(shoppingList.IsIngredientInList(2));
        assertTrue(shoppingList.IsIngredientInList(1));
    }

    @Test
    public void isIngredientInSuggestionsWorks() {
        ShoppingList shoppingList = new ShoppingList(null, new HashSet<>(), new HashSet<>());
        ingredient.setIngredient_id(1);
        shoppingList.addSuggestion(ingredient);
        assertFalse(shoppingList.IsIngredientInSuggestions(2));
        assertTrue(shoppingList.IsIngredientInSuggestions(1));
    }

    @Test
    public void getIngredientFromListWorks() {
        ShoppingList shoppingList = new ShoppingList(null, new HashSet<>(), new HashSet<>());
        ingredient.setIngredient_id(1);
        shoppingList.addIngredient(ingredient);
        assertEquals(ingredient,shoppingList.getIngredientFromList(1));
        assertNull(shoppingList.getIngredientFromList(2));
    }

    @Test
    public void getIngredientFromSuggestionListWorks() {
        ShoppingList shoppingList = new ShoppingList(null, new HashSet<>(), new HashSet<>());
        ingredient.setIngredient_id(1);
        shoppingList.addSuggestion(ingredient);
        assertEquals(ingredient,shoppingList.getSuggestionFromList(1));
        assertNull(shoppingList.getSuggestionFromList(2));
    }
}
