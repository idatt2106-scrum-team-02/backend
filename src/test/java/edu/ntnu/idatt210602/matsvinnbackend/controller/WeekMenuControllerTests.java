package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import edu.ntnu.idatt210602.matsvinnbackend.service.WeekMenuService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class WeekMenuControllerTests {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private WeekMenuService weekMenuService;

    @InjectMocks
    private WeekMenuController weekMenuController;

    Account testAccount;
    @BeforeEach
    void setup() {

        testAccount = new Account("test", "test@test.no", "test") {
            {
                setId(1);
            }
        };
    }

    @Test
    public void generateWeekMenuEndpointWorks() {

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(weekMenuService.generateWeekMenu(any(),any())).thenReturn(testAccount.getWeekMenu());
        assertDoesNotThrow(() -> weekMenuController.generateWeekMenu(1));
    }

    @Test
    public void generateWeekMenuEndpointInvalidNumPeopleWorks() {

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        assertThrows(ResponseStatusException.class,() -> weekMenuController.generateWeekMenu(0));
        assertThrows(ResponseStatusException.class,() -> weekMenuController.generateWeekMenu(9999999));
    }

    @Test
    public void getWeekMenuEndpointWorks() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(weekMenuService.getWeekMenu(any(Account.class))).thenReturn(testAccount.getWeekMenu());
        assertDoesNotThrow(() -> weekMenuController.getWeekMenu());
    }
}
