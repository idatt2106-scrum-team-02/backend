package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.FoodPreference;
import edu.ntnu.idatt210602.matsvinnbackend.repo.FoodPreferencesRepository;
import edu.ntnu.idatt210602.matsvinnbackend.service.FoodPreferenceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(FoodPreferenceController.class)
@AutoConfigureMockMvc(addFilters = false)
public class FoodPreferencesControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FoodPreferencesRepository foodPreferencesRepository;

    @MockBean
    private FoodPreferenceService foodPreferenceService;

    @Autowired
    FoodPreferenceController foodPreferenceController;

    FoodPreference testPreference = new FoodPreference(null,"Vegan");
    FoodPreference testPreference2 = new FoodPreference(null,"Vegetarian");

    @BeforeEach
    void setup() {
        foodPreferencesRepository.deleteAll();
    }

    @Test
    public void GetAllFoodPreferencesGivesOkStatusResponse() throws Exception {

        this.mvc.perform(get("/foodpreferences")).andDo(print()).andExpect(status().isOk());
        assertEquals(0, foodPreferenceController.getAllFoodPreferences().size());
    }

    @Test
    public void getAllFoodPreferencesWhenSavedPreferencesReturnsAllSavedPreferences() {

        List<FoodPreference> preferences = new ArrayList<>();
        preferences.add(testPreference);
        preferences.add(testPreference2);
        foodPreferencesRepository.save(testPreference);
        foodPreferencesRepository.save(testPreference2);
        when(foodPreferenceService.getAllFoodPreferences()).thenReturn(preferences);
        assertEquals(preferences, foodPreferenceController.getAllFoodPreferences());
    }
}
