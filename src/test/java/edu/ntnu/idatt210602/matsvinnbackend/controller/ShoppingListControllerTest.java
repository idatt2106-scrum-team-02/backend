package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Count;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import edu.ntnu.idatt210602.matsvinnbackend.service.ShoppingListService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ShoppingListControllerTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private ShoppingListService shoppingListService;

    @InjectMocks
    private ShoppingListController shoppingListController;

    Account testAccount;
    Item item;
    ShoppingList shoppingList;
    Ingredient ingredient;

    @BeforeEach
    void setup() {

        testAccount = new Account("test", "test@test.no", "test") {
            {
                setId(1);
            }
        };
        shoppingList = new ShoppingList(1,new HashSet<>(), new HashSet<>());
        testAccount.setShoppingList(shoppingList);

        item = new Item("carrot","1",null,null,null,null,null);
        ingredient = new Ingredient(item,new Amount(1, Count.COUNT).serialize());
    }

    @Test
    public void GetShoppingListWorks() {

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.getShoppingList(any(Account.class))).thenReturn(shoppingList);
        assertDoesNotThrow(() -> shoppingListController.getShoppingList());
    }

    @Test
    public void whenAddingItemCorrectlyItReturnsOkStatus() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.addItem(eq(1),eq(4),any(Account.class))).thenReturn(ingredient);
        assertDoesNotThrow(() -> shoppingListController.addItem(1,4));
    }

    @Test
    public void whenAddingSuggestionCorrectlyItReturnsOkStatus() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.addSuggestion(eq(1),eq(3),any(Account.class))).thenReturn(ingredient);
        assertDoesNotThrow(() -> shoppingListController.addSuggestion(1,3));
    }

    @Test
    public void whenAddingNonExistingItemItReturnsException() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.addItem(eq(2),eq(4),any(Account.class))).thenThrow(IllegalArgumentException.class);
        assertThrows(ResponseStatusException.class, ()-> shoppingListController.addItem(2,4));
    }

    @Test
    public void whenAddingNonExistingItemForSuggestionItReturnsException() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.addSuggestion(eq(2),eq(4),any(Account.class))).thenThrow(IllegalArgumentException.class);
        assertThrows(ResponseStatusException.class, ()-> shoppingListController.addSuggestion(2,4));
    }

    @Test
    public void whenRemovingExistingItemThenReturnsOkStatus() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.removeItem(eq(1),any(Account.class))).thenReturn(ingredient);
        assertDoesNotThrow(() -> shoppingListController.removeIngredient(1));
    }

    @Test
    public void whenRemovingNonExistingItemThenReturnsException() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        doThrow(IllegalArgumentException.class).when(shoppingListService).removeItem(eq(2), any(Account.class));
        assertThrows(ResponseStatusException.class, () -> shoppingListController.removeIngredient(2));
    }

    @Test
    public void whenRemovingExistingSuggestionThenReturnsOkStatus() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.removeSuggestion(eq(1),any(Account.class))).thenReturn(ingredient);
        assertDoesNotThrow(() -> shoppingListController.denySuggestion(1));
    }

    @Test
    public void whenRemovingNonExistingSuggestionThenReturnsException() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        doThrow(IllegalArgumentException.class).when(shoppingListService).removeSuggestion(eq(2), any(Account.class));
        assertThrows(ResponseStatusException.class, () -> shoppingListController.denySuggestion(2));
    }

    @Test
    public void whenAcceptingExistingSuggestionThenReturnsOkStatus() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.acceptSuggestion(eq(1),any(Account.class))).thenReturn(ingredient);
        assertDoesNotThrow(() -> shoppingListController.acceptSuggestion(1));
    }

    @Test
    public void whenAcceptingNonExistingSuggestionThenReturnsException() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        doThrow(IllegalArgumentException.class).when(shoppingListService).acceptSuggestion(eq(2), any(Account.class));
        assertThrows(ResponseStatusException.class, () -> shoppingListController.acceptSuggestion(2));
    }

    @Test
    public void whenChangingAmountForExistingItemReturnsOkStatus() {

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.changeItemAmount(eq(1),anyInt(),any(Account.class))).thenReturn(shoppingList);
        assertDoesNotThrow(() -> shoppingListController.changeItemAmount(1,3));
    }

    @Test
    public void whenChangingAmountForItemsWithIllegalValuesThrowsResponseStatus() {

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        doThrow(IllegalArgumentException.class).when(shoppingListService).changeItemAmount(eq(1),anyInt(),any(Account.class));
        assertThrows(ResponseStatusException.class, () -> shoppingListController.changeItemAmount(1,3));
    }

    @Test
    public void whenChangingAmountForExistingSuggestionReturnsOkStatus() {

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.changeSuggestionAmount(eq(1),anyInt(),any(Account.class))).thenReturn(shoppingList);
        assertDoesNotThrow(() -> shoppingListController.changeSuggestionAmount(1,3));
    }

    @Test
    public void whenChangingAmountForSuggestionWithIllegalValuesThrowsResponseStatus() {

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        doThrow(IllegalArgumentException.class).when(shoppingListService).changeSuggestionAmount(eq(1),anyInt(),any(Account.class));
        assertThrows(ResponseStatusException.class, () -> shoppingListController.changeSuggestionAmount(1,3));
    }

    @Test
    public void addRecipeItemsEndpointWorks() {

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(shoppingListService.addAllItemsFromRecipe(anyInt(), any(Account.class), anyInt())).thenReturn(shoppingList);
        assertDoesNotThrow(() -> shoppingListController.addItemsFromRecipe(1));
    }

    @Test
    public void addRecipeItemsInvalidInputThrowsResponseStatus() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        doThrow(IllegalArgumentException.class).when(shoppingListService).addAllItemsFromRecipe(anyInt(), any(Account.class), anyInt());
        assertThrows(ResponseStatusException.class, () -> shoppingListController.addItemsFromRecipe(1));
    }
}
