package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.Account;
import edu.ntnu.idatt210602.matsvinnbackend.model.AddAccountRequest;
import edu.ntnu.idatt210602.matsvinnbackend.model.Profile;
import edu.ntnu.idatt210602.matsvinnbackend.model.UpdateAccountRequest;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.ProfileRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class AccountControllerTests {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private ProfileRepository profileRepository;

    @InjectMocks
    private AccountController accountController;

    Account testAccount = new Account("test", "test@test.no", "test") {
        {
            setId(1);
        }
    };

    @Test
    public void addAccountWithUniqueEmailWorks() {
        when(accountRepository.existsByEmail(Mockito.anyString())).thenReturn(false);
        when(profileRepository.save(Mockito.any(Profile.class))).thenReturn(new Profile());
        when(accountRepository.save(Mockito.any(Account.class))).thenReturn(testAccount);
        assertEquals(accountController.addAccount(new AddAccountRequest(testAccount.getEmail(), testAccount.getPassword(), testAccount.getFirstname())).getFirstname(), testAccount.getFirstname());
    }

    @Test
    public void addAccountWithDuplicateEmailDoesntWork() {
        when(accountRepository.existsByEmail(Mockito.anyString())).thenReturn(true);
        assertThrows(ResponseStatusException.class, ()->accountController.addAccount(new AddAccountRequest(testAccount.getEmail(), testAccount.getPassword(), testAccount.getFirstname())).getFirstname(), testAccount.getFirstname());
    }

    @Test
    public void deleteOwnAccount() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        assertDoesNotThrow(()-> accountController.deleteAccount(1));
    }

    @Test
    public void deleteOthersAccountDoesntWork() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        assertThrows(ResponseStatusException.class, ()-> accountController.deleteAccount(2));
    }

    @Test
    public void getOwnAccount() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(accountRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testAccount));
        assertDoesNotThrow(()-> accountController.getAccount(1));
    }

    @Test
    public void updateOwnAccount() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(accountRepository.save(Mockito.any(Account.class))).thenReturn(testAccount);

        assertDoesNotThrow(()->accountController.updateAccount(1, new UpdateAccountRequest("pass", "test")));
    }

}
