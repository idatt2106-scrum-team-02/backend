package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.repo.*;
import edu.ntnu.idatt210602.matsvinnbackend.service.FridgeService;
import edu.ntnu.idatt210602.matsvinnbackend.service.LogService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class FridgeControllerTests {

    @Mock
    FridgeRepository fridgeRepository;

    @Mock
    AccountRepository accountRepository;

    @Mock
    IngredientRepository ingredientRepository;

    @Mock
    FridgeService fridgeService;

    @Mock
    LogRepository logRepository;

    @Mock
    LogService logService;

    @InjectMocks
    FridgeController fridgeController;

    @Mock
    ItemRepository itemRepository;

    Fridge testFridge = new Fridge() {
        {
            setId(1);
        }
    };

    Account testAccount = new Account("Test", "test@test.no", "test") {
        {
            setId(1);
            setFridge(testFridge);
        }
    };

    SerializedAmount testAmount = new SerializedAmount(600.0, "g");
    Item testItem = new Item("cheese", "ean", "image_url", 7, testAmount, null, null) {
        {
            setId(1);
        }
    };
    Ingredient testIngredient = new Ingredient(testItem, testAmount) {
        {
            setIngredient_id(1);
            setExp_date(new Date());
        }
    };

    List<Ingredient> testIngredientList = new ArrayList<>();


    @Test
    public void changeValidAmountOfIngredientWorks() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(fridgeRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testFridge));
        when(fridgeRepository.save(Mockito.any(Fridge.class))).thenReturn(testFridge);
        when(ingredientRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testIngredient));
        testFridge.addIngredient(testIngredient);
        HashMap<Integer, SerializedAmount> newMap = new HashMap<>();
        newMap.put(1, new SerializedAmount(200.0, "g"));
        List<Ingredient> list = fridgeController.removeIngredientsAmount(new UpdateIngredientsAmountInFridge(Log.Action.CONSUMED, newMap));

        assertEquals(list.get(0).getAmount().getQuantity(), 400.0);
    }

    @Test
    public void removeIngredientByChangingValueTo0() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(fridgeRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testFridge));
        when(fridgeRepository.save(Mockito.any(Fridge.class))).thenReturn(testFridge);
        when(ingredientRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testIngredient));
        testFridge.addIngredient(testIngredient);
        HashMap<Integer, SerializedAmount> newMap = new HashMap<>();
        newMap.put(1, new SerializedAmount(600.0, "g"));
        List<Ingredient> list = fridgeController.removeIngredientsAmount(new UpdateIngredientsAmountInFridge(Log.Action.CONSUMED, newMap));

        assertTrue(list.isEmpty());
    }

    @Test
    public void addItemToFridge() {
        List<Ingredient> testIngredientList = new ArrayList<>();
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(itemRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testItem));
        when(ingredientRepository.save(Mockito.any(Ingredient.class))).thenReturn(testIngredient);
        testIngredientList.add(testIngredient);
        when(fridgeService.addIngredients(Mockito.any(Account.class), Mockito.anyList())).thenReturn(testIngredientList);
        List<Ingredient> list = fridgeController.addItem(new AddItemToFridgeRequest(1, testAmount));
        assertEquals(list.isEmpty(), false);
    }


    @Test
    public void changeExpireDate() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));

        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(ingredientRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testIngredient));
        when(fridgeRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testFridge));
        testFridge.addIngredient(testIngredient);
        Calendar cal = Calendar.getInstance();
        cal.add( Calendar.DATE,30);
        Ingredient ingredient = fridgeController.adjustIngredientExpireDate(new UpdateIngredientDateRequest(1, cal.getTime()));

        assertEquals(ingredient.getExp_date(), cal.getTime());
    }

    /*@Test
    public void removeIngredientsFromFridgeBasedOnRecipe() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));

         *//*
         Test description:
         Recipe needs: 200g "cheese"
         Fridge has: 2x100g "cheese"
         Expected result: cheese should be removed completely from fridge.
         *//*

        //MOCKS
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.ofNullable(testAccount));
        when(ingredientRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testIngredient));
        when(fridgeRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testFridge));


        /////////////
        // RECIPE //
        ///////////
        //recipe needs 200g cheese
        Ingredient testRecipeIngredient = new Ingredient(testItem, new SerializedAmount(200.0, "g")) {
            {
                setIngredient_id(1);
            }
        };

        ArrayList<Ingredient> testRecipeIngredientList = new ArrayList<>();
        testRecipeIngredientList.add(testRecipeIngredient); //200g of "ost"

        int recipeId = 1;
        int recipeScalar = 1;
        Recipe testRecipe = new Recipe(
                "Title",
                "Description",
                "time",
                testRecipeIngredientList,
                "Instructions") {
            {
                setId(recipeId);
            }
        };

        /////////////
        // FRIDGE //
        ///////////
        //fridge has 2x 100g cheese
        Ingredient testFridgeIngredient = new Ingredient(testItem, new SerializedAmount(100.0, "g")){
            {
                setExp_date(new Date());
            }
        };

        Ingredient testFridgeIngredient2 = new Ingredient(testItem, new SerializedAmount(100.0, "g")) {
            {
                setExp_date(new Date());
            }
        };

        ArrayList<Ingredient> testFridgeIngredients = new ArrayList<>();
        testFridgeIngredients.add(testFridgeIngredient); //100g of "cheese"
        testFridgeIngredients.add(testFridgeIngredient2); //100g of "cheese"



        fridgeController.removeIngredients(new RemoveIngredientsRequest(recipeId, recipeScalar));

        //then fridge should not contain cheese longer


    }*/
}
