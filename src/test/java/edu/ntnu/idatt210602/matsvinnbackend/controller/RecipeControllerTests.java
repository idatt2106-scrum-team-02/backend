package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.Recipe;
import edu.ntnu.idatt210602.matsvinnbackend.repo.RecipeRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class RecipeControllerTests {

    @Mock//Does this do anything? - Yes, this enables mock use of database
    RecipeRepository recipeRepository;

    @InjectMocks //Does this do anything?
    RecipeController recipeController;

    Recipe testRecipe = new Recipe();
    Recipe secondTestRecipe = new Recipe();
    List<Recipe> testRecipes = new ArrayList<>();


    @Test
    public void saveRecipeWorks(){
        when(recipeRepository.save(testRecipe)).thenReturn(new Recipe());
        assertEquals(new Recipe().getId(), recipeRepository.save(testRecipe).getId());
    }
    @Test
    public void getRecipePositive() {
        when(recipeRepository.findById(1)).thenReturn(Optional.of(testRecipe));
        assertEquals(testRecipe, recipeController.getRecipe(1));
    }
    @Test
    public void getRecipeNegative() {
        assertThrows(ResponseStatusException.class, () -> recipeController.getRecipe(2));
    }

    @BeforeEach
    public void setup(){
        testRecipes.add(testRecipe);
        testRecipes.add(secondTestRecipe);
    }
    @Test
    public void getRecipesPositive() {
        when(recipeRepository.findAll()).thenReturn(testRecipes);
        assertEquals(testRecipes, recipeController.getRecipes());
    }

    @Test
    public void getRecipesNegative() {
        recipeRepository.deleteAll();
        //when(recipeRepository.findAll()).thenReturn(Optional.of(testRecipes));
        assertNotEquals(testRecipes, recipeController.getRecipes());
    }
}
