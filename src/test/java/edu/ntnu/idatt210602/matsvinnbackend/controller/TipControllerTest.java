package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.Tip;
import edu.ntnu.idatt210602.matsvinnbackend.repo.TipRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class TipControllerTest {
    @Mock
    TipRepository tipRepository;
    @InjectMocks
    TipController tipController;
    @Test
    void tipGetsSaved(){
        Tip tip = new Tip("This is a tip on how to do things");
        when(tipRepository.save(tip)).thenReturn(new Tip());
        assertEquals(new Tip().getId(), tipRepository.save(tip).getId());

        //assertEquals(tip, tipRepository.save(tip));
        //assertEquals(tip, tipController.getRandomTip());
    }

    @Test
    void fetchRandomTip(){
        Tip tip = new Tip("This is a tip on how to do things");
        List<Tip> tips = new ArrayList<>();
        tips.add(tip);
        when(tipRepository.findAll()).thenReturn(tips);
        assertEquals(tip.getId(), tipController.getRandomTip().getId());
    }
    @Test //TODO: Change to require 404 instead of any exception
    void fetchRandomTipNegative(){

        //when(tipRepository.getReferenceById(1)).thenReturn(new Tip());

        assertThrows(ResponseStatusException.class,() -> tipController.getRandomTip());
    }
}
