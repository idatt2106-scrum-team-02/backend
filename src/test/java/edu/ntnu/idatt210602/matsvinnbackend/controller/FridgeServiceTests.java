package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Mass;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.FridgeRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.IngredientRepository;
import edu.ntnu.idatt210602.matsvinnbackend.service.FridgeService;
import edu.ntnu.idatt210602.matsvinnbackend.service.LogService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class FridgeServiceTests {

    @Mock
    FridgeRepository fridgeRepository;

    @Mock
    AccountRepository accountRepository;

    @Mock
    IngredientRepository ingredientRepository;

    @Mock
    FridgeController fridgeController;

    @Mock
    LogService logService;

    @InjectMocks
    FridgeService fridgeService;

    Fridge testFridge = new Fridge() {
        {
            setId(1);
        }
    };

    Account testAccount = new Account("Test", "test@test.no", "test") {
        {
            setId(1);
            setFridge(testFridge);
        }
    };

    Amount testAmount = new Amount(1.0, Mass.GRAM);
    Item testItem = new Item("test", "test", "test" ,7, testAmount.serialize(), null, null);

    @Test
    public void addIngredientsToFridge() {
        List<Ingredient> ingredientsList = new ArrayList<>();
        ingredientsList.add(new Ingredient(testItem, testAmount.serialize()));
        when(fridgeRepository.save(Mockito.any(Fridge.class))).thenReturn(testFridge);
        assertEquals(fridgeService.addIngredients(testAccount, ingredientsList).isEmpty(), false);
    }
}
