package edu.ntnu.idatt210602.matsvinnbackend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import java.util.Optional;
import edu.ntnu.idatt210602.matsvinnbackend.model.Account;
import edu.ntnu.idatt210602.matsvinnbackend.model.LoginRequest;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.server.ResponseStatusException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class TokenControllerTests {

    @Mock
    AccountRepository accountRepository;

    @InjectMocks
    private TokenController tokenController;

    Account testAccount = new Account("Test", "test@test.no", "test") {
        {
            setId(1);
        }
    };

    @Test
    public void testCreateTokenPositive() {
        when(accountRepository.findByEmail("test@test.no"))
                .thenReturn(Optional.of(testAccount));

        assertEquals(
                tokenController.getToken(
                        new LoginRequest("test@test.no", "test")),
                TokenController.generateToken(testAccount));
    }
    @Test
    public void testCreateTokenWrongPassword() {
        when(accountRepository.findByEmail("test"))
                .thenReturn(Optional.of(testAccount));
        assertThrows(ResponseStatusException.class, ()->tokenController.getToken(
                new LoginRequest("test", "not the right password")));
    }

    @Test
    public void testCreateTokenNonExistingUser() {
        when(accountRepository.findByEmail("test"))
                .thenReturn(Optional.empty());

        assertThrows(
                ResponseStatusException.class,
                () -> {
                    tokenController.getToken(
                            new LoginRequest("test", "irrelevant to this test")
                    );
                }
        );
    }

    @Test
    public void testTokenContainsEmailTypeAndID() {
        when(accountRepository.findByEmail("test"))
                .thenReturn(Optional.of(testAccount));

        final String token = tokenController.getToken(
                new LoginRequest("test", "test")
        );

        DecodedJWT decoded = JWT.decode(token);

        assertEquals(testAccount.getEmail(), decoded.getSubject());
        assertEquals(decoded.getClaim("id").asString(), testAccount.getId().toString());
    }

}
