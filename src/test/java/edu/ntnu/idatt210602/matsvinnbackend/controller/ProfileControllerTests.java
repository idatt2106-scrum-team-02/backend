package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.Account;
import edu.ntnu.idatt210602.matsvinnbackend.model.AddProfileRequest;
import edu.ntnu.idatt210602.matsvinnbackend.model.Profile;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.ProfileRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.server.ResponseStatusException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ProfileControllerTests {
    @Mock
    AccountRepository accountRepository;

    @Mock
    ProfileRepository profileRepository;

    @InjectMocks
    ProfileController profileController;

    Account testAccount = new Account("Test", "test@test.no", "test") {
        {
            setId(1);
        }
    };

    Profile testProfile = new Profile("Test", "", false, testAccount.getId()) {
        {
            setId(1);
        }
    };

    @BeforeEach
    public void authenticate() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(testAccount.getUsername(), null, null));
    }

    @Test
    public void createProfileWithUniqueNameWorks() {
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(testAccount));
        when(profileRepository.save(Mockito.any(Profile.class))).thenReturn(testProfile);
        assertEquals(profileController.addProfile(new AddProfileRequest("Test", "", false)).getName(), testProfile.getName());
    }

    @Test
    public void createProfileWithDuplicateNameDoesntWork() {
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(testAccount));
        when(profileRepository.existsByName(Mockito.anyString())).thenReturn(true);

        assertThrows(
                ResponseStatusException.class,
                () -> profileController.addProfile(new AddProfileRequest("Test", "", false))
        );
    }

    @Test
    public void changeToRestrictedWhenIsLastUnrestricted() {
        List<Profile> list = new ArrayList<>();
        list.add(testProfile);
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(testAccount));
        when(profileRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(testProfile));
        when(profileRepository.findAllByAccountId(Mockito.anyInt())).thenReturn(list);

        assertThrows(
                ResponseStatusException.class,
                () -> profileController.updateProfile(1, new AddProfileRequest(null, null, true))
        );
    }

    @Test
    public void changeToRestrictedWhenMultipleUnrestricted() {
        List<Profile> list = new ArrayList<>();
        list.add(testProfile);
        list.add(new Profile("test", "", false, 1));

        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(testAccount));
        when(profileRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(testProfile));
        when(profileRepository.save(Mockito.any(Profile.class))).thenReturn(testProfile);
        when(profileRepository.findAllByAccountId(Mockito.anyInt())).thenReturn(list);

        assertTrue(profileController.updateProfile(1, new AddProfileRequest(null, null, true)).isRestricted());
    }

    @Test
    public void changeToUnrestricted() {
        List<Profile> list = new ArrayList<>();
        testProfile.setId(1);
        testProfile.setRestricted(true);
        list.add(testProfile);
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(testAccount));
        when(profileRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(testProfile));
        when(profileRepository.save(Mockito.any(Profile.class))).thenReturn(testProfile);

        assertFalse(profileController.updateProfile(1, new AddProfileRequest(null, null, false)).isRestricted());
    }

    @Test
    public void getProfileByExistingId() {
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(testAccount));
        when(profileRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testProfile));
        assertEquals(profileController.getProfile(1).getName(), testProfile.getName());
    }

    @Test
    public void failToGetProfileByNonExistingId() {
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(testAccount));
        when(profileRepository.findById(Mockito.anyInt())).thenThrow(NullPointerException.class);

        assertThrows(NullPointerException.class, () -> profileController.getProfile(Mockito.anyInt()));
    }


    @Test
    public void deleteWhenIsLastUnrestrictedProfile() {
        List<Profile> list = new ArrayList<>();
        list.add(testProfile);
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(testAccount));
        when(profileRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testProfile));
        when(profileRepository.findAllByAccountId(Mockito.anyInt())).thenReturn(list);

        assertThrows(ResponseStatusException.class, () -> profileController.deleteProfile(testProfile.getId()));
    }

    @Test
    public void deleteUnrestrictedWhenThereAreOthers() {
        List<Profile> list = new ArrayList<>();
        list.add(testProfile);
        list.add(new Profile("test", "", false, testAccount.getId()));
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(testAccount));
        when(profileRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testProfile));

        assertDoesNotThrow(() -> profileController.deleteProfile(testProfile.getId()));
    }

    @Test
    public void deleteRestrictedProfile() {
        List<Profile> list = new ArrayList<>();
        Profile testProfile2 = new Profile("test", "", true, testAccount.getId());
        testProfile2.setId(2);
        list.add(testProfile);
        list.add(testProfile2);
        when(accountRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(testAccount));
        when(profileRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(testProfile));

        assertDoesNotThrow(() -> profileController.deleteProfile(testProfile2.getId()));
    }

}
