package edu.ntnu.idatt210602.matsvinnbackend.startup;


import edu.ntnu.idatt210602.matsvinnbackend.model.Account;
import edu.ntnu.idatt210602.matsvinnbackend.model.Profile;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * This class is run on applications start and creates a set of users
 */
@Component
public class CreateAccountAndProfile implements CommandLineRunner {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    ProfileRepository profileRepository;

    @Override
    public void run(String... args) {
        if (!accountRepository.existsByEmail("ola@ola.no")) {
            Account account = new Account("Ola", "ola@ola.no", "ola123");
            accountRepository.save(account);
            Profile profile = new Profile(account.getFirstname(), "", false, account.getId());
            profileRepository.save(profile);
        }

        if (!accountRepository.existsByEmail("kari@kari.no")) {
            Account account = new Account("Kari", "kari@kari.no", "kari123");
            accountRepository.save(account);
            Profile profile = new Profile(account.getFirstname(), "", false, account.getId());
            profileRepository.save(profile);
        }

    }
}