package edu.ntnu.idatt210602.matsvinnbackend.startup;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;

import edu.ntnu.idatt210602.matsvinnbackend.repo.IngredientRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import edu.ntnu.idatt210602.matsvinnbackend.repo.ItemRepository;

@Component
public class CreateDefaultItemsAndRecipes implements CommandLineRunner {

    @Autowired
    ItemRepository itemRepo;

    @Autowired
    RecipeRepository recipeRepo;

    @Autowired
    IngredientRepository ingredientRepo;

    @Override
    public void run(String... args) throws Exception {
        if (itemRepo.count() > 0 && recipeRepo.count() > 0)
            return;


        ///////////
        // ITEM //
        /////////
        itemRepo.save(
                new Item(
                        "Vegansk Krone-Is Jordbær",
                        "7041012550001",
                        "https://bilder.ngdata.no/7041012550001/meny/large.jpg",
                        30,
                        new SerializedAmount(
                                4,
                                "stk"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Nøtter (kan inneholde spor)"));
                                add(new Allergen("Peanøtter (kan inneholde spor)"));
                                add(new Allergen("Soya (kan inneholde spor)"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(264.0, "stk")));
                                add(new Nutrition("Energi", new SerializedAmount(1106.0, "stk")));
                                add(new Nutrition("Fett", new SerializedAmount(11.2, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(38.7, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(9.5, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(1.5, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.11, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(23.6, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Jarlsberg Gulost",
                        "7038010053368",
                        "https://bilder.ngdata.no/7038010053368/kmh/large.jpg",
                        14,
                        new SerializedAmount(
                                700,
                                "g"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Melk"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(351.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(1458.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(27.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(17.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(27.0, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(1.1, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Q Lettmelk",
                        "",
                        "https://engrosnett.no/image/melk/2474732-2474732.jpg?lb=true&v=638157803293530000",
                        8,
                        new SerializedAmount(
                                1,
                                "l"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Melk"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(41.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(173.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(1.0, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(4.0, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(3.0, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(4.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Hjertesalat",
                        "7040512565881",
                        "https://bilder.ngdata.no/7040512565881/meny/large.jpg",
                        5,
                        new SerializedAmount(
                                2,
                                "stk"),
                        new ArrayList<>() {
                            {
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(15.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(64.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(1.4, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(1.2, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Superbrød",
                        "7025165002457",
                        "https://cdcimg.coop.no/rte/RTE2/7025165002457.png",
                        10,
                        new SerializedAmount(
                                1,
                                "stk"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Gluten"));
                                add(new Allergen("Nøtter (kan inneholde spor)"));
                                add(new Allergen("Sesam (kan inneholde spor)"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(255.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(1068.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(3.7, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(45.0, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(5.6, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.6, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(7.3, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.9, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(1.3, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Hvetemel",
                        "7044416015367",
                        "https://cdcimg.coop.no/rte/RTE2/7044416015367.png",
                        180,
                        new SerializedAmount(
                                1,
                                "kg"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Gluten"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(350.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(1450.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(2.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(68.0, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(3.3, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.3, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(12.0, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(1.7, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Laksefilet",
                        "7025110142122",
                        "https://cdcimg.coop.no/rte/RTE2/7025110142122.png",
                        5,
                        new SerializedAmount(
                                250,
                                "g"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(200.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(834.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(4.9, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(13.4, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(2.5, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(2.2, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(19.9, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Erter (frosne)",
                        "7025110065407",
                        "https://cdcimg.coop.no/rte/RTE2/7025110065407.png",
                        180,
                        new SerializedAmount(
                                800,
                                "g"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(68.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(284.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(0.4, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(8.0, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(5.5, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(5.2, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(4.8, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Brokkoli",
                        "94349",
                        "https://res.cloudinary.com/norgesgruppen/image/upload/v1647661227/Product/94349.jpg",
                        14,
                        new SerializedAmount(
                                400,
                                "g"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(28.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(119.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(0.3, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.2, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(1.9, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(2.7, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(3.2, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Stivelse", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Appelsinjuice",
                        "",
                        "https://bilder.kolonial.no/local_products/d140c271-0630-4d7a-a24e-e05e21c4ec06.jpeg?auto=format&fit=max&w=233&s=d1bfc70b77f54a043765a906221b47ef",
                        90,
                        new SerializedAmount(
                                1,
                                "l"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(49.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(206.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(0.5, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.4, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(9.6, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(1.0, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(9.6, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Tomat",
                        "7040512550214",
                        "https://bilder.ngdata.no/7040512550818/meny/large.jpg",
                        10,
                        new SerializedAmount(
                                4,
                                "stk"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(18.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(74.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(0.2, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(2.5, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(0.7, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Stivelse", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Laktosefri Lettrømme",
                        "7038010040351",
                        "https://cdcimg.coop.no/rte/RTE2/7038010040351.png",
                        20,
                        new SerializedAmount(
                                300,
                                "g"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Melk"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(179.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(741.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(17.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(3.8, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(11.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(2.8, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.08, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(3.7, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Bananer i Klase",
                        "4186",
                        "https://bilder.ngdata.no/4186/kiwi/large.jpg",
                        7,
                        new SerializedAmount(
                                5,
                                "stk"),
                        new ArrayList<>() {
                            {
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(83.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(352.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(0.3, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(18.1, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(1.6, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(1.1, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Stivelse", new SerializedAmount(4.4, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Kjøttdeig av Storfe",
                        "7037203627263",
                        "https://cdcimg.coop.no/rte/RTE2/7037203627263.png",
                        7,
                        new SerializedAmount(
                                400,
                                "g"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(194.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(807.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(6.1, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(14.0, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.7, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(7.2, "g")));
                                add(new Nutrition("Polyoler", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(17.0, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(1.1, "g")));
                                add(new Nutrition("Stivelse", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Kjøttbuljong i terninger",
                        "7340011403272",
                        "https://cdcimg.coop.no/rte/RTE2/7340011403272.png",
                        365,
                        new SerializedAmount(
                                12,
                                "stk"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Selleri (kan inneholde spor)"));
                                add(new Allergen("Soya (kan inneholde spor)"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(6.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(23.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(0.4, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(0.4, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.3, "g")));
                                add(new Nutrition("Polyoler", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.9, "g")));
                                add(new Nutrition("Stivelse", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.4, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Hodekål",
                        "3050",
                        "https://nettbutikk.bunnpris.no/itemImages/3050_0_f.png",
                        14,
                        new SerializedAmount(
                                1.5,
                                "kg"),
                        new ArrayList<>(),
                        new ArrayList<>()));

        itemRepo.save(
                new Item(
                        "Nypotet",
                        "2000567400006",
                        "https://cdcimg.coop.no/rte/RTE2/2000567400006.png",
                        90,
                        new SerializedAmount(
                                1.5,
                                "kg"),
                        new ArrayList<>(),
                        new ArrayList<>()));

        itemRepo.save(
                new Item(
                        "Egg (18pk)",
                        "7025110184344",
                        "https://cdcimg.coop.no/rte/RTE2/7025110184344.png",
                        21,
                        new SerializedAmount(
                                18,
                                "stk"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Egg"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(149.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(620.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(11.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(0.3, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(3.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(13.0, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.4, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.3, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Løk",
                        "7040514001233",
                        "https://nettbutikk.bunnpris.no/itemImages/7040514001233_0_f.png",
                        18,
                        new SerializedAmount(
                                4,
                                "stk"),
                        new ArrayList<>(),
                        new ArrayList<>()));

        itemRepo.save(
                new Item(
                        "Rødløk",
                        "7040514501634",
                        "https://nettbutikk.bunnpris.no/itemImages/7040514501634_0_f.png",
                        18,
                        new SerializedAmount(
                                2,
                                "stk"),
                        new ArrayList<>(),
                        new ArrayList<>()));

        itemRepo.save(
                new Item(
                        "Spinat (frossen)",
                        "7026510013456",
                        "https://engrosnett.no/image/frukt-og-gront/fryste-baer-og-gronnsaker/1132950-1132950.jpg?lb=true&v=638180967347830000",
                        365,
                        new SerializedAmount(
                                1,
                                "kg"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(19.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(79.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(2.0, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Grønnsaksbuljong i terninger",
                        "7340011403265",
                        "https://cdcimg.coop.no/rte/RTE2/7340011403265.png",
                        365,
                        new SerializedAmount(
                                12,
                                "stk"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Selleri"));
                                add(new Allergen("Soya (kan inneholde spor)"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(7.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(27.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(0.5, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(0.4, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.4, "g")));
                                add(new Nutrition("Polyoler", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(0.11, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.9, "g")));
                                add(new Nutrition("Stivelse", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.1, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Purreløk",
                        "4374",
                        "https://bilder.ngdata.no/4374/kiwi/large.jpg",
                        14,
                        new SerializedAmount(
                                500,
                                "g"),
                        new ArrayList<>() {
                            {
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(28.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(118.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(0.2, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(3.6, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(2.8, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(1.6, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Stivelse", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Laktosefri Creme Fraiche",
                        "7038010060243",
                        "https://bilder.ngdata.no/7038010060243/kmh/large.jpg",
                        14,
                        new SerializedAmount(
                                300,
                                "g"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Melk"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(335.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(1381.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(35.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(2.9, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(22.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(2.2, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.07, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(2.8, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Kjøttboller",
                        "7025110097835",
                        "https://cdcimg.coop.no/rte/RTE2/7025110097835.png",
                        30,
                        new SerializedAmount(
                                1,
                                "kg"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(206.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(858.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(14.0, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(9.0, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(4.76, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(11.0, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(1.6, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(1.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Paprika",
                        "4682",
                        "https://bilder.ngdata.no/4682/kmh/large.jpg",
                        7,
                        new SerializedAmount(
                                1,
                                "stk"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(21.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(88.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(0.3, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.2, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(2.8, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(2.0, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(0.8, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Stivelse", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Pasta Penne",
                        "",
                        "https://bilder.kolonial.no/local_products/e86d8310-28c8-4518-85bf-15038bbc1979.jpeg?auto=format&fit=max&w=263&s=95a38362ae6c981fda12bddab0f876d6",
                        180,
                        new SerializedAmount(
                                500,
                                "g"),
                        new ArrayList<>() {
                            {
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(359.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(1522.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(0.2, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(1.4, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(0.6, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(73.0, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(3.2, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.2, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(12.0, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.02, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(3.1, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Hakkede Tomater",
                        "7311041084526",
                        "https://cdcimg.coop.no/rte/RTE2/7340011476054.png",
                        90,
                        new SerializedAmount(
                                390,
                                "g"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(20.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(85.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(3.5, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(0.8, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkeralkoholer", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(1.1, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.08, "g")));
                                add(new Nutrition("Stivelse", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(3.5, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Tomatpure",
                        "7022810470647",
                        "https://images2.europris.no/produkter/vw800/198235/198235_main.webp",
                        365,
                        new SerializedAmount(
                                150,
                                "g"),
                        new ArrayList<>(),
                        new ArrayList<>()));

        itemRepo.save(
                new Item(
                        "Spaghetti",
                        "7044416015626",
                        "https://bilder.ngdata.no/7044416015626/meny/large.jpg",
                        365,
                        new SerializedAmount(
                                800,
                                "g"),
                        new ArrayList<>() {
                            {
                                add(new Allergen("Gluten"));
                            }
                        },
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(304.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(1502.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(1.5, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(60.0, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(3.0, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.3, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(11.0, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.03, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(2.4, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Gulrot",
                        "7031540000352",
                        "https://bilder.ngdata.no/7040513001753/kiwi/large.jpg",
                        14,
                        new SerializedAmount(
                                1,
                                "kg"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(36.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(152.0, "kj")));
                                add(new Nutrition("Fett", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(6.7, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.0, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(0.7, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(0.1, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                            }
                        }));

        itemRepo.save(
                new Item(
                        "Fiskekaker",
                        "",
                        "https://bilder.kolonial.no/local_products/38597dee-915a-44e8-99db-5a5fc9a6926c.jpeg?auto=format&fit=max&w=376&s=45418fa0a305b733b154badc667fcee6",
                        21,
                        new SerializedAmount(
                                1,
                                "kg"),
                        new ArrayList<>(),
                        new ArrayList<>() {
                            {
                                add(new Nutrition("Kalorier", new SerializedAmount(145.0, "kcal")));
                                add(new Nutrition("Energi", new SerializedAmount(607.0, "kj")));
                                add(new Nutrition("Enumettet fett", new SerializedAmount(4.2, "g")));
                                add(new Nutrition("Fett", new SerializedAmount(7.7, "g")));
                                add(new Nutrition("Flerumettet fett", new SerializedAmount(2.1, "g")));
                                add(new Nutrition("Karbohydrater", new SerializedAmount(9.6, "g")));
                                add(new Nutrition("Kostfiber", new SerializedAmount(0.5, "g")));
                                add(new Nutrition("Mettet fett", new SerializedAmount(0.7, "g")));
                                add(new Nutrition("Protein", new SerializedAmount(9.1, "g")));
                                add(new Nutrition("Salt", new SerializedAmount(1.1, "g")));
                                add(new Nutrition("Sukkerarter", new SerializedAmount(0.5, "g")));
                            }
                        }));
        itemRepo.save(
                new Item(
                        "Jasminris",
                        "7311041013380",
                        "https://bilder.ngdata.no/7311041013380/kmh/large.jpg",
                        730,
                        new SerializedAmount(
                                1,
                                "kg"
                        ),
                        new ArrayList<>() {{
                        }},
                        new ArrayList<>() {{
                            add(new Nutrition("Kalorier", new SerializedAmount(352.0, "kcal")));
                            add(new Nutrition("Energi", new SerializedAmount(1493.0, "kj")));
                            add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                            add(new Nutrition("Fett", new SerializedAmount(1.1, "g")));
                            add(new Nutrition("Karbohydrater", new SerializedAmount(78.9, "g")));
                            add(new Nutrition("Kostfiber", new SerializedAmount(0.7, "g")));
                            add(new Nutrition("Mettet fett", new SerializedAmount(0.5, "g")));
                            add(new Nutrition("Sukkeralkoholer", new SerializedAmount(0.0, "g")));
                            add(new Nutrition("Protein", new SerializedAmount(6.2, "g")));
                            add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                            add(new Nutrition("Stivelse", new SerializedAmount(78.9, "g")));
                            add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                        }}));

        itemRepo.save(
                new Item(
                        "Salsa/tacosaus",
                        "7035620045912",
                        "https://bilder.ngdata.no/7035620045912/kiwi/large.jpg",
                        7,
                        new SerializedAmount(
                                230,
                                "g"
                        ),
                        new ArrayList<>() {{
                        }},
                        new ArrayList<>() {{
                            add(new Nutrition("Kalorier", new SerializedAmount(33.0, "kcal")));
                            add(new Nutrition("Energi", new SerializedAmount(139.0, "kj")));
                            add(new Nutrition("Fett", new SerializedAmount(0.7, "g")));
                            add(new Nutrition("Karbohydrater", new SerializedAmount(4.3, "g")));
                            add(new Nutrition("Kostfiber", new SerializedAmount(2.4, "g")));
                            add(new Nutrition("Mettet fett", new SerializedAmount(0.1, "g")));
                            add(new Nutrition("Sukkeralkoholer", new SerializedAmount(0.0, "g")));
                            add(new Nutrition("Protein", new SerializedAmount(1.2, "g")));
                            add(new Nutrition("Salt", new SerializedAmount(0.98, "g")));
                            add(new Nutrition("Stivelse", new SerializedAmount(1.4, "g")));
                            add(new Nutrition("Sukkerarter", new SerializedAmount(2.9, "g")));
                        }}));
        itemRepo.save(
                new Item(
                        "Kokosmelk",
                        "7035620025181",
                        "https://bilder.ngdata.no/7035620025181/kmh/large.jpg",
                        30,
                        new SerializedAmount(
                                250,
                                "ml"
                        ),
                        new ArrayList<>() {{
                        }},
                        new ArrayList<>() {{
                            add(new Nutrition("Kalorier", new SerializedAmount(182.0, "kcal")));
                            add(new Nutrition("Energi", new SerializedAmount(749.0, "kj")));
                            add(new Nutrition("Fett", new SerializedAmount(18.0, "g")));
                            add(new Nutrition("Karbohydrater", new SerializedAmount(3.7, "g")));
                            add(new Nutrition("Kostfiber", new SerializedAmount(0.0, "g")));
                            add(new Nutrition("Mettet fett", new SerializedAmount(15.0, "g")));
                            add(new Nutrition("Sukkeralkoholer", new SerializedAmount(0.0, "g")));
                            add(new Nutrition("Protein", new SerializedAmount(1.2, "g")));
                            add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                            add(new Nutrition("Stivelse", new SerializedAmount(1.4, "g")));
                            add(new Nutrition("Sukkerarter", new SerializedAmount(2.3, "g")));
                        }}));

        itemRepo.save(
                new Item(
                        "Egg",
                        "7039610009021",
                        "https://bilder.ngdata.no/7039610009021/meny/large.jpg",
                        90,
                        new SerializedAmount(
                                12,
                                "stk"
                        ),
                        new ArrayList<>() {{
                            add(new Allergen("Egg"));
                        }},
                        new ArrayList<>() {{
                            add(new Nutrition("Kalorier", new SerializedAmount(149.0, "kcal")));
                            add(new Nutrition("Energi", new SerializedAmount(620.0, "kj")));
                            add(new Nutrition("Fett", new SerializedAmount(11.0, "g")));
                            add(new Nutrition("Karbohydrater", new SerializedAmount(0.3, "g")));
                            add(new Nutrition("Mettet fett", new SerializedAmount(3.0, "g")));
                            add(new Nutrition("Protein", new SerializedAmount(13.0, "g")));
                            add(new Nutrition("Salt", new SerializedAmount(0.4, "g")));
                            add(new Nutrition("Sukkerarter", new SerializedAmount(0.3, "g")));
                        }}));

        itemRepo.save(
                new Item(
                        "Egg Økologisk",
                        "7090015509004",
                        "https://bilder.ngdata.no/7090015509004/meny/large.jpg",
                        90,
                        new SerializedAmount(
                                6,
                                "stk"
                        ),
                        new ArrayList<>() {{
                            add(new Allergen("Egg"));
                        }},
                        new ArrayList<>() {{
                            add(new Nutrition("Kalorier", new SerializedAmount(142.0, "kcal")));
                            add(new Nutrition("Energi", new SerializedAmount(590.0, "kj")));
                            add(new Nutrition("Fett", new SerializedAmount(10.1, "g")));
                            add(new Nutrition("Karbohydrater", new SerializedAmount(0.3, "g")));
                            add(new Nutrition("Mettet fett", new SerializedAmount(2.9, "g")));
                            add(new Nutrition("Protein", new SerializedAmount(12.4, "g")));
                            add(new Nutrition("Salt", new SerializedAmount(0.4, "g")));
                            add(new Nutrition("Sukkerarter", new SerializedAmount(0.3, "g")));
                        }}));
        itemRepo.save(
                new Item(
                        "Creme Fraiche 35%",
                        "7038010004926",
                        "https://bilder.ngdata.no/7038010004926/kmh/large.jpg",
                        14,
                        new SerializedAmount(
                                300,
                                "g"
                        ),
                        new ArrayList<>() {{
                            add(new Allergen("Melk"));
                        }},
                        new ArrayList<>() {{
                            add(new Nutrition("Kalorier", new SerializedAmount(335.0, "kcal")));
                            add(new Nutrition("Energi", new SerializedAmount(1381.0, "kj")));
                            add(new Nutrition("Fett", new SerializedAmount(35.0, "g")));
                            add(new Nutrition("Karbohydrater", new SerializedAmount(2.9, "g")));
                            add(new Nutrition("Mettet fett", new SerializedAmount(22.0, "g")));
                            add(new Nutrition("Protein", new SerializedAmount(2.2, "g")));
                            add(new Nutrition("Salt", new SerializedAmount(0.07, "g")));
                            add(new Nutrition("Sukkerarter", new SerializedAmount(2.9, "g")));
                        }}));
        itemRepo.save(
                new Item(
                        "Søtpotet",
                        "",
                        "https://bilder.kolonial.no/produkter/13d6cdf5-6cc8-4af9-9686-655859077cbb.jpg?auto=format&fit=max&w=500&s=0e4aff0b6e66522c865b3094cf3086a6",
                        50,
                        new SerializedAmount(
                                1,
                                "stk"
                        ),
                        new ArrayList<>() {{
                        }},
                        new ArrayList<>() {{
                        }}));

        itemRepo.save(
                new Item(
                        "Yoghurt Naturell",
                        "7038010055652",
                        "https://cdcimg.coop.no/rte/RTE2/7038010055652.png",
                        31,
                        new SerializedAmount(
                                500,
                                "g"
                        ),
                        new ArrayList<>() {{
                            add(new Allergen("Melk"));
                        }},
                        new ArrayList<>() {{
                            add(new Nutrition("Kalorier", new SerializedAmount(69.0, "kcal")));
                            add(new Nutrition("Energi", new SerializedAmount(287.0, "kj")));
                            add(new Nutrition("Fett", new SerializedAmount(3.4, "g")));
                            add(new Nutrition("Karbohydrater", new SerializedAmount(5.6, "g")));
                            add(new Nutrition("Mettet fett", new SerializedAmount(2.3, "g")));
                            add(new Nutrition("Protein", new SerializedAmount(4.1, "g")));
                            add(new Nutrition("Salt", new SerializedAmount(0.13, "g")));
                            add(new Nutrition("Sukkerarter", new SerializedAmount(5.6, "g")));
                        }}));
        itemRepo.save(
                new Item(
                        "Avokado",
                        "7040511504751",
                        "https://nettbutikk.bunnpris.no/itemImages/7040511504751_0_f.png",
                        2,
                        new SerializedAmount(
                                1,
                                "stk"
                        ),
                        new ArrayList<>() {{
                        }},
                        new ArrayList<>() {{
                        }}));
        itemRepo.save(
                new Item(
                        "Kylling Lårfilet",
                        "7090013751511",
                        "https://bilder.ngdata.no/7090013751511/meny/large.jpg",
                        14,
                        new SerializedAmount(
                                2,
                                "stk"
                        ),
                        new ArrayList<>() {{
                        }},
                        new ArrayList<>() {{
                            add(new Nutrition("Kalorier", new SerializedAmount(177.0, "kcal")));
                            add(new Nutrition("Energi", new SerializedAmount(740.0, "kj")));
                            add(new Nutrition("Fett", new SerializedAmount(11.1, "g")));
                            add(new Nutrition("Karbohydrater", new SerializedAmount(0.0, "g")));
                            add(new Nutrition("Mettet fett", new SerializedAmount(2.8, "g")));
                            add(new Nutrition("Protein", new SerializedAmount(19.4, "g")));
                            add(new Nutrition("Salt", new SerializedAmount(0.2, "g")));
                            add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                        }}));
        itemRepo.save(
                new Item(
                        "Indrefilet Av Kalv hel pr Kg",
                        "2000781300007",
                        "https://res.cloudinary.com/norgesgruppen/image/upload/v1504268212/Product/2000781300007.jpg",
                        13,
                        new SerializedAmount(
                                1000,
                                "g"
                        ),
                        new ArrayList<>() {{
                        }},
                        new ArrayList<>() {{
                        }}));
        itemRepo.save(
                new Item(
                        "Fårikålkjøtt",
                        "7037204196980",
                        "https://bilder.ngdata.no/7037204196980/kiwi/large.jpg",
                        20,
                        new SerializedAmount(
                                1,
                                "kg"
                        ),
                        new ArrayList<>() {{
                        }},
                        new ArrayList<>() {{
                            add(new Nutrition("Kalorier", new SerializedAmount(234.0, "kcal")));
                            add(new Nutrition("Energi", new SerializedAmount(972.0, "kj")));
                            add(new Nutrition("Enumettet fett", new SerializedAmount(6.3, "g")));
                            add(new Nutrition("Fett", new SerializedAmount(18.0, "g")));
                            add(new Nutrition("Flerumettet fett", new SerializedAmount(1.1, "g")));
                            add(new Nutrition("Karbohydrater", new SerializedAmount(0.0, "g")));
                            add(new Nutrition("Mettet fett", new SerializedAmount(8.5, "g")));
                            add(new Nutrition("Protein", new SerializedAmount(18.0, "g")));
                            add(new Nutrition("Salt", new SerializedAmount(0.2, "g")));
                            add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                        }}));
        itemRepo.save(
        new Item(
                "Blomkål",
                "94079",
                "https://bilder.ngdata.no/94079/kiwi/large.jpg",
                30,
                new SerializedAmount(
                        500,
                        "g"
                ),
                new ArrayList<>() {{
                }},
                new ArrayList<>() {{
                    add(new Nutrition("Kalorier", new SerializedAmount(23.0, "kcal")));
                    add(new Nutrition("Energi", new SerializedAmount(97.0, "kj")));
                    add(new Nutrition("Enumettet fett", new SerializedAmount(0.0, "g")));
                    add(new Nutrition("Fett", new SerializedAmount(0.2, "g")));
                    add(new Nutrition("Flerumettet fett", new SerializedAmount(0.1, "g")));
                    add(new Nutrition("Karbohydrater", new SerializedAmount(2.3, "g")));
                    add(new Nutrition("Kostfiber", new SerializedAmount(2.3, "g")));
                    add(new Nutrition("Mettet fett", new SerializedAmount(0.0, "g")));
                    add(new Nutrition("Sukkeralkoholer", new SerializedAmount(0.0, "g")));
                    add(new Nutrition("Protein", new SerializedAmount(1.9, "g")));
                    add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                    add(new Nutrition("Stivelse", new SerializedAmount(0.0, "g")));
                    add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                }}));

        itemRepo.save(
                new Item(
                        "Spisskål",
                        "7040513909523",
                        "https://res.cloudinary.com/norgesgruppen/image/upload/v1539394811/Product/7040513909523.jpg",
                        30,
                        new SerializedAmount(
                                600,
                                "g"
                        ),
                        new ArrayList<>() {{
                        }},
                        new ArrayList<>() {{
                            add(new Nutrition("Kalorier", new SerializedAmount(18.0, "kcal")));
                            add(new Nutrition("Energi", new SerializedAmount(77.0, "kj")));
                            add(new Nutrition("Fett", new SerializedAmount(0.3, "g")));
                            add(new Nutrition("Karbohydrater", new SerializedAmount(0.9, "g")));
                            add(new Nutrition("Mettet fett", new SerializedAmount(0.1, "g")));
                            add(new Nutrition("Protein", new SerializedAmount(2.1, "g")));
                            add(new Nutrition("Salt", new SerializedAmount(0.0, "g")));
                            add(new Nutrition("Sukkerarter", new SerializedAmount(0.0, "g")));
                        }}));


















        //////////////
        // RECIPES //
        /////////////

        ArrayList<Ingredient> ingredients = new ArrayList<>();

        //1
        Ingredient ingredient1 = new Ingredient(itemRepo.findByName("Løk"), new SerializedAmount(1, "stk"));
        Ingredient ingredient2 = new Ingredient(itemRepo.findByName("Spinat (frossen)"), new SerializedAmount(300, "g"));
        Ingredient ingredient3 = new Ingredient(itemRepo.findByName("Grønnsaksbuljong i terninger"), new SerializedAmount(2, "stk"));
        Ingredient ingredient4 = new Ingredient(itemRepo.findByName("Laktosefri Creme Fraiche"), new SerializedAmount(100, "g"));
        ingredientRepo.save(ingredient1);
        ingredientRepo.save(ingredient2);
        ingredientRepo.save(ingredient3);
        ingredientRepo.save(ingredient4);
        ingredients.add(ingredient1);
        ingredients.add(ingredient2);
        ingredients.add(ingredient3);
        ingredients.add(ingredient4);

        recipeRepo.save(
                new Recipe("Spinatsuppe med egg", "Det er ikke bare Skipper`n som har godt av spinat. En klassisk spinatsuppe med kokt egg er overraskende godt.",
                        "20–40 min",
                        ingredients,
                        "1. Kok egg i 8-10 minutter. Avkjøl dem av i kaldt vann, og ta av skallet.\n" +
                                "2. Hakk løk, og ha den i en varm kjele med olivenolje sammen med tint spinat. La det frese et par minutter.\n" +
                                "3. Tilsett vann, buljong og purre. La det koke i ca. 5 minutter.\n" +
                                "4. Tilsett crème fraîche og kjør suppen jevn med en stavmikser eller i en hurtigmikser. Smak til med salt og pepper.\n"));


        ingredients.clear();

        //2
        ingredient1 = new Ingredient(itemRepo.findByName("Kjøttdeig av Storfe"), new SerializedAmount(400, "g"));
        ingredient2 = new Ingredient(itemRepo.findByName("Q Lettmelk"), new SerializedAmount(2, "dl"));
        ingredient3 = new Ingredient(itemRepo.findByName("Hodekål"), new SerializedAmount(700, "g"));
        ingredient4 = new Ingredient(itemRepo.findByName("Nypotet"), new SerializedAmount(500, "g"));
        ingredientRepo.save(ingredient1);
        ingredientRepo.save(ingredient2);
        ingredientRepo.save(ingredient3);
        ingredientRepo.save(ingredient4);
        ingredients.add(ingredient1);
        ingredients.add(ingredient2);
        ingredients.add(ingredient3);
        ingredients.add(ingredient4);

        recipeRepo.save(
                new Recipe("Kjøttkaker med kålstuing", "Kjøttkaker til middag er god tradisjonsmat. Her servert med kokte poteter, kålstuing og tyttebær. Ta frem foodprosessoren og lag dette knallgode måltidet!",
                        "40–60 min",
                        ingredients,
                        "\n" +
                                "    Sett over poteter til koking og kok dem til de er møre.\n" +
                                "    Bruk en foodprosessor. Ha alle ingrediensene i bollen og kjør ca. 10-20 sekunder til farsen er jevn og fin. \n" +
                                "\n" +
                                "    Form farsen til runde kaker ved hjelp av en skje, hånden din, og kaldt vann.\n" +
                                "    Ha smør i en middels varm stekepanne, og stek kakene i ca. 2 minutter på hver side til de har fått en fin bruning.\n" +
                                "    Smelt smør i en kjele til brun saus. Tilsett hvetemel og brun det over svak varme til blandingen får en nøttebrun farge. Spe med varm kraft eller buljong, og rør godt mellom hver gang du sper. La sausen småkoke i ca. 10 minutter. Smak til med salt og pepper.\n" +
                                "    Ha kakene over i brun saus og la dem trekke i ca. 10 minutter (til de er gjennomkokt), eller til du er ferdig med resten av komponentene i retten.\n" +
                                "    Del kålen i terninger og kok den i lettsaltet vann i ca. 15 minutter, til den er gjennomkokt.\n" +
                                "    Smelt smør i en kjele og tilsett melet. Spe med kokekraft fra kålen, og melk til du får en litt tykk konsistens. La sausen koke ca 5 minutter. Smak til med muskat, salt og pepper, tilsett den kokte kålen. \n"));

        ingredients.clear();

        //3
        ingredient1 = new Ingredient(itemRepo.findByName("Kjøttdeig av Storfe"), new SerializedAmount(400, "g"));
        ingredient2 = new Ingredient(itemRepo.findByName("Q Lettmelk"), new SerializedAmount(2, "dl"));
        ingredient3 = new Ingredient(itemRepo.findByName("Hodekål"), new SerializedAmount(700, "g"));
        ingredient4 = new Ingredient(itemRepo.findByName("Nypotet"), new SerializedAmount(500, "g"));
        ingredientRepo.save(ingredient1);
        ingredientRepo.save(ingredient2);
        ingredientRepo.save(ingredient3);
        ingredientRepo.save(ingredient4);
        ingredients.add(ingredient1);
        ingredients.add(ingredient2);
        ingredients.add(ingredient3);
        ingredients.add(ingredient4);

        recipeRepo.save(
                new Recipe("Pasta med kjøttboller i tomatsaus", "\n" +
                        "\n" +
                        "Disse små smakfulle kjøttbollene er enkle å lage og sammen med tomatsaus blir dette en supergod hverdagsmiddag!\n",
                        "40 min",
                        ingredients,
                        "\n" +
                                "Steg 1\n" +
                                "\n" +
                                "Skyll paprika. Del den i to, ta ut frø og stilk.\n" +
                                "Steg 2\n" +
                                "\n" +
                                "Skjær den i små biter. \n" +
                                "Steg 3\n" +
                                "\n" +
                                "Sett stekepannen på platen, skru på middels varme. Ha i margarin.\n \n" +
                                "Steg 4\n" +
                                "\n" +
                                "Åpne pakken med kjøttboller og ha dem i stekepannen når margarinen slutter å bruse. Stek kjøttbollene i 5 minutter. \n" +
                                "\n" +
                                "Steg 5\n" +
                                "\n" +
                                "Hell i pastasaus og skru ned varmen på det laveste. Rør av og til med stekespaden.\n \n" +
                                "Steg 6\n" +
                                "\n" +
                                "Sett en stor kjele på kokeplaten, bruk et desilitermål eller en mugge og fyll kjelen halvfull med vann. Skru platen på fullt.\n \n" +
                                "Steg 7\n" +
                                "\n" +
                                "Les på pastapakken og finn ut hvor lenge pastaen skal koke.\n \n" +
                                "Steg 8\n" +
                                "\n" +
                                "Ha salt i vannet.\n\n" +
                                "Steg 9\n" +
                                "\n" +
                                "Bruk et målebeger og mål opp så mye pasta du trenger.\n \n" +
                                "Steg 10\n" +
                                "\n" +
                                "Ha pasta i det kokende vannet og rør rundt med en sleiv. Skru ned varmen til middels, slik at det ikke koker over. \n \n" +
                                "Steg 11\n" +
                                "\n" +
                                "Ha paprikabitene i stekepannen sammen med kjøttbollene. Rør rundt.\n \n" +
                                "Steg 12\n" +
                                "\n" +
                                "Skru av kokeplaten når pastaen er ferdig. Sett et dørslag på en tallerken i vasken. Hell pastaen i dørslaget, løft det opp og rist litt slik at alt vannet renner av. Sett dørslaget oppi kjelen.\n \n" +
                                "Steg 13\n" +
                                "\n" +
                                "Sett kjelen på et gryteunderlag. Fordel pasta i dype tallerkener. \n"));

        ingredients.clear();

        //4
        ingredient1 = new Ingredient(itemRepo.findByName("Grønnsaksbuljong i terninger"), new SerializedAmount(1, "stk"));
        ingredient2 = new Ingredient(itemRepo.findByName("Purreløk"), new SerializedAmount(200, "g"));
        ingredient3 = new Ingredient(itemRepo.findByName("Gulrot"), new SerializedAmount(400, "g"));
        ingredient4 = new Ingredient(itemRepo.findByName("Nypotet"), new SerializedAmount(600, "g"));
        ingredientRepo.save(ingredient1);
        ingredientRepo.save(ingredient2);
        ingredientRepo.save(ingredient3);
        ingredientRepo.save(ingredient4);
        ingredients.add(ingredient1);
        ingredients.add(ingredient2);
        ingredients.add(ingredient3);
        ingredients.add(ingredient4);

        recipeRepo.save(
                new Recipe("Grønnsakssuppe", "\n" +
                        "\n" +
                        "Kjempe god grønnsakssuppe\n",
                        "30 min",
                        ingredients,
                        "Kutt opp grønnsakene og kok de opp sammen med buljongen. Krydre etter smak."));

        ingredients.clear();

        //5
        ingredient1 = new Ingredient(itemRepo.findByName("Laksefilet"), new SerializedAmount(250, "g"));
        ingredient2 = new Ingredient(itemRepo.findByName("Brokkoli"), new SerializedAmount(200, "g"));
        ingredient3 = new Ingredient(itemRepo.findByName("Hvetemel"), new SerializedAmount(500, "g"));
        ingredient4 = new Ingredient(itemRepo.findByName("Q Lettmelk"), new SerializedAmount(200, "ml"));
        ingredientRepo.save(ingredient1);
        ingredientRepo.save(ingredient2);
        ingredientRepo.save(ingredient3);
        ingredientRepo.save(ingredient4);
        ingredients.add(ingredient1);
        ingredients.add(ingredient2);
        ingredients.add(ingredient3);
        ingredients.add(ingredient4);

        recipeRepo.save(
                new Recipe("Laks med hvitsaus og brokkoli", "\n" +
                        "\n" +
                        "Laksefilet med hvitsaus og brokkoli.",
                        "50 min",
                        ingredients,
                        "Lag hvitsaus og stek laksen med sausen og brokkolien."));

        ingredients.clear();


        //6
        ingredient1 = new Ingredient(itemRepo.findByName("Hjertesalat"), new SerializedAmount(1, "stk"));
        ingredient2 = new Ingredient(itemRepo.findByName("Erter (frosne)"), new SerializedAmount(200, "g"));
        ingredient3 = new Ingredient(itemRepo.findByName("Laktosefri Creme Fraiche"), new SerializedAmount(200, "g"));
        ingredientRepo.save(ingredient1);
        ingredientRepo.save(ingredient2);
        ingredientRepo.save(ingredient3);
        ingredients.add(ingredient1);
        ingredients.add(ingredient2);
        ingredients.add(ingredient3);

        recipeRepo.save(
                new Recipe("Salat med erter og creme fraiche", "Kjempe god salat",
                        "20 min",
                        ingredients,
                        "Bland alt sammen og kryddre etter smak."));

        ingredients.clear();

        //7
        ingredient1 = new Ingredient(itemRepo.findByName("Hjertesalat"), new SerializedAmount(1, "stk"));
        ingredient2 = new Ingredient(itemRepo.findByName("Tomat"), new SerializedAmount(4, "stk"));
        ingredientRepo.save(ingredient1);
        ingredientRepo.save(ingredient2);
        ingredients.add(ingredient1);
        ingredients.add(ingredient2);

        recipeRepo.save(
                new Recipe("Tomatsalat", "\n" +
                        "\n" +
                        "Kjempegod tomatsalat.",
                        "50 min",
                        ingredients,
                        "Kutt opp salaten og tomatene og bland sammen. Kryddre med salt, pepper og olivenolje."));

        ingredients.clear();

    }
}
