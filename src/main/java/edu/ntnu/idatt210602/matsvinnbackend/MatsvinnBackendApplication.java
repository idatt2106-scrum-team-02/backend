package edu.ntnu.idatt210602.matsvinnbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.Generated;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
public class MatsvinnBackendApplication {

	@Generated
	public static void main(String[] args) {
		SpringApplication.run(MatsvinnBackendApplication.class, args);
	}

}
