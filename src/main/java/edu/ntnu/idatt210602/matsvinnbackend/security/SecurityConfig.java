package edu.ntnu.idatt210602.matsvinnbackend.security;

import edu.ntnu.idatt210602.matsvinnbackend.service.AccountService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class SecurityConfig {
    @Bean
    public UserDetailsService userDetailsService() {
        return new AccountService();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .cors().and()
                .authorizeHttpRequests()
                // Allow all OPTIONS requests from browsers
                .requestMatchers(HttpMethod.OPTIONS).permitAll()

                // ITEM ENDPOINTS
                .requestMatchers(HttpMethod.GET, "/item").permitAll()
                .requestMatchers(HttpMethod.GET, "/item/*").permitAll()

                // RECIPE ENDPOINTS
                .requestMatchers(HttpMethod.GET, "/recipe").permitAll()
                .requestMatchers(HttpMethod.GET, "/recipe/*").permitAll()

                //ACCOUNT ENDPOINTS
                .requestMatchers(HttpMethod.POST, "/account").permitAll()
                .requestMatchers(HttpMethod.DELETE, "/account").authenticated()
                .requestMatchers(HttpMethod.PUT, "/account/*").authenticated()
                .requestMatchers(HttpMethod.GET, "/account/*").authenticated()

                //PROFILE ENDPOINTS
                .requestMatchers(HttpMethod.POST, "/profile").authenticated()
                .requestMatchers(HttpMethod.DELETE, "/profile/*").authenticated()
                .requestMatchers(HttpMethod.PUT, "/profile/*").authenticated()
                .requestMatchers(HttpMethod.GET, "/profile/*").authenticated()
                .requestMatchers(HttpMethod.GET, "/profile").authenticated()

                //SHOPPING LIST ENDPOINTS
                .requestMatchers(HttpMethod.PUT,"/shoppinglist/*").authenticated()
                .requestMatchers(HttpMethod.GET,"/shoppinglist").authenticated()
                .requestMatchers(HttpMethod.GET,"/shoppinglist/*").authenticated()
                .requestMatchers(HttpMethod.DELETE,"/shoppinglist/*").authenticated()

                //FRIDGE ENDPOINTS
                .requestMatchers(HttpMethod.POST, "/fridge").authenticated()
                .requestMatchers(HttpMethod.POST, "/fridge/items").authenticated()
                .requestMatchers(HttpMethod.PUT, "/fridge/ingredients").authenticated()
                .requestMatchers(HttpMethod.PUT, "/fridge/ingredient").authenticated()
                .requestMatchers(HttpMethod.GET, "/fridge").authenticated()

                // The login endpoint is open to anyone
                .requestMatchers("/login").permitAll()

                // WEEK MENU ENDPOINTS
                .requestMatchers(HttpMethod.PUT, "/weekmenu/*").authenticated()
                .requestMatchers(HttpMethod.GET, "/weekmenu").authenticated()

                //FILE ENDPOINTS
                .requestMatchers(HttpMethod.GET, "/img/*").permitAll()
                .requestMatchers(HttpMethod.POST, "/img").authenticated()
                .requestMatchers(HttpMethod.DELETE, "/img/*").authenticated()

                .anyRequest().authenticated().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .addFilterBefore(new AuthFilter(), UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
}
