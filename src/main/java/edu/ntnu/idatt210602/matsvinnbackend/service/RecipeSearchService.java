package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.model.Account;
import edu.ntnu.idatt210602.matsvinnbackend.model.Ingredient;
import edu.ntnu.idatt210602.matsvinnbackend.model.Recipe;
import edu.ntnu.idatt210602.matsvinnbackend.model.SerializedAmount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class RecipeSearchService {

    @Autowired
    ShoppingListService shoppingListService;

    /**
     * This method fetches all recipes and filters out invalid recipes by ingredient (based on the ean of the product)
     *
     * @complexity O(n*m), n number of recipes, m ingredients per recipe
     * @param ingredient which the recipes should use
     * @return List<Recipe> that contains recipes using this single ingredient
     */
    public List<Recipe> getRecipesWithIngredient(Ingredient ingredient, List<Recipe> recipes){
        //List<Recipe> recipes = recipeRepository.findAll();
        List<Recipe> validRecipes = new ArrayList<>();
        for (Recipe recipe:recipes) {
            for (Ingredient ingredient1:recipe.getIngredient()) {
                // equal ean numbers means exactly same ingredient
                // This might be done better by exchanging EAN for EXTREMELY specific
                // Ingredient/category, ie. Gulost, Cod, parsnip, etc.
                // This would avoid situations where multiple products are interchangeable
                if (ingredient.getItem().getEan().equals(ingredient1.getItem().getEan())) validRecipes.add(recipe);
            }
        }
        return validRecipes;
    }

    /**
     * Gets list of recipes that use a subset of the provided ingredients
     *
     * @param ingredients that the recipes can use
     * @param recipes that need filtering
     * @return list of recipes that use a subset of the provided ingredients
     */
    public List<Recipe> getRecipesWithIngredients(List<Ingredient> ingredients, List<Recipe> recipes){
        //List<Recipe> recipes = recipeRepository.findAll();
        List<Recipe> validRecipes = new ArrayList<>();
        for (Recipe recipe: recipes) {
            int target = recipe.getIngredient().size();
            int current = 0;
            for (Ingredient recIng:recipe.getIngredient()) {
                for (Ingredient ing:ingredients) {
                    //if ingredient match has been found
                    if (ing.getItem().getEan().equals(recIng.getItem().getEan())){
                        current +=1;
                        break;
                    }
                }
            }
            if (target == current) validRecipes.add(recipe);
        }

        return validRecipes;
    }

    /**
     * This method checks the amount of ingredients available for a recipe.
     * @param ingredients the list of ingredients
     * @param recipe the recipe
     * @return an int
     */
    private int getNumberOfMatches(List<Ingredient> ingredients, Recipe recipe, Integer people){
        int result = 0;
        //GROUP UP FREE INGREDIENTS FOR VALIDITY CHECKS
        List<Ingredient> groupedIngredients = groupIngredients(ingredients); //grouped without exp_date
        for (Ingredient i:recipe.getIngredient()) {
            for (Ingredient groupedIngredient : groupedIngredients) {
                if (i.getItem().getEan().equals(groupedIngredient.getItem().getEan())
                        && i.getAmount().getQuantity() * people >= groupedIngredient.getAmount().getQuantity()
                        && i.getStatus() == Ingredient.Status.FREE) {
                    result += 1;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Get recipes sorted by matching the most ingredients.
     *
     * This method requires ingredient enum to be set correctly
     * @param ingredients that the recipes should contain
     * @param recipes to search through
     * @return list of recipes that match most ingredients
     */
    public List<Recipe> getMostMatches(List<Ingredient> ingredients, List<Recipe> recipes, Integer people){
        recipes.sort(Comparator.comparing(o ->-1*getNumberOfMatches(ingredients, o, people)));
        return recipes;//Best matches, all ingredients match
    }
    // getBestMatches() could possibly be used to find best
    // matches that use multiple soon-to-expire ingredients

    /**
     * This method gets recipes that use the ingredient which is closest to expiry.
     *
     * This implementation only cares about one ingredient
     * @param ingredients to compare
     * @param recipes to look through
     * @return list of recipes that use the ingredient which is closest to expiry
     */
    public List<Recipe> getRecipesByIngExpiry(List<Ingredient> ingredients, List<Recipe> recipes){
        List<Recipe> validRecipes = new ArrayList<>();

        //Ingredients sorted by increasing time til expiry
        ArrayList<Ingredient> input = (ArrayList<Ingredient>) ingredients;
        input.sort(Comparator.comparing(o -> o.getExp_date().getTime()));

        //This only gets recipes with the ingredient closest to expiry,
        // does not consider multiple ingredients close to expiry
        if (input.size() > 0) validRecipes.addAll(getRecipesWithIngredient(input.get(0), recipes));
        return validRecipes;
    }

    /**
     * gets multiple recipe suggestions based of the provided list of ingredient and recipes.
     *
     * This is the main recipe-provider method,
     * it should provide a well fitting set of
     * recipes for the given amount of days.
     *
     * The Ingredient entity has an enumerator
     * which should be used to distinguish
     * between free and taken ingredients.
     * @param ingredients the ingredients
     * @param recipes a list of recipes
     * @param days number of days
     * @param people number of people to get suggestions for
     * @return Well fitting list of recipes
     */
    public List<Recipe> getRecipeSuggestions(Account account, List<Ingredient> ingredients, List<Recipe> recipes, Integer days, Integer people){
        List<Recipe> validRecipes = new ArrayList<>();

        //Suggest single recipe repeatedly
        for (int i = 0; i < days; i++) {
            // claim as it selects recipes
            Recipe suggestedRecipe = getMostMatches(ingredients, recipes, people).get(0);
            validRecipes.add(suggestedRecipe);
            shoppingListService.addAllItemsFromRecipe(suggestedRecipe.getId(),account,people);
        }

        return validRecipes;
    }

    /**
     * Method to get total amount of two ingredients
     * @param ing1 ingredient1
     * @param ing2 ingredient2
     * @return A SerializedAmount object with quantity
     */
    private SerializedAmount totAmount(Ingredient ing1, Ingredient ing2){
        double totAmount = ing1.getAmount().getQuantity() + ing2.getAmount().getQuantity();
        String unit;
        if (ing1.getAmount().getUnit().equals(ing2.getAmount().getUnit())) unit = ing1.getAmount().getUnit();
        else {
            throw new IllegalArgumentException("miss-matched units, these ingredients are incompatible");
        }
        return new SerializedAmount(totAmount, unit);
    }

    /**
     * Group ingredients by type, returns pooled list of ingredients, pooled ingredients have earliest exp_date
     * @param ingredients the ingredients to group
     * @return pooled list of ingredients
     */
    public List<Ingredient> groupIngredients(List<Ingredient> ingredients){
        List<Ingredient> groupedIngredients = new ArrayList<>();
        for (Ingredient i: ingredients){
            boolean match = false;
            for (Ingredient j: groupedIngredients) {
                if (i.getItem().getEan().equals(j.getItem().getEan())
                        &&
                    i.getStatus() ==  Ingredient.Status.FREE){

                    match = true;
                    //grouping up
                    j.setAmount(totAmount(i, j));
                    j.setExp_date(new Date(Math.min(i.getExp_date().getTime(), j.getExp_date().getTime())));
                    break;
                }
            }
            if (!match && i.getStatus() ==  Ingredient.Status.FREE) groupedIngredients.add(i);
        }
        return groupedIngredients;
    }
}
