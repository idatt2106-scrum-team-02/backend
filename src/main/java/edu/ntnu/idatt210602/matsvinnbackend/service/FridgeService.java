package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.repo.FridgeRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.IngredientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Class fridgeService to provide business functionalities related to the fridge
 */
@Service
public class FridgeService {

    @Autowired
    FridgeRepository fridgeRepository;

    @Autowired
    IngredientRepository ingredientRepository;

    @Autowired
    LogService logService;

    Logger logger = LoggerFactory.getLogger(FridgeService.class);

    /**
     * Method to add a list of ingredients to the fridge
     * @param account the account that owns the fridge
     * @param ingredientList the list of ingredients
     * @return the updated ingredient list in the fridge
     */
    public List<Ingredient> addIngredients(Account account, List<Ingredient> ingredientList) {
        if (Objects.nonNull(ingredientList)) {
            for (Ingredient ingredient : ingredientList) {
                //Set expiration date based on today's date and item's shelf life
                Calendar c = Calendar.getInstance();
                c.setTime(new Date());
                logger.info("Current ingredient id: "+ingredient.getIngredient_id());
                c.add(Calendar.DATE, ingredient.getItem().getShelfLife());
                //set expiration date on added ingredient
                ingredient.setExp_date(c.getTime());
                //add ingredient to fridge
                account.getFridge().addIngredient(ingredient);
                //log action
                logService.logAction(new Date(), Log.Action.ADDED_TO_FRIDGE, ingredient, account);
            }
        }

        return fridgeRepository.save(account.getFridge()).getIngredientList();

    }

    /**
     * Method to reserve ingredients in the fridge of a given item totaling a given amount to reserve
     * @param item The ingredient item to reserve
     * @param ingredientAmountToReserve the total amount of ingredient to reserve
     * @param loggedInAccount the account that owns the fridge
     */
    public void reserveIngredients(Item item, double ingredientAmountToReserve, Account loggedInAccount) {

        List<Ingredient> ingredientsOfItem = new ArrayList<>();
        for (Ingredient ingredient: loggedInAccount.getFridge().getFreeIngredients()) {
            if (Objects.equals(ingredient.getItem().getId(), item.getId())) {
                ingredientsOfItem.add(ingredient);
            }
        }
        ingredientsOfItem.sort(Comparator.comparing(o -> o.getExp_date().getTime()));

        for (Ingredient ingredient: ingredientsOfItem) {
            if (ingredientAmountToReserve > 0) {
                if (ingredient.getAmount().getQuantity() <= ingredientAmountToReserve) {
                    ingredient.setStatus(Ingredient.Status.RESERVED);
                    ingredientAmountToReserve =- ingredient.getAmount().getQuantity();
                    ingredientRepository.save(ingredient);
                } else {
                    double originalAmount = ingredient.getAmount().getQuantity();
                    ingredient.setStatus(Ingredient.Status.RESERVED);
                    ingredient.setAmount(new SerializedAmount(ingredientAmountToReserve, ingredient.getAmount().getUnit()));
                    ingredientRepository.save(ingredient);
                    Ingredient remainingIngredient = new Ingredient(ingredient.getItem(),new SerializedAmount(originalAmount-ingredientAmountToReserve, ingredient.getAmount().getUnit()));
                    remainingIngredient.setStatus(Ingredient.Status.FREE);
                    remainingIngredient.setExp_date(ingredient.getExp_date());
                    Ingredient savedIngredient = ingredientRepository.save(remainingIngredient);
                    loggedInAccount.getFridge().addIngredient(savedIngredient);
                    fridgeRepository.save(loggedInAccount.getFridge());
                }
            } else {
                return;
            }
        }
    }

    /**
     * Method to set all ingredients in the fridge to free
     * @param loggedInAccount the account that is logged in
     */
    public void freeAllIngredients(Account loggedInAccount) {

        if (fridgeRepository.findById(loggedInAccount.getFridge().getId()).isEmpty()) {
            throw new IllegalStateException("fridge is not in repository");
        }
        Fridge fridge = loggedInAccount.getFridge();

        List<Ingredient> ingredients = fridge.getIngredientList();

        for (Ingredient ingredient : ingredients) {
            if (ingredient.getStatus() == Ingredient.Status.RESERVED) {

                ingredient.setStatus(Ingredient.Status.FREE);
            }
        }
        loggedInAccount.getFridge().setIngredientList(ingredients);
        combineFreeIngredients(loggedInAccount);
    }

    /**
     * Method to combine all free ingredients that are of the same item and date in the fridge
     * @param loggedInAccount the account that owns the fridge
     */
    public void combineFreeIngredients(Account loggedInAccount) {

        List<Ingredient> ingredientsToRemove = new ArrayList<>();
        for (Ingredient ingredient: loggedInAccount.getFridge().getFreeIngredients()) {

            if (!ingredientsToRemove.contains(ingredient)) { //skip ingredients that are set to be removed
                for (Ingredient ingredient2 : loggedInAccount.getFridge().getFreeIngredients()) {

                    if (!ingredientsToRemove.contains(ingredient2)) { //skip ingredients that are set to be removed
                        if ((!Objects.equals(ingredient.getIngredient_id(), ingredient2.getIngredient_id()))
                                && Objects.equals(ingredient.getItem().getId(), ingredient2.getItem().getId())
                                && ingredient.getExp_date().equals(ingredient2.getExp_date())) {

                            ingredient.getAmount().setQuantity(ingredient.getAmount().getQuantity() + ingredient2.getAmount().getQuantity());
                            ingredientsToRemove.add(ingredient2);
                        }
                    }
                }
            }
        }

        for (Ingredient ingredient: ingredientsToRemove) {
            loggedInAccount.getFridge().getIngredientList().remove(ingredient);
            ingredientRepository.deleteById(ingredient.getIngredient_id());
        }

        fridgeRepository.save(loggedInAccount.getFridge());
    }

    /**
     * Method to combine all reserved ingredients that are of the same item and date in the fridge
     * @param loggedInAccount the account that owns the fridge
     */
    public void combineReservedIngredients(Account loggedInAccount) {

        List<Ingredient> ingredientsToRemove = new ArrayList<>();
        for (Ingredient ingredient: loggedInAccount.getFridge().getReservedIngredients()) {

            if (!ingredientsToRemove.contains(ingredient)) { //skip ingredients that are set to be removed
                for (Ingredient ingredient2 : loggedInAccount.getFridge().getReservedIngredients()) {

                    if (!ingredientsToRemove.contains(ingredient2)) { //skip ingredients that are set to be removed
                        if ((!Objects.equals(ingredient.getIngredient_id(), ingredient2.getIngredient_id()))
                                && Objects.equals(ingredient.getItem().getId(), ingredient2.getItem().getId())
                                && ingredient.getExp_date().equals(ingredient2.getExp_date())) {

                            ingredient.getAmount().setQuantity(ingredient.getAmount().getQuantity() + ingredient2.getAmount().getQuantity());
                            ingredientsToRemove.add(ingredient2);
                        }
                    }
                }
            }
        }

        for (Ingredient ingredient: ingredientsToRemove) {
            loggedInAccount.getFridge().getIngredientList().remove(ingredient);
            ingredientRepository.deleteById(ingredient.getIngredient_id());
        }

        fridgeRepository.save(loggedInAccount.getFridge());
    }
}