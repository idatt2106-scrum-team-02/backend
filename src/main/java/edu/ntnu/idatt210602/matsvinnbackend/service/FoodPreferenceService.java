package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.model.FoodPreference;
import edu.ntnu.idatt210602.matsvinnbackend.repo.FoodPreferencesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Class FoodPreferenceService to provide business functionalities related to foodPreferences
 */
@Service
public class FoodPreferenceService {

    @Autowired
    FoodPreferencesRepository foodPreferencesRepository;

    /**
     * Method to get all food preferences from the repository
     * @return a list of food preferences
     */
    public List<FoodPreference> getAllFoodPreferences() {
        return foodPreferencesRepository.findAll();
    }
}
