package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.repo.DayMenuRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.RecipeRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.WeekMenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Class WeekMenuService to provide business functionalities related to persistence of the week menu
 */
@Service
public class WeekMenuService {

    @Autowired
    WeekMenuRepository weekMenuRepository;

    @Autowired
    DayMenuRepository dayMenuRepository;

    @Autowired
    RecipeRepository recipeRepository;

    @Autowired
    RecipeSearchService recipeSearchService;

    @Autowired
    FridgeService fridgeService;

    @Autowired
    ShoppingListService shoppingListService;

    /**
     * Method to get a week menu with the dayMenus sorted by week day
     * @param loggedInAccount the account that owns the week menu
     * @return a WeekMenu object
     */
    public WeekMenu getWeekMenu(Account loggedInAccount) {
        if (weekMenuRepository.findById(loggedInAccount.getWeekMenu().getId()).isEmpty()) {
            throw new IllegalStateException("week menu is not in repository");
        }
        WeekMenu weekmenu = weekMenuRepository.findById(loggedInAccount.getWeekMenu().getId()).get();

        List<DayMenu> weekRecipes = new ArrayList<>(7);

        for (DayMenu ignored : weekmenu.getDayMenus()) {
            weekRecipes.add(null);
        }

        for (DayMenu dayMenu: weekmenu.getDayMenus()) {
            if (Objects.equals(dayMenu.getDay(), "Mandag")) {
                weekRecipes.add(0, dayMenu);
            }
            if (Objects.equals(dayMenu.getDay(), "Tirsdag")) {
                weekRecipes.add(1, dayMenu);
            }
            if (Objects.equals(dayMenu.getDay(), "Onsdag")) {
                weekRecipes.add(2, dayMenu);
            }
            if (Objects.equals(dayMenu.getDay(), "Torsdag")) {
                weekRecipes.add(3, dayMenu);
            }
            if (Objects.equals(dayMenu.getDay(), "Fredag")) {
                weekRecipes.add(4, dayMenu);
            }
            if (Objects.equals(dayMenu.getDay(), "Lørdag")) {
                weekRecipes.add(5, dayMenu);
            }
            if (Objects.equals(dayMenu.getDay(), "Søndag")) {
                weekRecipes.add(6, dayMenu);
            }
        }

        loggedInAccount.getWeekMenu().setDayMenus(weekRecipes.subList(0,Math.min(7, weekRecipes.size())));
        return loggedInAccount.getWeekMenu();
    }

    /**
     * Method to update a weekMenu
     * @param weekMenu the updated menu
     * @return a WeekMenu object
     */
    public WeekMenu updateWeekMenu(WeekMenu weekMenu) {
        if (weekMenuRepository.existsById(weekMenu.getId())) {
            return weekMenuRepository.save(weekMenu);
        } else {
            throw new IllegalStateException("week menu is not in repository");
        }
    }

    /**
     * Method to generate a new week menu
     * @param loggedInAccount the account that owns the week menu
     * @param numberOfPeople the amount of people to generate for
     * @return a list of day menu
     */
    public WeekMenu generateWeekMenu(Account loggedInAccount, Integer numberOfPeople) {

        Fridge fridge = loggedInAccount.getFridge();

        //clear potentially remaining reserved and requested ingredients from previous weekMenu
        fridgeService.freeAllIngredients(loggedInAccount);
        shoppingListService.removeAllRequestedFromShoppingList(loggedInAccount);

        List<Recipe> validRecipes = recipeSearchService.getRecipesByIngExpiry(fridge.getFreeIngredients(), recipeRepository.findAll());

        List<Recipe> weekMenu = recipeSearchService.getRecipeSuggestions(
                loggedInAccount,
                fridge.getFreeIngredients(),
                (validRecipes.subList(0, Math.min(validRecipes.size(), 20))), //get twenty recipes that use ingredients close to expiry
                Math.min(7, validRecipes.size()),
                numberOfPeople);

        List<Recipe> AllNewRecipes = recipeRepository.findAll();

        for (Recipe recipe:weekMenu) {
            AllNewRecipes.remove(recipe);
        }
        //Fill remaining with random unique recipes
        Random random = new Random();
        while (weekMenu.size() < 7 && AllNewRecipes.size() > 0) {

            Recipe randomRecipe = AllNewRecipes.get(random.nextInt(AllNewRecipes.size()));
            AllNewRecipes.remove(randomRecipe);
            weekMenu.add(randomRecipe);
            shoppingListService.addAllItemsFromRecipe(randomRecipe.getId(),loggedInAccount,numberOfPeople);
        }

        List<Recipe> AllRecipes = recipeRepository.findAll();

        while (weekMenu.size() < 7 && AllRecipes.size() > 0) {

            Recipe randomRecipe = AllRecipes.get(random.nextInt(AllRecipes.size()));
            weekMenu.add(randomRecipe);
            shoppingListService.addAllItemsFromRecipe(randomRecipe.getId(),loggedInAccount,numberOfPeople);
        }


        fridgeService.combineFreeIngredients(loggedInAccount);
        fridgeService.combineReservedIngredients(loggedInAccount);

        List<DayMenu> dayMenus = new ArrayList<>();
        DayMenu monday = dayMenuRepository.save(new DayMenu(null,weekMenu.get(0),"Mandag"));
        DayMenu tuesday = dayMenuRepository.save(new DayMenu(null,weekMenu.get(1),"Tirsdag"));
        DayMenu wednesday = dayMenuRepository.save(new DayMenu(null,weekMenu.get(2),"Onsdag"));
        DayMenu thursday = dayMenuRepository.save(new DayMenu(null,weekMenu.get(3),"Torsdag"));
        DayMenu friday = dayMenuRepository.save(new DayMenu(null,weekMenu.get(4),"Fredag"));
        DayMenu saturday = dayMenuRepository.save(new DayMenu(null,weekMenu.get(5),"Lørdag"));
        DayMenu sunday = dayMenuRepository.save(new DayMenu(null,weekMenu.get(6),"Søndag"));

        dayMenus.add(monday);
        dayMenus.add(tuesday);
        dayMenus.add(wednesday);
        dayMenus.add(thursday);
        dayMenus.add(friday);
        dayMenus.add(saturday);
        dayMenus.add(sunday);

        List<Integer> dayMenusToDelete = new ArrayList<>();
        for (DayMenu dayMenu: loggedInAccount.getWeekMenu().getDayMenus()) {
            dayMenusToDelete.add(dayMenu.getId());
        }
        loggedInAccount.getWeekMenu().getDayMenus().clear();
        updateWeekMenu(loggedInAccount.getWeekMenu()); //removes reference so they can be deleted
        for (Integer id: dayMenusToDelete) {
            dayMenuRepository.deleteById(id);
        }
        loggedInAccount.getWeekMenu().setDayMenus(dayMenus);
        loggedInAccount.getWeekMenu().setPeopleAmount(numberOfPeople);

        updateWeekMenu(loggedInAccount.getWeekMenu());

        return getWeekMenu(loggedInAccount);
    }
}
