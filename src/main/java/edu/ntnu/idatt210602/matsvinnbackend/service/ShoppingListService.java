package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.controller.ItemController;
import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Count;
import edu.ntnu.idatt210602.matsvinnbackend.repo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Class shoppingListService to provide business functionalities related to the shopping list
 */
@Service
public class ShoppingListService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ItemController itemController;

    @Autowired
    private ShoppingListRepository shoppingListRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private FridgeRepository fridgeRepository;

    @Autowired
    private FridgeService fridgeService;

    /**
     * Method to get the shopping list of an account
     * @param loggedInAccount the logged in account
     * @return the shopping list
     */
    public ShoppingList getShoppingList(Account loggedInAccount) {
        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }
        return shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).get();
    }

    /**
     * Method to add an item
     * @param itemId The id of the item to be added
     * @param quantity The quantity of the item
     * @param loggedInAccount The logged in account
     * @return The ingredient that was added
     * @throws IllegalArgumentException if item was not found in repo
     * @throws IllegalStateException if a shopping list for this account was not found (should not ever happen)
     */
    public Ingredient addItem(Integer itemId, Integer quantity, Account loggedInAccount) throws IllegalArgumentException, IllegalStateException{

        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }

        if (itemRepository.findById(itemId).isEmpty()) {
            throw new IllegalArgumentException("item was not found");
        }
        Item item = itemRepository.findById(itemId).get();

        Ingredient ingredient = new Ingredient(item, new Amount(quantity, Count.COUNT).serialize());
        ingredient.setStatus(Ingredient.Status.PENDING);
        Ingredient newIngredient = ingredientRepository.save(ingredient);
        loggedInAccount.getShoppingList().addIngredient(newIngredient);
        shoppingListRepository.save(loggedInAccount.getShoppingList());
        return newIngredient;
    }

    /**
     * Method to add a suggestion
     * @param itemId The ean of the suggested item to be added
     * @param quantity The quantity of the suggested item
     * @param loggedInAccount The logged in account
     * @return The ingredient suggestion that was added
     * @throws IllegalArgumentException if item was not found in repo
     * @throws IllegalStateException if a shopping list for this account was not found (should not ever happen)
     */
    public Ingredient addSuggestion(Integer itemId, Integer quantity, Account loggedInAccount) throws IllegalArgumentException, IllegalStateException{

        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }
        if (itemRepository.findById(itemId).isEmpty()) {
            throw new IllegalArgumentException("item was not found");
        }
        Item item = itemRepository.findById(itemId).get();

        Ingredient ingredient = new Ingredient(item, new Amount(quantity,item.getAmount().deserialize().getUnit()).serialize());
        ingredient.setStatus(Ingredient.Status.PENDING);
        Ingredient newIngredient = ingredientRepository.save(ingredient);
        ShoppingList shoppingList = shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).get();
        shoppingList.addSuggestion(newIngredient);
        shoppingListRepository.save(shoppingList);
        return newIngredient;
    }

    /**
     * Method to remove an item from list
     * @param ingredientId the id of the ingredient
     * @param loggedInAccount the owner of the shopping list
     * @return The ingredient that was removed
     * @throws IllegalArgumentException if the ingredient does not exist, or it is not in the shopping list
     * @throws IllegalStateException if the account is missing a shopping list
     */
    public Ingredient removeItem(Integer ingredientId, Account loggedInAccount) throws IllegalArgumentException, IllegalStateException {

        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }
        if (ingredientRepository.findById(ingredientId).isEmpty()) {
            throw new IllegalArgumentException("An ingredient with a given id does not exist");
        }
        ShoppingList shoppingList = shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).get();
        if (!shoppingList.IsIngredientInList(ingredientId)) {
            throw new IllegalArgumentException("An ingredient with a given id is not in the shopping list for given account");
        }
        Ingredient ingredient = ingredientRepository.findById(ingredientId).get();
        shoppingList.removeIngredient(ingredientId);
        ingredientRepository.deleteById(ingredientId);
        shoppingListRepository.save(shoppingList);
        return ingredient;
    }

    /**
     * Method to remove a suggestion from the suggestions list
     * @param ingredientId the id of the suggestion
     * @param loggedInAccount the owner of the suggestion list
     * @return The ingredient that was removed
     * @throws IllegalArgumentException if the ingredient does not exist, or it is not in the suggestion list
     * @throws IllegalStateException if the account is missing a shopping list
     */
    public Ingredient removeSuggestion(Integer ingredientId, Account loggedInAccount) throws IllegalArgumentException, IllegalStateException {

        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }
        if (ingredientRepository.findById(ingredientId).isEmpty()) {
            throw new IllegalArgumentException("An ingredient with a given id does not exist");
        }
        ShoppingList shoppingList = shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).get();
        if (!shoppingList.IsIngredientInSuggestions(ingredientId)) {
            throw new IllegalArgumentException("An ingredient with a given id is not in the suggestions list for given account");
        }
        Ingredient ingredient = ingredientRepository.findById(ingredientId).get();
        shoppingList.removeSuggestion(ingredientId);
        ingredientRepository.deleteById(ingredientId);
        shoppingListRepository.save(shoppingList);
        return ingredient;
    }

    /**
     * Method to accept a suggestion from the suggestion list, which removes the suggestion and adds it to the list
     * @param ingredientId the id of the ingredient to accept
     * @param loggedInAccount the account that owns the shopping list
     * @return The ingredient that accepted
     */
    public Ingredient acceptSuggestion(Integer ingredientId, Account loggedInAccount) {
        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }
        if (ingredientRepository.findById(ingredientId).isEmpty()) {
            throw new IllegalArgumentException("The ingredient with the given id does not exist");
        }
        ShoppingList shoppingList = shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).get();
        if (!shoppingList.IsIngredientInSuggestions(ingredientId)) {
            throw new IllegalArgumentException("The ingredient with the given id is not in the suggestions list for given account");
        }
        if (shoppingList.IsIngredientInList(ingredientId)) {
            throw new IllegalStateException("The ingredient is already also in the shopping list");
        }
        Ingredient ingredient = ingredientRepository.findById(ingredientId).get();
        shoppingList.addIngredient(ingredient);
        shoppingList.removeSuggestion(ingredientId);
        shoppingListRepository.save(shoppingList);
        return ingredient;
    }

    /**
     * Method to change the quantity of an ingredient in the shopping list
     * @param ingredientId the id of the ingredient
     * @param quantity the quantity
     * @param loggedInAccount the account that is logged in
     * @return the updated shopping list
     */
    public ShoppingList changeItemAmount(Integer ingredientId, Integer quantity, Account loggedInAccount) {

        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }
        if (quantity < 0) {
            throw new IllegalArgumentException("quantity cannot be set to less than one");
        }
        if (ingredientRepository.findById(ingredientId).isEmpty()) {
            throw new IllegalArgumentException("The ingredient with a given id does not exist");
        }
        ShoppingList shoppingList = shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).get();
        if (!shoppingList.IsIngredientInList(ingredientId)) {
            throw new IllegalArgumentException("The ingredient with the given id is not in the suggestions list for given account");
        }
        if(quantity > 0) {
            Ingredient ingredient = shoppingList.getIngredientFromList(ingredientId);
            ingredient.setAmount(new Amount(quantity, ingredient.getAmount().deserialize().getUnit()).serialize());
            ingredientRepository.save(ingredient);
        }
        if (quantity == 0) {
            shoppingList.removeIngredient(ingredientId);
            ingredientRepository.deleteById(ingredientId);
        }
        return shoppingListRepository.save(shoppingList);
    }

    /**
     * Method to change the quantity of an ingredient in the suggestion list
     * @param ingredientId the id of the ingredient
     * @param quantity the quantity
     * @param loggedInAccount the account that is logged in
     * @return the updated shopping list
     */
    public ShoppingList changeSuggestionAmount(Integer ingredientId, Integer quantity, Account loggedInAccount) {

        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }
        if (quantity < 0) {
            throw new IllegalArgumentException("quantity cannot be set to less than one");
        }
        if (ingredientRepository.findById(ingredientId).isEmpty()) {
            throw new IllegalArgumentException("The ingredient with a given id does not exist");
        }
        ShoppingList shoppingList = shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).get();
        if (!shoppingList.IsIngredientInSuggestions(ingredientId)) {
            throw new IllegalArgumentException("The ingredient with the given id is not in the suggestions list for given account");
        }
        if(quantity > 0) {
            Ingredient ingredient = shoppingList.getSuggestionFromList(ingredientId);
            ingredient.setAmount(new Amount(quantity, ingredient.getAmount().deserialize().getUnit()).serialize());
            ingredientRepository.save(ingredient);
        }
        if (quantity == 0) {
            shoppingList.removeSuggestion(ingredientId);
            ingredientRepository.deleteById(ingredientId);
        }
        return shoppingListRepository.save(shoppingList);
    }

    /**
     * Buy all method to remove a list of ingredients from shopping list and add them to fridge
     * @param ingredients a list of ingredients
     * @param loggedInAccount the account that is logged in
     * @return The shopping
     */
    public ShoppingList buyAll(List<Ingredient> ingredients, Account loggedInAccount) {

        //Gets the fridge and shopping list from account
        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }
        if (fridgeRepository.findById(loggedInAccount.getFridge().getId()).isEmpty()) {
            throw new IllegalStateException("Account fridge does not exist");
        }
        ShoppingList shoppingList = shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).get();

        //Checking if ingredient list to add is valid
        ingredients.forEach(ingredient -> {
            Integer id = ingredient.getIngredient_id();
            if (!ingredientRepository.existsById(id)) {
                throw new IllegalArgumentException("The ingredient with the given id does not exist in the database");
            }
            if (!shoppingList.IsIngredientInList(id)) {
                throw new IllegalArgumentException("The ingredient with the given id is not in the shopping list for given account");
            }
        });

        List<Ingredient> ingredientsToAddToFridge = new ArrayList<>();
        //add to fridge and remove from shopping list
        ingredients.forEach(ing -> {
            Integer id = ing.getIngredient_id();
            Optional<Ingredient> ingredient = ingredientRepository.findById(id);
            shoppingList.removeIngredient(id);
            if (ingredient.isPresent()) {
                Ingredient ingredient1 = ingredient.get();
                double quantity = ingredient1.getAmount().getQuantity();
                ingredient1.setAmount(new Amount(ingredient1.getItem().getAmount().getQuantity() * quantity, ingredient1.getItem().getAmount().deserialize().getUnit()).serialize());
                ingredient1.changeStatusFromShoppingListToFridge();
                Ingredient toFridge = ingredientRepository.save(ingredient1);
                ingredientsToAddToFridge.add(toFridge);
            }
        });
        fridgeService.addIngredients(loggedInAccount,ingredientsToAddToFridge);
        return shoppingListRepository.save(shoppingList);
    }

    /**
     * Method to add all items from a recipe without adding items already in fridge and not reserved
     * @param recipeId The recipe id
     * @param loggedInAccount The logged in account
     * @return The updated shopping list
     */
    public ShoppingList addAllItemsFromRecipe(Integer recipeId, Account loggedInAccount, Integer peopleCount) {

        if (recipeRepository.findById(recipeId).isEmpty()) {
            throw new IllegalArgumentException("Recipe was not found");
        }
        Recipe recipe = recipeRepository.findById(recipeId).get();
        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }

        ShoppingList shoppingList = shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).get();
        recipe.getIngredient().forEach(ingredient -> {
            double itemAmount = ingredient.getItem().getAmount().getQuantity();
            double ingredientAmount = ingredient.getAmount().deserialize().convertTo(ingredient.getItem().getAmount().deserialize().getUnit()).getQuantity()*peopleCount;
            double amountOfItemInFridge = getAmountOfItemFromFridge(ingredient.getItem(),loggedInAccount);
            double ingredientAmountNotOwned = ingredientAmount - amountOfItemInFridge;
            double ingredientAmountToReserve = Math.min(ingredientAmount, amountOfItemInFridge);
            fridgeService.reserveIngredients(ingredient.getItem(), ingredientAmountToReserve, loggedInAccount);
            if (ingredientAmountNotOwned < 0) {
                ingredientAmountNotOwned = 0;
            }
            double quantityToAddOfWare = ingredientAmountNotOwned/itemAmount;
            if (quantityToAddOfWare > 0) {
                //check if it exists first if so add to that request instead
                boolean alreadyRequested = false;
                for (Ingredient ingredient1: shoppingList.getIngredientList()) {
                    if (ingredient1.getStatus() == Ingredient.Status.REQUESTED && Objects.equals(ingredient1.getItem().getId(), ingredient.getItem().getId())) {
                        ingredient1.getAmount().setQuantity(ingredient1.getAmount().getQuantity() + Math.ceil(quantityToAddOfWare));
                        alreadyRequested = true;
                    }
                }

                //otherwise
                if (!alreadyRequested) {
                    Ingredient shoppingListIngredient = new Ingredient(ingredient.getItem(), new Amount(Math.ceil(quantityToAddOfWare), Count.COUNT).serialize());
                    shoppingListIngredient.setStatus(Ingredient.Status.REQUESTED);
                    ingredientRepository.save(shoppingListIngredient);
                    shoppingList.addIngredient(shoppingListIngredient);
                }
            }
        });
        return shoppingListRepository.save(shoppingList);
    }

    /**
     * Method to get the amount of an item in the fridge
     * @param item The item to find for
     * @param loggedInAccount the logged in account
     * @return the total amount of an item in the fridge
     */
    public double getAmountOfItemFromFridge(Item item, Account loggedInAccount) {

        return loggedInAccount.getFridge().getFreeIngredients().stream().filter(ingredient -> Objects.equals(ingredient.getItem().getId(), item.getId())).mapToDouble(ingredient -> ingredient.getAmount().getQuantity()).sum();
    }

    /**
     * Method to remove all requested items from shopping list
     * @param loggedInAccount the account that owns the shopping list
     */
    public void removeAllRequestedFromShoppingList(Account loggedInAccount) {

        if (shoppingListRepository.findById(loggedInAccount.getShoppingList().getId()).isEmpty()) {
            throw new IllegalStateException("Account shopping list does not exist");
        }
        List<Ingredient> requestsToRemove = new ArrayList<>();
        for (Ingredient ingredient: loggedInAccount.getShoppingList().getIngredientList()) {
            if (ingredient.getStatus() == Ingredient.Status.REQUESTED) {
                requestsToRemove.add(ingredient);
            }
        }

        for (Ingredient ingredient: requestsToRemove) {
            loggedInAccount.getShoppingList().getIngredientList().remove(ingredient);
            ingredientRepository.deleteById(ingredient.ingredient_id);
        }

        shoppingListRepository.save(loggedInAccount.getShoppingList());
    }
}
