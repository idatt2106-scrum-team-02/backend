package edu.ntnu.idatt210602.matsvinnbackend.service;

import edu.ntnu.idatt210602.matsvinnbackend.model.Account;
import edu.ntnu.idatt210602.matsvinnbackend.model.Ingredient;
import edu.ntnu.idatt210602.matsvinnbackend.repo.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.ntnu.idatt210602.matsvinnbackend.model.Log;
import java.util.Date;

@Service
public class LogService {

    /*
    This service should be used by the Fridge API to add log entries.
    Currently assumed to cover three cases: added to fridge, consumed, discarded.
     */
    @Autowired
    LogRepository logRepository;

    /**
     * Logs action
     *
     * This method logs an action done in the fridge.
     * The method should be called whenever a user interacts with the fridge.
     *
     *
     * @param date - time of log entry
     * @param action - action committed
     * @param ingredient - subject of the action
     * @param account - did the action
     */
    public Log logAction(Date date, Log.Action action, Ingredient ingredient, Account account){
        Log log = new Log(date, action, ingredient, account);

        return logRepository.save(log);
    }
    /**
     * Logs action
     *
     * This method logs an action done in the fridge.
     * The method should be called whenever a user interacts with the fridge.
     *
     * This alternate logging method saves any provided log,
     * allowing custom logs
     *
     * @param log - created before action is logged
     */
    public Log logAction(Log log){
        return logRepository.save(log);
    }

    /**
     * This constructor explicitly provides a repository for storing logs
     *
     * @param logRepository - given LogRepository
     */
    public LogService(LogRepository logRepository){
        this.logRepository = logRepository;
    }

    /**
     * basic constructor
     */
    public LogService(){}
}
