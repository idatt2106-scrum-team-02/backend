package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.Account;
import edu.ntnu.idatt210602.matsvinnbackend.model.AddAccountRequest;
import edu.ntnu.idatt210602.matsvinnbackend.model.Profile;
import edu.ntnu.idatt210602.matsvinnbackend.model.UpdateAccountRequest;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.ProfileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.Objects;

/**
 * AccountController
 *
 * This class contains all endpoint handlers for the `/account` API,
 * including creation, updating, fetching and deleting of accounts
 */
@Controller
@CrossOrigin
@RequestMapping(path = "/account")
public class AccountController {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ProfileRepository profileRepository;

    Logger logger = LoggerFactory.getLogger(AccountController.class);

    /**
     * Add new account and a standard profile
     *
     * This endpoint expects account data to be supplied as a JSON object
     * in the request body
     *
     * Possible responses are HTTP 201 Created or HTTP 409 Conflict if another account with the same email already exists
     *
     * @param request request with email, password and first name
     */
    @PostMapping(path = "")
    @ResponseStatus(code = HttpStatus.CREATED)
    public @ResponseBody Account addAccount(
            @RequestBody AddAccountRequest request) {

        // If there already exists an account with the same email throw 409
        if (accountRepository.existsByEmail(request.email())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }

        Account newAccount = new Account(
                request.firstname(),
                request.email(),
                request.password());

        Account account = accountRepository.save(newAccount);

        Profile profile = new Profile(request.firstname(), "", false, account.getId());
        profileRepository.save(profile);

        return account;
    }

    /**
     * Delete an existing account
     *
     * This endpoint removes an account given their ID
     * This is only available if the authenticated account is
     * the same as the account being deleted
     *
     * @param accountId - ID of the account to delete
     */
    @DeleteMapping(path = "/{accountId}")
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteAccount(@PathVariable Integer accountId) {
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();

        if (Objects.equals(loggedInAccount.getId(), accountId)) {
            accountRepository.deleteById(accountId);
        } else {
            logger.warn("Account " + loggedInAccount.getId() + " made illegal attempt to delete account " + accountId);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    /**
     * Fetch an account
     *
     * This endpoint responds with the info of the account with the
     * given ID, or HTTP 404 Not found - if no such account exists or 403 Forbidden - authentication error
     *
     * @param accountId - ID of the account to fetch
     * @return the requested account
     */
    @GetMapping(path = "/{accountId}")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody Account getAccount(@PathVariable Integer accountId) {
        logger.info("Request to get account with id " + accountId);
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        if (!Objects.equals(loggedInAccount.getId(), accountId)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        return accountRepository.findById(accountId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    }

    /**
     * Update an existing account
     *
     * This endpoint makes changes to an existing account given their ID
     * and the updated values of the fields to update.
     *
     * This endpoint requires the authenticated account to be the account
     * to update
     *
     * @param accountId - the ID of the account to change
     * @param request - the updated values (null fields are ignored)
     */
    @PutMapping(path = "/{accountId}")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody Account updateAccount(@PathVariable Integer accountId,
                                         @RequestBody UpdateAccountRequest request) {

        logger.info("Request to update an account with id "+accountId);

        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        Account authenticatedAccount = accountRepository.findByEmail(email).orElseThrow(() -> new ResponseStatusException(HttpStatus.FORBIDDEN));

        if (!Objects.equals(authenticatedAccount.getId(), accountId)) {
            logger.warn("Account " + authenticatedAccount.getId() + " made illegal attempt to modify account " + accountId);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        if (Objects.nonNull(request.firstname())) {
            authenticatedAccount.setFirstname(request.firstname());
        }

        if (Objects.nonNull(request.password())) {
            authenticatedAccount.setPassword(request.password());
        }

        return accountRepository.save(authenticatedAccount);
    }

}
