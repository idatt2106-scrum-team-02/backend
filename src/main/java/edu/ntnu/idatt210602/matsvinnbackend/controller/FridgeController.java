package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.repo.*;
import edu.ntnu.idatt210602.matsvinnbackend.service.FridgeService;
import edu.ntnu.idatt210602.matsvinnbackend.service.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Fridge controller
 * This class contains all the endpoint handlers for the '/fridge' API
 */
@Controller
@CrossOrigin
@RequestMapping(path = "/fridge")
public class FridgeController {

    @Autowired
    FridgeRepository fridgeRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    IngredientRepository ingredientRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    LogService logService;

    @Autowired
    FridgeService fridgeService;

    @Autowired
    RecipeRepository recipeRepository;

    Logger logger = LoggerFactory.getLogger(FridgeController.class);

    /**
     * Add ingredients to fridge
     *
     * Possible responses are:
     * HTTP 200 OK - ingredients added to fridge
     * HTTP 404 Not found - if account or fridge were not found
     *
     * @param ingredientList the ingredient list
     * @return ingredient list
     */
    @PostMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody List<Ingredient> addIngredients(@RequestBody List<Ingredient> ingredientList) {
        logger.info("Request to add ingredients into the fridge.");
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();

        return fridgeService.addIngredients(loggedInAccount, ingredientList);
    }

    /**
     * Add items to fridge
     *
     * Possible responses:
     * HTTP 200 OK - added items
     * HTTP 404 Not Found - if account or item were not found
     * @param request item id and serialized amount
     * @return ingredient list
     */
    @PostMapping("/items")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody List<Ingredient> addItem(@RequestBody AddItemToFridgeRequest request) {
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        Item item = itemRepository.findById(request.itemId()).orElseThrow();
        Ingredient ingredient = new Ingredient(item, request.amount());
        ingredient.setStatus(Ingredient.Status.FREE);
        Ingredient newIngredient = ingredientRepository.save(ingredient);
        List<Ingredient> ingredientList = new ArrayList<>();
        ingredientList.add(newIngredient);

        return fridgeService.addIngredients(loggedInAccount, ingredientList);
    }

    /**
     * Get all ingredients from the fridge
     *
     * Possible responses are:
     * HTTP 200 OK - fetched ingredients
     * HTTP 404 Not found - account/fridge not found
     *
     * @return ingredient list
     */
    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody List<Ingredient> getIngredients() {
        logger.info("Request to get ingredients from the fridge.");
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        Fridge fridge = fridgeRepository.findById(loggedInAccount.getFridge().getId()).orElseThrow();

        return fridge.getIngredientList();
    }

    /**
     * Remove ingredients' amount
     *
     * Possible responses:
     * HTTP 200 OK - ingredient amount updated
     * HTTP 404 Not found - account/fridge not found
     * @param request action and a map with ingredient id and used up amount of the ingredient
     * @return updated fridge ingredients
     */
    @PutMapping(path = "/ingredientsAmount")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody List<Ingredient>  removeIngredientsAmount(@RequestBody UpdateIngredientsAmountInFridge request) {
        logger.info("Request to remove ingredients' amount");
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        Fridge fridge = fridgeRepository.findById(loggedInAccount.getFridge().getId()).orElseThrow();

        request.ingredients().forEach((ingredientId, usedAmount) -> {
            Ingredient ingredient = ingredientRepository.findById(ingredientId).orElseThrow();
            int ingredientIndex = fridge.getIngredientList().indexOf(ingredient);
            Amount currentAmount = fridge.getIngredientList().get(ingredientIndex).getAmount().deserialize();
            Amount remainingAmount = currentAmount.subtract(usedAmount.deserialize());

            if (request.action() == Log.Action.ADDED_TO_FRIDGE) {
                logger.warn("Action not valid here. Use either consumed or discarded.");
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }

            if (remainingAmount.getQuantity() <= 0) {
                logService.logAction(new Date(), request.action(), fridge.getIngredientList().get(ingredientIndex), loggedInAccount);
                fridge.getIngredientList().remove(ingredientIndex);
                logger.info("Ingredient removed from the fridge");

            } else {
                fridge.getIngredientList().get(ingredientIndex).setAmount(remainingAmount.serialize());
                logger.info("Ingredient updated and logged.");
            }
        });

        return fridgeRepository.save(fridge).getIngredientList();
    }

    /**
     * Update expire date on an ingredient
     *
     * Possible responses:
     * HTTP 200 OK - ingredient updated
     * HTTP 404 Not Found - account, fridge or ingredient not found
     *
     * @param request ingredient id and new exp date
     * @return updated ingredient object
     */
    @PutMapping(path = "/ingredientDate")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody Ingredient adjustIngredientExpireDate(@RequestBody UpdateIngredientDateRequest request) {
        //find logged in account and its fridge
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        Fridge fridge = fridgeRepository.findById(loggedInAccount.getFridge().getId()).orElseThrow();

        //check if the request ingredient exists
        ingredientRepository.findById(request.ingredientId()).orElseThrow();

        //check if the ingredient is in current account's fridge and find its index
        Ingredient ingredient = fridge.getIngredientList().stream().filter(i -> i.getIngredient_id() == request.ingredientId()).findFirst().orElseThrow();
        int ingredientIndex = fridge.getIngredientList().indexOf(ingredient);

        //update exp date on the ingredient
        fridge.getIngredientList().get(ingredientIndex).setExp_date(request.newExpDate());
        fridgeRepository.save(fridge);

        //return updated ingredient
        return fridge.getIngredientList().get(ingredientIndex);
    }

    /**
     * Remove ingredients based on recipe
     *
     * Possible responses:
     * HTTP 200 OK - ingredient amount removed
     * HTTP 404 Not found - account/fridge/recipe not found
     * @param request that contains recipe id and recipe scalar
     */
    @PutMapping(path = "/ingredients")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody void removeIngredients(@RequestBody RemoveIngredientsRequest request) {
        int recipeId = request.recipeId();
        int recipeScalar = request.recipeScalar();
        logger.info("Request to remove ingredients from fridge based on recipe with id: "+recipeId);

        //Authentication
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        Fridge fridge = fridgeRepository.findById(loggedInAccount.getFridge().getId()).orElseThrow();
        Recipe recipe = recipeRepository.findById(recipeId).orElseThrow();

        //Declare all variables
        Amount recipeIngredientAmount;
        Amount remainingAmount;
        Amount currentFridgeIngredientAmount;

        logger.info("Recipe ingredients: "+recipe.getIngredient());
        //Loop through recipe ingredients
        for (Ingredient recipeIngredient : recipe.getIngredient()) {
            //current recipe ingredient amount multiplied with recipe scalar
            recipeIngredientAmount = recipeIngredient.getAmount().deserialize().multiply(recipeScalar);

            //remaining amount of that ingredient
            remainingAmount = recipeIngredientAmount;

            //Find all ingredients in fridge that match current recipe ingredient and sort based on expire date
            List<Ingredient> fridgeIngredients = fridge.getIngredientList().stream().filter(i -> Objects.equals(i.getItem().getName(), recipeIngredient.getItem().getName())).collect(Collectors.toList());
            fridgeIngredients.sort(Comparator.comparing(Ingredient::getExp_date));

            logger.info("Fridge ingredients: "+fridgeIngredients);
            //Loop through matching ingredients in fridge
            for (Ingredient fridgeIngredient : fridgeIngredients) {
                //Check if the ingredient is RESERVED and if there is any remaining amount of current recipe ingredient
                if (fridgeIngredient.getStatus() == Ingredient.Status.RESERVED && remainingAmount.getQuantity() != 0) {

                    //amount of current fridge ingredient
                    currentFridgeIngredientAmount = fridgeIngredient.getAmount().deserialize();

                    //log consumed ingredient before it potentially gets removed
                    int ingredientIndex = fridge.getIngredientList().indexOf(fridgeIngredient);
                    logService.logAction(new Date(), Log.Action.CONSUMED, fridge.getIngredientList().get(ingredientIndex), loggedInAccount);

                    //if there is any remaining fridge ingredient after we subtracted recipe amount, save ingredient and set remaining amount to 0
                    if (currentFridgeIngredientAmount.subtract(remainingAmount).getQuantity() > 0) {
                        ingredientRepository.save(fridgeIngredient);
                        fridge.getIngredientList().get(ingredientIndex).setAmount(currentFridgeIngredientAmount.subtract(remainingAmount).serialize());
                        //remaining amount is now 0
                        remainingAmount.subtract(remainingAmount);
                        logger.info("Removed recipe amount of this ingredient and updated it in the fridge.");

                    //if current fridge ingredient is exactly the same as the remaining amount, remove ingredient and set remaining amount to 0
                    } else if (currentFridgeIngredientAmount.subtract(remainingAmount).getQuantity() == 0) {
                        fridge.getIngredientList().remove(ingredientIndex);
                        ingredientRepository.delete(fridgeIngredient);
                        //remaining amount is now 0
                        remainingAmount.subtract(remainingAmount);
                        logger.info("Ingredient removed from the fridge");

                    // if current fridge ingredient is not enough for remaining amount,
                    // remove ingredient and update remaining amount to the remainder and multiple with -1 to get positive number
                    } else {
                        remainingAmount = currentFridgeIngredientAmount.subtract(remainingAmount).multiply(-1);
                        fridge.getIngredientList().remove(ingredientIndex);
                        ingredientRepository.delete(fridgeIngredient);
                        logger.info("Ingredient removed from the fridge");
                    }
                }
            }
        }

        fridgeRepository.save(fridge);
    }


}
