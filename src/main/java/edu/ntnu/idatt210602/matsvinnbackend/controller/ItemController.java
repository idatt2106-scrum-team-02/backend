package edu.ntnu.idatt210602.matsvinnbackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.ntnu.idatt210602.matsvinnbackend.model.Item;
import edu.ntnu.idatt210602.matsvinnbackend.repo.ItemRepository;
import jakarta.websocket.server.PathParam;

/**
 * Class ItemController
 * This class contains all the endpoint handlers for the '/item' API
 */
@CrossOrigin
@RestController
@RequestMapping(path = "/item")
public class ItemController {

    @Autowired
    ItemRepository itemRepo;

    /**
     * Checks the database for an item with the given ID and returns it if it
     * exists, throws a ResponseStatusException with HTTP 404 otherwise
     * 
     * @param id - ID of the item being requested
     * @return the requested item, if it exists
     */
    @GetMapping("/{id}")
    @ResponseBody
    Item getItem(@PathParam("id") Integer id) {
        return itemRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /**
     * Retrieves a paginated list of all items in the database
     * 
     * @param page    - the page number to retrieve (first page by default)
     * @param perPage - number of results per page (defaults to 15)
     * @return a page with items
     */
    @GetMapping("/all")
    @ResponseBody
    Page<Item> getAll(@RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "15") Integer perPage) {
        return itemRepo.findAll(Pageable.ofSize(perPage).withPage(page));
    }

    /**
     * Performs a search for items with a given name and returns paginated results
     * 
     * @param name    - the name to search for
     * @param page    - the page number to retreive
     * @param perPage - number of results per page (defaults to 15)
     * @return a page with the results
     */
    @GetMapping("/search")
    @ResponseBody
    Page<Item> search(@RequestParam String name, @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "15") Integer perPage) {
        return itemRepo.searchByName(name, Pageable.ofSize(perPage).withPage(page));
    }
}
