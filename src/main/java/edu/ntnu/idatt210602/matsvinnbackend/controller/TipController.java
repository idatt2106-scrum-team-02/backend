package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.Tip;
import edu.ntnu.idatt210602.matsvinnbackend.repo.TipRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Random;

/**
 * Class TipController
 * This class contains all the endpoint handlers for the '/tip' API
 */
@Controller
@CrossOrigin
@RequestMapping(path = "/tip")
public class TipController {

    @Autowired
    TipRepository tipRepository;

    /**
     * This method returns a random tip from the database
     *
     * @return Random tip from the database
     */
    @GetMapping("/random")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody Tip getRandomTip(){
        List<Tip> tips = tipRepository.findAll();

        Random r = new Random();
        try {
            return tips.get(r.nextInt(0, tips.size()));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
