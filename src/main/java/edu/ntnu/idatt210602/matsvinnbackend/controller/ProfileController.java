package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.Account;
import edu.ntnu.idatt210602.matsvinnbackend.model.AddProfileRequest;
import edu.ntnu.idatt210602.matsvinnbackend.model.Profile;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import edu.ntnu.idatt210602.matsvinnbackend.repo.ProfileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;
import java.util.Objects;

/**
 * ProfileController
 *
 * This class contains all endpoint handlers for the `/profile` API,
 * including creation, updating, fetching and deleting of profiles
 */
@Controller
@CrossOrigin
@RequestMapping(path = "/profile")
public class ProfileController {
    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private AccountRepository accountRepository;

    Logger logger = LoggerFactory.getLogger(ProfileController.class);

    /**
     * Add new profile
     * This endpoint expects profile data to be supplied as a JSON object
     * in the request body
     *
     * Possible responses are:
     * HTTP 201 Created - profile created
     * HTTP 409 Conflict - when another profile on the same account has the same name
     *
     * @param request AddProfileRequest
     */
    @PostMapping(path = "")
    @ResponseStatus(code = HttpStatus.CREATED)
    public @ResponseBody Profile addProfile(
            @RequestBody AddProfileRequest request) {
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        logger.info("Request to add a profile with data: "+request);

        if (profileRepository.existsByName(request.name())) {
            logger.warn("There exists other profile with the same name");
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }

        Profile newProfile = new Profile(
                request.name(),
                request.profileImageUrl(),
                request.isRestricted(),
                loggedInAccount.getId());

        return profileRepository.save(newProfile);
    }

    /**
     * Update profile
     *
     * Updates profile's name, picture or/and restriction.
     *
     * Possible responses are:
     * HTTP 200 OK - profile updated
     * HTTP 403 Forbidden - authentication error
     * HTTP 400 Bad Request - when the name of the profile is already taken or restriction cannot be changed
     * @param profileId the id of the profile
     */
    @PutMapping("/{profileId}")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody Profile updateProfile(@PathVariable int profileId, @RequestBody AddProfileRequest request) {
        logger.info("Request to update profile with id: "+profileId);
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        Profile profile = profileRepository.findById(profileId).orElseThrow();

        //Logged in account should be the owner of the profile that will be updated
        if (loggedInAccount.getId() != profile.getAccountId()) {
            logger.warn("Account " + loggedInAccount.getId() + " made illegal attempt to update profile " + profileId);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        if (Objects.nonNull(request.name())) {
            List<Profile> listOfProfiles = profileRepository.findAllByAccountId(loggedInAccount.getId()).stream().filter(p -> Objects.equals(p.getName(), request.name())).toList();

            if (!listOfProfiles.isEmpty()) {
                logger.warn("Another profile on the current account with the same name exists.");
                throw new ResponseStatusException(HttpStatus.CONFLICT);
            } else {
                profile.setName(request.name());
            }
        }

        if (Objects.nonNull(request.profileImageUrl())) {
            profile.setProfileImageUrl(request.profileImageUrl());
        }

        if (Objects.nonNull(request.isRestricted())) {
            if (!request.isRestricted()) {
                profile.setRestricted(false);
            } else {
                //Find all unrestricted profiles
                List<Profile> unrestrictedProfiles = profileRepository.findAllByAccountId(loggedInAccount.getId()).stream().filter(p -> Objects.equals(p.isRestricted(), false)).toList();
                //There is only one unrestricted profile left and current profile is this profile
                if (unrestrictedProfiles.size() == 1 && (Objects.equals(unrestrictedProfiles.get(0).getId(), profile.getId()))) {
                    logger.warn("Cannot change this profile to restricted when there are no other unrestricted profiles left.");
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                } else {
                    profile.setRestricted(true);
                }
            }
        }

        return profileRepository.save(profile);
    }

    /**
     * Delete profile
     *
     * Possible responses are:
     * HTTP 200 OK - Profile deleted
     * HTTP 403 Forbidden - authentication error
     * HTTP 400 Bad Request - profile restriction problem
     * @param profileId the id of the profile
     */
    @DeleteMapping(path = "/{profileId}")
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteProfile(@PathVariable int profileId) {
        logger.info("Request to delete profile with id: "+profileId);
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        Profile profile = profileRepository.findById(profileId).orElseThrow();

        //Logged in account should be the owner of the profile that will be deleted
        if (loggedInAccount.getId() != profile.getAccountId()) {
            logger.warn("Account " + loggedInAccount.getId() + " made illegal attempt to remove profile " + profileId);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        //If this is restricted profile it can be safely deleted
        if (profile.isRestricted()) {
            profileRepository.deleteById(profile.getId());
            logger.info("Deleted profile with id "+profileId);

            //If this is unrestricted profile we have to check if there are others so there is always at least one per account
        } else {
            //Find all unrestricted profiles
            List<Profile> unrestrictedProfiles = profileRepository.findAllByAccountId(loggedInAccount.getId()).stream().filter(p -> Objects.equals(p.isRestricted(), false)).toList();
            //There is only one unrestricted profile left and profile to be deleted is this profile
            if (unrestrictedProfiles.size() == 1 && (Objects.equals(unrestrictedProfiles.get(0).getId(), profile.getId()))) {
                logger.warn("Cannot delete this profile when there are no other unrestricted profiles left.");
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            } else {
                profileRepository.deleteById(profile.getId());
                logger.info("Deleted profile with id "+profileId);
            }
        }


    }

    /**
     * Fetch a profile
     *
     * This endpoint responds with the info of the profile
     *
     * Possible responses are:
     * HTTP 200 OK - profile fetched
     * HTTP 403 Forbidden - if the account of this profile doesn't match the current logged in account
     * HTTP 404 Not Found - if no such profile exists
     *
     * @param profileId - ID of the profile to fetch
     * @return the requested profile
     */
    @GetMapping(path = "/{profileId}")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody Profile getProfile(@PathVariable Integer profileId) {
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        Profile profile = profileRepository.findById(profileId).orElseThrow();

        if (loggedInAccount.getId() != profile.getAccountId()) {
            logger.warn("Account " + loggedInAccount.getId() + " made illegal attempt to get profile " + profileId);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        logger.info("Getting profile with id " + profileId);
        return profile;
    }

    /**
     * Fetch all profiles
     *
     * This endpoint responds with all profiles with a specific accountId
     * Possible responses:
     * HTTP 200 OK - fetched all profiles
     * HTTP 403 Forbidden - if the account of this profile doesn't match the current logged in account
     *
     * @return list with all profiles from current account
     */
    @GetMapping(path = "")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody List<Profile> getProfiles() {
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        logger.info("Getting all existing profiles from current account");

        //There should be at least 1 standard profile existing from when the account was made
        return profileRepository.findAllByAccountId(loggedInAccount.getId());
    }


}
