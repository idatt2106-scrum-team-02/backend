package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.Account;
import edu.ntnu.idatt210602.matsvinnbackend.model.Ingredient;
import edu.ntnu.idatt210602.matsvinnbackend.model.ShoppingList;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import edu.ntnu.idatt210602.matsvinnbackend.service.ShoppingListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * Class ShoppingList responsible for processing incoming REST API requests related to shopping list
 */
@RequestMapping("/shoppinglist")
@RestController
@CrossOrigin
public class ShoppingListController {

    Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    ShoppingListService shoppingListService;

    /**
     * Endpoint to get the shopping list for the logged in account
     * @return the shopping list
     */
    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("")
    public @ResponseBody ShoppingList getShoppingList() {
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();

        try {
            return shoppingListService.getShoppingList(loggedInAccount);
        } catch (IllegalStateException e) {
            logger.error("An Account is missing a shopping list");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Endpoint to add an item to the shopping list of the logged in account
     * @param itemId the id of the item to add
     * @param Quantity the quantity to add
     * @return The ingredient added
     */
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/items/{itemId}/{Quantity}")
    public @ResponseBody Ingredient addItem(@PathVariable("itemId") Integer itemId, @PathVariable("Quantity") Integer Quantity) {

        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        try {
            return shoppingListService.addItem(itemId, Quantity, loggedInAccount);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Item with given ean was not found");
        } catch (IllegalStateException e) {
            logger.error("An Account is missing a shopping list");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Endpoint to add an item to the shopping list of the logged in account
     * @param itemId the id of the item to add
     * @param Quantity the quantity to add
     * @return The ingredient added
     */
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/suggestions/{itemId}/{Quantity}")
    public @ResponseBody
    Ingredient addSuggestion(@PathVariable("itemId") Integer itemId, @PathVariable("Quantity") Integer Quantity) {

        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        try {
            return shoppingListService.addSuggestion(itemId, Quantity, loggedInAccount);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Item with given ean was not found");
        } catch (IllegalStateException e) {
            logger.error("An Account is missing a shopping list");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * EndPoint to remove an ingredient from the shopping list
     * @param ingredientId the id of the ingredient from the shopping list to remove
     * @return The ingredient that was removed
     */
    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/items/{ingredientId}")
    public @ResponseBody Ingredient removeIngredient(@PathVariable("ingredientId") Integer ingredientId) {

        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();

        try {
            return shoppingListService.removeItem(ingredientId, loggedInAccount);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalStateException e) {
            logger.error("An Account is missing a shopping list");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Endpoint to deny (remove) a suggestion from the suggestion list
     * @param ingredientId the id of the ingredient from the suggestion list to deny
     * @return The Ingredient that was removed
     */
    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/suggestions/{ingredientId}")
    public @ResponseBody Ingredient denySuggestion(@PathVariable("ingredientId") Integer ingredientId) {

        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();

        try {
            return shoppingListService.removeSuggestion(ingredientId, loggedInAccount);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalStateException e) {
            logger.error("An Account is missing a shopping list");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Method to accept a suggestion which removes the ingredient from the suggestion list and adds it to the shopping
     * list
     * @param ingredientId the id of the ingredient to add
     * @return The Ingredient that was accepted
     */
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/suggestions/accept/{ingredientId}")
    public @ResponseBody Ingredient acceptSuggestion(@PathVariable("ingredientId") Integer ingredientId) {

        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();

        try {
            return shoppingListService.acceptSuggestion(ingredientId, loggedInAccount);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalStateException e) {
            logger.error("An Account is missing a shopping list");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Method to change the amount of an item ingredient in the shopping list
     * @param ingredientId the id of the ingredient to change the amount of
     * @param quantity the new quantity
     * @return the updated shopping list
     */
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/items/amount/{ingredientId}/{Quantity}")
    public @ResponseBody ShoppingList changeItemAmount(@PathVariable("ingredientId") Integer ingredientId, @PathVariable("Quantity") Integer quantity) {

        logger.info("Request to adjust ingredient amount");

        if (quantity < 0) {
            logger.warn("Quantity cannot be set to less than 0.");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        try {
            return shoppingListService.changeItemAmount(ingredientId, quantity, loggedInAccount);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalStateException e) {
            logger.error("An Account is missing a shopping list");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Method to change the amount of a suggested ingredient in the suggestion list
     * @param ingredientId the id of the ingredient to change the amount of
     * @param quantity the new quantity
     * @return the updated shopping list
     */
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/suggestions/amount/{ingredientId}/{Quantity}")
    public @ResponseBody ShoppingList changeSuggestionAmount(@PathVariable("ingredientId") Integer ingredientId, @PathVariable("Quantity") Integer quantity) {

        logger.info("Request to adjust ingredient amount");

        if (quantity < 0) {
            logger.warn("Quantity cannot be set to less than 0.");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();
        try {
            return shoppingListService.changeSuggestionAmount(ingredientId, quantity, loggedInAccount);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalStateException e) {
            logger.error("An Account is missing a shopping list");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Method to buy a list of ingredients from the shopping list, putting them in fridge
     * @param ingredients a list of ingredients
     * @return the updated shopping list
     */
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/purchased")
    public @ResponseBody ShoppingList buyIngredients(@RequestBody List<Ingredient> ingredients) {

        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();

        try {
            return shoppingListService.buyAll(ingredients, loggedInAccount);
        } catch ( IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalStateException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Method to add all items from a recipe
     * @param recipeId the id of the recipe to add all items for
     * @return a shopping list object
     */
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/recipe/{recipeId}")
    public @ResponseBody ShoppingList addItemsFromRecipe(@PathVariable("recipeId") Integer recipeId) {

        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();

        try {
            return shoppingListService.addAllItemsFromRecipe(recipeId, loggedInAccount, 1);
        } catch ( IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalStateException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
