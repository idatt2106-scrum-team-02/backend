package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.Recipe;
import edu.ntnu.idatt210602.matsvinnbackend.repo.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
@CrossOrigin
@RequestMapping(path = "/recipe")
public class RecipeController {
    //TODO: logging?

    @Autowired
    private RecipeRepository recipeRepo;

    /**
     * Get all recipes
     *
     * This endpoint responds with a list
     * of all recipes in the recipe database
     * throws 404 exception if none found
     * @return the list of all recipes
     */
    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody List<Recipe> getRecipes(){
        return recipeRepo.findAll();
    }

    /**
     * Gets single recipe by given id
     *
     * This endpoint responds with a recipe
     *
     * throws 404 exception if recipe found
     *
     * @param id - Recipe identification
     * @return recipe with given id
     */
    @GetMapping("/{id}")
    public @ResponseBody Recipe getRecipe(@PathVariable Integer id){ //Potential sec. risk, should recipe id be public?
        return recipeRepo.findById(id).orElseThrow(() -> {
            return new ResponseStatusException(HttpStatus.NOT_FOUND);       //.orElseThrow throws exception
                                                                            //returned by lambda
        });
    }


}
