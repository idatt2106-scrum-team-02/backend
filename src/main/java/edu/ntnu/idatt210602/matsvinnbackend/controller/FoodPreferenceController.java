package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.FoodPreference;
import edu.ntnu.idatt210602.matsvinnbackend.service.FoodPreferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Class FoodPreferenceController responsible for processing incoming REST API requests related to food preferences
 */
@RequestMapping("/foodpreferences")
@RestController
@CrossOrigin
public class FoodPreferenceController {

    @Autowired
    FoodPreferenceService foodPreferenceService;

    /**
     * Api endpoint to get all existing food preference types
     * @return All foodPreferences
     */
    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public List<FoodPreference> getAllFoodPreferences() {
        return foodPreferenceService.getAllFoodPreferences();
    }
}
