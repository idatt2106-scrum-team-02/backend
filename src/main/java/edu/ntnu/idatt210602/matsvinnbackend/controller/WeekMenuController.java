package edu.ntnu.idatt210602.matsvinnbackend.controller;

import edu.ntnu.idatt210602.matsvinnbackend.model.*;
import edu.ntnu.idatt210602.matsvinnbackend.repo.AccountRepository;
import edu.ntnu.idatt210602.matsvinnbackend.service.WeekMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * Class WeekMenuController
 * This class contains all the endpoint handlers for the '/weekmenu' API
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/weekmenu")
public class WeekMenuController {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    WeekMenuService weekMenuService;

    Logger logger = LoggerFactory.getLogger(WeekMenuController.class);

    /**
     * Endpoint to generate a new week menu
     * @param numberOfPeople the number of people to generate for
     * @return a week menu
     */
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/{numberOfPeople}")
    public @ResponseBody
    WeekMenu generateWeekMenu(@PathVariable("numberOfPeople") Integer numberOfPeople){

        logger.info("Request to generate a week menu");
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();

        if (numberOfPeople < 1 || numberOfPeople > 999) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        return weekMenuService.generateWeekMenu(loggedInAccount, numberOfPeople);
    }

    /**
     * Method to get the stored week menu for an account
     * @return a week menu
     */
    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("")
    public @ResponseBody WeekMenu getWeekMenu() {

        logger.info("Request to fetch a week menu");
        String authenticatedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        Account loggedInAccount = accountRepository.findByEmail(authenticatedUsername).orElseThrow();

        return weekMenuService.getWeekMenu(loggedInAccount);
    }
}
