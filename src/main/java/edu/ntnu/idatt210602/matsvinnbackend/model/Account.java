package edu.ntnu.idatt210602.matsvinnbackend.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import jakarta.persistence.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

/**
 * Account class
 * An account can have multiple profiles.
 */
@Entity
public class Account implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String firstname;

    @Getter
    @JsonIgnore
    private String password;

    @Setter
    @Getter
    @OneToOne(cascade = CascadeType.ALL)
    private ShoppingList shoppingList;

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL)
    private Fridge fridge;

    @Setter
    @Getter
    @OneToOne(cascade = CascadeType.ALL)
    private WeekMenu weekMenu;


    public Account() {
    }

    /**
     * Constructs an account object with the given data,
     *
     * @param firstname the first name for the account
     * @param email the email for the account
     * @param password the password for the account
     */
    public Account(String firstname, String email, String password) throws IllegalArgumentException {
        this.firstname = firstname;
        this.setEmail(email);
        this.setPassword(password);
        this.fridge = new Fridge();
        this.shoppingList = new ShoppingList();
        this.weekMenu = new WeekMenu();
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<>();
    }

    @Override
    public String getUsername() {
        return email;
    }

    /**
     * We do not expire accounts
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * We do not support locked accounts at this time
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * User credentials do not expire
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * We do not support disabling accounts at this time
     */
    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
