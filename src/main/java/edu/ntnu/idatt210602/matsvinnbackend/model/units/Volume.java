package edu.ntnu.idatt210602.matsvinnbackend.model.units;

public enum Volume implements Unit {

    MILLILITER {
        @Override
        public <T extends Unit> double getRatio(T unit) {
            if (!(unit instanceof Volume)) {
                throw new IllegalArgumentException("Can't convert from volume to mass or unitless size");
            }

            if (((Volume) unit).equals(Volume.MILLILITER)) {
                return 1;
            } else if (((Volume) unit).equals(Volume.DESILITER)) {
                return 0.01;
            } else {
                return 0.001;
            }
        }
    },
    DESILITER {
        @Override
        public <T extends Unit> double getRatio(T unit) {
            if (!(unit instanceof Volume)) {
                throw new IllegalArgumentException("Can't convert from volume to mass or unitless size");
            }

            if (((Volume) unit).equals(Volume.MILLILITER)) {
                return 100;
            } else if (((Volume) unit).equals(Volume.DESILITER)) {
                return 1;
            } else {
                return 0.1;
            }
        }
    },
    LITER {
        @Override
        public <T extends Unit> double getRatio(T unit) {
            if (!(unit instanceof Volume)) {
                throw new IllegalArgumentException("Can't convert from volume to mass or unitless size");
            }

            if (((Volume) unit).equals(Volume.MILLILITER)) {
                return 1000;
            } else if (((Volume) unit).equals(Volume.DESILITER)) {
                return 10;
            } else {
                return 1;
            }
        }
    };
}
