package edu.ntnu.idatt210602.matsvinnbackend.model;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Class Log to logg one action in fridge
 */
@Entity
public class Log {
    public enum Action {
        CONSUMED,
        DISCARDED,
        ADDED_TO_FRIDGE
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter @Setter Integer log_id;

    @Getter @Setter Date date;

    @Getter @Setter Action action;

    @ManyToOne(cascade = CascadeType.ALL)
    //@JoinColumn(name = "ingredient_id")
    @Getter @Setter Ingredient ingredient;


    @ManyToOne
    @JoinColumn(name = "id")
    @Getter @Setter Account account;


    public Log(){}
    public Log(Date date, Action action, Ingredient ingredient, Account account) {
        this.date = date;
        this.action = action;
        this.ingredient = ingredient;
        this.account = account;
    }
}
