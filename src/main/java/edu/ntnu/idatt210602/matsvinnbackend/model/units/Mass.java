package edu.ntnu.idatt210602.matsvinnbackend.model.units;

public enum Mass implements Unit {
    GRAM {
        @Override
        public <T extends Unit> double getRatio(T unit) {
            if (!(unit instanceof Mass)) {
                throw new IllegalArgumentException("Can't convert from mass to volume or unitless size");
            }

            if (((Mass) unit).equals(Mass.KILOGRAM)) {
                return 0.001;
            } else {
                return 1;
            }
        }
    },
    KILOGRAM {
        @Override
        public <T extends Unit> double getRatio(T unit) {
            if (!(unit instanceof Mass)) {
                throw new IllegalArgumentException("Can't convert from mass to volume or unitless size");
            }

            if (((Mass) unit).equals(Mass.GRAM)) {
                return 1000;
            } else {
                return 1;
            }
        }

    };
}
