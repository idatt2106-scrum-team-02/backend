package edu.ntnu.idatt210602.matsvinnbackend.model;

import java.util.Map;

public record UpdateIngredientsAmountInFridge(Log.Action action, Map<Integer, SerializedAmount> ingredients) {
}
