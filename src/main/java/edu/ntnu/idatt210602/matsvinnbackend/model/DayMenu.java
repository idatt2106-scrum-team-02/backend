package edu.ntnu.idatt210602.matsvinnbackend.model;

import jakarta.persistence.*;
import lombok.*;

/**
 * Class day menu to represent the menu for one day of the week menu
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class DayMenu {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Integer id;

    @Setter @Getter @ManyToOne Recipe recipe;

    @Setter @Getter String day;
}
