package edu.ntnu.idatt210602.matsvinnbackend.model;

public record AddItemToFridgeRequest(int itemId, SerializedAmount amount) {
}
