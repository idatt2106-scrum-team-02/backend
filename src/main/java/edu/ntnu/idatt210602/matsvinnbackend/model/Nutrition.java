package edu.ntnu.idatt210602.matsvinnbackend.model;

import java.io.Serializable;

import jakarta.persistence.Embeddable;
import jakarta.persistence.Embedded;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated
@Embeddable
public class Nutrition implements Serializable {
    public Nutrition(){};
    public Nutrition(String name, SerializedAmount amount) {
        this.name = name;
        this.amount = amount;
    }
    
    @Getter @Setter String name;
    @Getter @Setter @Embedded SerializedAmount amount;
}
