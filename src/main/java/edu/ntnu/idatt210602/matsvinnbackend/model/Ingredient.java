package edu.ntnu.idatt210602.matsvinnbackend.model;

import jakarta.persistence.*;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Class ingredient
 */
@Generated
@Entity
public class Ingredient implements Serializable {

    /**
     * Enumerator used to describe the status of an ingredient.
     */
    public enum Status{
        /**
         * Ingredient within shopping list
         */
        PENDING,
        /**
         * Ingredient within shopping list and needed for recipe (do not use for requests by profile requests)
         */
        REQUESTED,
        /**
         * Ingredient is in fridge
         */
        FREE,
        /**
         * Ingredient is in fridge and reserved for recipe
         */
        RESERVED

    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    public Integer ingredient_id;


    @Getter @Setter Status status;

    @Setter @Getter @Embedded SerializedAmount amount;

    @ManyToOne @Setter @Getter Item item;

    @Setter
    @Getter
    public Date exp_date;

    public Ingredient(){}

    public Ingredient(Item item, SerializedAmount amount) {
        this.item = item;
        this.amount = amount;
    }

    /**
     * Method to change status from in shopping list to in fridge
     */
    public void changeStatusFromShoppingListToFridge() {
        if (this.status == Status.PENDING) {
            this.status = Status.FREE;
            return;
        }
        if (this.status == Status.REQUESTED) {
            this.status = Status.RESERVED;
        }
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "ingredient_id=" + ingredient_id +
                ", status=" + status +
                ", amount=" + amount +
                ", item=" + item +
                ", exp_date=" + exp_date +
                '}';
    }
}
