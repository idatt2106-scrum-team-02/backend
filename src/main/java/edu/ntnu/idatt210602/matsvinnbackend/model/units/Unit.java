package edu.ntnu.idatt210602.matsvinnbackend.model.units;

import java.io.Serializable;

import jakarta.persistence.Embeddable;

@Embeddable
public interface Unit extends Serializable {
    public <T extends Unit> double getRatio(T unit);
}
