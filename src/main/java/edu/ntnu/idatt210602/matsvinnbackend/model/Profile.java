package edu.ntnu.idatt210602.matsvinnbackend.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

/**
 * Profile class
 * An account should own at least one unrestricted profile.
 * There is no limit for how many profiles an account can have, but they all should have unique names.
 * Actual restriction is provided by front-end.
 */
@Entity
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String profileImageUrl;

    @Getter
    @Setter
    private boolean isRestricted;

    @Getter
    private int accountId;

    public Profile() {

    }

    public Profile(String name, String profileImageUrl, boolean isRestricted, int accountId) {
        this.name = name;
        this.profileImageUrl = profileImageUrl;
        this.isRestricted =  isRestricted;
        this.accountId = accountId;
    }

}
