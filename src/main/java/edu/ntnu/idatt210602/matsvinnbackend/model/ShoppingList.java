package edu.ntnu.idatt210602.matsvinnbackend.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.*;

/**
 * Class ShoppingList to represent one shopping list for an account
 */
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ShoppingList {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Integer id;

    @Getter
    @Setter
    @OneToMany(cascade = CascadeType.REMOVE)
    private Set<Ingredient> ingredientList = new HashSet<>();

    @Getter
    @Setter
    @OneToMany(cascade = CascadeType.REMOVE)
    private Set<Ingredient> suggestionList = new HashSet<>();


    /**
     * Method to add an ingredient to the ingredient list of the object
     * @param ingredient the ingredient to add
     */
    public void addIngredient(Ingredient ingredient) {
        this.ingredientList.add(ingredient);
    }

    /**
     * Method to remove an ingredient from the ingredient list of the object
     * @param ingredientId the id of the ingredient to remove
     */
    public void removeIngredient(Integer ingredientId) {
        List<Ingredient> ingredientsToRemove = new ArrayList<>();
        this.ingredientList.forEach(ingredient -> {
            if (Objects.equals(ingredientId, ingredient.ingredient_id)) {
            ingredientsToRemove.add(ingredient);
            }
        });
        this.ingredientList.removeAll(ingredientsToRemove);
    }

    public void addSuggestion(Ingredient ingredient) {
        this.suggestionList.add(ingredient);
    }

    /**
     * Method to remove an ingredient from the suggestion list of the object
     * @param ingredientId the id of the ingredient to remove
     */
    public void removeSuggestion(Integer ingredientId) {
        List<Ingredient> suggestionsToRemove = new ArrayList<>();
        this.suggestionList.forEach(ingredient -> {
            if (Objects.equals(ingredientId, ingredient.ingredient_id)) {
                suggestionsToRemove.add(ingredient);
            }
        });
        this.suggestionList.removeAll(suggestionsToRemove);
    }

    /**
     * Method to check if an ingredient with a given id is in the shopping list
     * @param ingredientId the ingredient id
     * @return true or false
     */
    public boolean IsIngredientInList(Integer ingredientId) {
        return this.getIngredientList().stream().anyMatch(ingredient -> Objects.equals(ingredient.ingredient_id, ingredientId));
    }

    /**
     * Method to check if an ingredient with a given id is in the suggestions list
     * @param ingredientId the ingredient id
     * @return true or false
     */
    public boolean IsIngredientInSuggestions(Integer ingredientId) {
        return this.getSuggestionList().stream().anyMatch(ingredient -> Objects.equals(ingredient.ingredient_id, ingredientId));
    }

    /**
     * Method to get an ingredient from the list
     * @param ingredientId the ingredient to get
     * @return ingredient or null if it doesn't exist
     */
    public Ingredient getIngredientFromList(Integer ingredientId) {

        Optional<Ingredient> i = this.getIngredientList().stream().filter(ingredient -> Objects.equals(ingredient.ingredient_id, ingredientId)).findFirst();
        return i.orElse(null);
    }

    /**
     * Method to get an ingredient from the suggestions list
     * @param ingredientId the ingredient to get
     * @return ingredient or null if it doesn't exist
     */
    public Ingredient getSuggestionFromList(Integer ingredientId) {

        Optional<Ingredient> i = this.getSuggestionList().stream().filter(ingredient -> Objects.equals(ingredient.ingredient_id, ingredientId)).findFirst();
        return i.orElse(null);
    }
}
