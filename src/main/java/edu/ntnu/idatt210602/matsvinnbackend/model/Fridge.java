package edu.ntnu.idatt210602.matsvinnbackend.model;

import jakarta.persistence.*;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Fridge class
 * Stores ingredients.
 */
@Generated
@Entity
public class Fridge {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    @OneToMany(cascade = CascadeType.REMOVE)
    List<Ingredient> ingredientList;

    /**
     * Method to get all ingredients with free status
     * @return a list of all free ingredients
     */
    public List<Ingredient> getFreeIngredients(){
        List<Ingredient> retList = new ArrayList<>();
        for (Ingredient ingredient:ingredientList) {
            if (ingredient.getStatus() == Ingredient.Status.FREE){
                retList.add(ingredient);
            }
        }
        return retList;
    }

    /**
     * Method to get all ingredients with reserved status
     * @return a list of all reserved ingredients
     */
    public List<Ingredient> getReservedIngredients(){
        List<Ingredient> retList = new ArrayList<>();
        for (Ingredient ingredient:ingredientList) {
            if (ingredient.getStatus() == Ingredient.Status.RESERVED){
                retList.add(ingredient);
            }
        }
        return retList;
    }

    public Fridge() {
        ingredientList = new ArrayList<>();
    }

    public void addIngredient(Ingredient ingredient) {
        this.ingredientList.add(ingredient);
    }
}
