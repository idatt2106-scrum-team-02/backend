package edu.ntnu.idatt210602.matsvinnbackend.model;

public record UpdateAccountRequest(String password, String firstname) {}
