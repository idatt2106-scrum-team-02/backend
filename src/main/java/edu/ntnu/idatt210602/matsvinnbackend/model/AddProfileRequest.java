package edu.ntnu.idatt210602.matsvinnbackend.model;

public record AddProfileRequest(String name, String profileImageUrl, boolean isRestricted) { }
