package edu.ntnu.idatt210602.matsvinnbackend.model;

public record RemoveIngredientsRequest(int recipeId, int recipeScalar) { }
