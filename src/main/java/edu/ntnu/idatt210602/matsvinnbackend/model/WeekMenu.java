package edu.ntnu.idatt210602.matsvinnbackend.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.*;

/**
 * Model class WeekMenu to represent a week menu for an account
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class WeekMenu {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Integer id;

    @Getter
    @Setter
    @OneToMany
    private List<DayMenu> dayMenus = new ArrayList<>();

    @Getter
    @Setter
    private Integer peopleAmount;
}


