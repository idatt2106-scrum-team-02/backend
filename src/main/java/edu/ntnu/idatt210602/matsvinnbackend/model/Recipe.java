package edu.ntnu.idatt210602.matsvinnbackend.model;

import jakarta.persistence.*;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Class Recipe to represent a recipe
 */
@Generated
@Entity
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Integer id; //Unique id
    @Setter
    @Getter
    private String title; //name of recipe, f. ex. lasagna, new york pizza, etc.
    @Setter
    @Getter
    private String description; //short description of recipe
    @Setter
    @Getter
    private String time; //Estimated required time
    @Setter
    @Getter
    @OneToMany
    //@JoinColumn(name = "ingredient_id")
    private List<Ingredient> ingredient; //List with ingredients (item with count/amount and expiry date)
    @Setter
    @Getter
    private String instructions; //instructions of how to make the recipe

    public Recipe() {}

    public Recipe(String title, String description, String time, List<Ingredient> ingredient, String instructions) {
        this.title = title;
        this.description = description;
        this.time = time;
        this.ingredient = ingredient;
        this.instructions = instructions;
    }
}
