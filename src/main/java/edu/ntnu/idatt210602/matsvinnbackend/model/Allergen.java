package edu.ntnu.idatt210602.matsvinnbackend.model;

import jakarta.persistence.Embeddable;
import lombok.Getter;

@Embeddable
public class Allergen {
    public Allergen(){};
    public Allergen(String name) {
        this.name = name;
    }

    @Getter String name;
}
