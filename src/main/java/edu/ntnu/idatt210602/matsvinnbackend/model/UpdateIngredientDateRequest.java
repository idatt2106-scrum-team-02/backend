package edu.ntnu.idatt210602.matsvinnbackend.model;

import java.util.Date;

public record UpdateIngredientDateRequest(int ingredientId, Date newExpDate) {
}
