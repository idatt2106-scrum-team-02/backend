package edu.ntnu.idatt210602.matsvinnbackend.model.units;

public enum Count implements Unit {
    COUNT;
    @Override
    public <T extends Unit> double getRatio(T unit) {
        if (!(unit instanceof Count)) {
            throw new IllegalArgumentException("Unitless count can not be converted to mass or volume");
        }
        
        return 1.0;
    }
}
