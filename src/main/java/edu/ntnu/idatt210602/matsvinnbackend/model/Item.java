package edu.ntnu.idatt210602.matsvinnbackend.model;

import java.util.List;

import jakarta.persistence.*;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

/**
 * The <code>Item</code> class represents an item from a grocery store,
 * including amount, nutritional value and allergens
 */
@Entity
@Generated
public class Item {    
    public Item() {}
    public Item(String name, String ean, String image_url, Integer shelfLife, SerializedAmount amount, List<Allergen> allergens, List<Nutrition> nutrition) {
        this.allergens = allergens;
        this.ean = ean;
        this.shelfLife = shelfLife;
        this.image_url = image_url;
        this.name = name;
        this.nutrition = nutrition;
        this.amount = amount;
    }

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
            @Setter @Getter Integer id;
    @Setter @Getter String name;
    @Setter @Getter String ean;
            @Getter Integer shelfLife;
    @Setter @Getter String image_url;
            @Getter @Embedded SerializedAmount amount;
    @Setter @Getter String store;
    @ElementCollection
    @Setter @Getter @Embedded List<Allergen> allergens;
    @ElementCollection
    @Setter @Getter @Embedded List<Nutrition> nutrition;
}
