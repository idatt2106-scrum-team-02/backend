package edu.ntnu.idatt210602.matsvinnbackend.model;

import edu.ntnu.idatt210602.matsvinnbackend.model.units.Count;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Energy;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Mass;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Unit;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Volume;
import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

/**
 * The <code>SerializedAmount</code> class exists to solve a particular problem
 * I encountered with JPA.
 * 
 * The original {@link Amount} class I built relies on a generic called
 * {@link Unit}
 * which in turn has a few implementing classes that provide definitions and
 * actual useful functionality for converting between units that represent the
 * same physical dimension ({@link Mass}, {@link Volume}, {@link Energy}, etc.)
 * and performing arithmetical operations with them while conserving units along
 * the way. For example, you could add 1 kilogram and 500 grams and get 1.5
 * kilograms or 1500 grams back, or divide 1 litre by 1 desilitre and get 10,
 * the number of desiliters in a liter.
 * 
 * This Unit type was used in Amount, which was again used in classes like Item,
 * Ingredient and Nutrition where representing an amount of a thing was
 * relevant. However, using this Unit type was causing JPA to get all sorts of
 * confused, and it was unable to create a proper database schema for the system
 * when using a generic Unit interface as described above.
 * 
 * This class is my solution to this issue. After putting effort into making JPA
 * and my fancy generic Unit be friends, I gave up and created SerializedAmount.
 * SerializedAmount is a sibling to the Amount class, but the generic Unit
 * member has been replaced with a plain String. This plain String slots easily
 * into a database, solving the JPA issues described. There is to be a 1:1
 * mapping between the String representations in this class and the variants of
 * all implementers of the Unit interface so that the <code>deserialize()</code>
 * method can always be called to create an {@link Amount} which allows safe
 * manipulations with preservation of units and safe conversions between
 * compatible units.
 */
@Embeddable
public class SerializedAmount {
    @Getter
    @Setter
    double quantity;
    @Getter
    @Setter
    String unit;

    public SerializedAmount() {
    };

    public SerializedAmount(double quantity, String unit) {
        this.quantity = quantity;
        this.unit = unit;
    }

    /**
     * Assuming a valid SerializedAmount, this method creates a corresponding
     * Amount, where the units are represented by an implementer of the {@link Unit}
     * interface which allows for safe arithmetics and conversions between units
     * 
     * @return an Amount object, equivalent to the SerializedAmount instance this
     *         method was called on
     */
    public Amount deserialize() {
        Unit deserializedUnit;

        switch (unit) {
            case "g":
                deserializedUnit = Mass.GRAM;
                break;

            case "kg":
                deserializedUnit = Mass.KILOGRAM;
                break;

            case "ml":
                deserializedUnit = Volume.MILLILITER;
                break;

            case "dl":
                deserializedUnit = Volume.DESILITER;
                break;

            case "l":
                deserializedUnit = Volume.LITER;
                break;

            case "stk":
                deserializedUnit = Count.COUNT;
                break;

            case "kcal":
                deserializedUnit = Energy.KCAL;
                break;

            case "kj":
                deserializedUnit = Energy.KILOJOULE;
                break;

            default:
                throw new IllegalArgumentException("Unknown unit");
        }

        return new Amount(quantity, deserializedUnit);
    }
}
