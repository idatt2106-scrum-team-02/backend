package edu.ntnu.idatt210602.matsvinnbackend.model;

public record AddAccountRequest(
        String email,
        String password,
        String firstname) {}
