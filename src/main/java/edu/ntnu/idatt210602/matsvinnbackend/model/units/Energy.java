package edu.ntnu.idatt210602.matsvinnbackend.model.units;

public enum Energy implements Unit {
    KCAL {
        @Override
        public <T extends Unit> double getRatio(T unit) {
            if (!(unit instanceof Energy)) {
                throw new IllegalArgumentException("Can't convert from energy to mass, volume or unitless size");
            }

            if (((Energy) unit).equals(Energy.KILOJOULE)) {
                return 4.184;
            } else {
                return 1;
            }
        }
    },
    KILOJOULE {
        @Override
        public <T extends Unit> double getRatio(T unit) {
            if (!(unit instanceof Energy)) {
                throw new IllegalArgumentException("Can't convert from energy to mass, volume or unitless size");
            }

            if (((Energy) unit).equals(Energy.KCAL)) {
                return 0.239;
            } else {
                return 1;
            }
        }

    };
    
}
