package edu.ntnu.idatt210602.matsvinnbackend.model;

public record LoginRequest(String email, String password) {}
