package edu.ntnu.idatt210602.matsvinnbackend.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class FoodPreferences to represent a food preference e.g. vegan.
 */
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class FoodPreference {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Integer id;

    @Getter
    @Setter
    private String type;
}
