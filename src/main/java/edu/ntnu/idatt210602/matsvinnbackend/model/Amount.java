package edu.ntnu.idatt210602.matsvinnbackend.model;

import java.util.Objects;

import edu.ntnu.idatt210602.matsvinnbackend.model.units.Count;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Energy;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Mass;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Unit;
import edu.ntnu.idatt210602.matsvinnbackend.model.units.Volume;
import lombok.Getter;

/**
 * The <code>Amount</code> class represents amounts with scalar values and
 * units, like "1 meter" or "500 grams". Additionally, it contains methods to
 * convert amounts between relevant units, and perform basic mathematical
 * operations while accounting for the different units involved. For example, 1
 * liter can be converted to 1000 milliliters but not to any amount of
 * kilograms, and 1 meter minus 11 centimeters gives 0.89 meters.
 */
public class Amount {
    @Getter
    double quantity;

    @Getter
    Unit unit;

    public Amount() {
    }

    /**
     * Creates a SerializedAmount equivalent to the Amount object the method is
     * called on
     * 
     * @return - a SerializedAmount with the same quantity as the Amount, and a
     *         String representation of the units
     */
    public SerializedAmount serialize() {
        String serializedUnit;

        if (unit == Mass.GRAM) {
            serializedUnit = "g";
        } else if (unit == Mass.KILOGRAM) {
            serializedUnit = "kg";
        } else if (unit == Volume.MILLILITER) {
            serializedUnit = "ml";
        } else if (unit == Volume.DESILITER) {
            serializedUnit = "dl";
        } else if (unit == Volume.LITER) {
            serializedUnit = "l";
        } else if (unit == Energy.KCAL) {
            serializedUnit = "kcal";
        } else if (unit == Energy.KILOJOULE) {
            serializedUnit = "kj";
        } else if (unit == Count.COUNT) {
            serializedUnit = "stk";
        } else {
            throw new IllegalArgumentException();
        }

        return new SerializedAmount(this.quantity, serializedUnit);
    }

    /**
     * Basic constructor, copies the quantity and parameter into the constructed
     * object
     * 
     * @param quantity - numeric value of the Amount
     * @param unit     - the {@link Unit} of the Amount (may not be null)
     */
    public Amount(double quantity, Unit unit) {
        this.quantity = quantity;
        this.unit = Objects.requireNonNull(unit);
    }

    /**
     * Converts an Amount from one unit to another (e.g. grams to kilograms)
     * 
     * @param newUnit - the Unit to convert to
     * @return the converted Amount if possible
     */
    public Amount convertTo(Unit newUnit) {
        double ratio = unit.getRatio(newUnit);

        return new Amount(this.quantity * ratio, newUnit);
    }

    /**
     * Divides one amount by another, returning a scalar value
     * 
     * @param divisor - the Amount to divide by
     * @return the ratio between the amounts
     */
    public double divideBy(Amount divisor) {
        Amount convertedDivisor = divisor.convertTo(unit);
        return this.quantity / convertedDivisor.quantity;
    }

    /**
     * Divides the amount by a scalar value, returning a new amount
     * 
     * @param divisor - the number to divide by
     * @return the resulting Amount
     */
    public Amount divideBy(double divisor) {
        return new Amount(quantity / divisor, unit);
    }

    /**
     * Adds two amounts together, returning the sum
     * The returned Amount will have the same Unit as the Amount the method was
     * called from
     * 
     * @param addend - the Amount to add
     * @return the sum of the two Amounts
     */
    public Amount add(Amount addend) {
        Amount convertedAddend = addend.convertTo(unit);
        return new Amount(this.quantity + convertedAddend.quantity, unit);
    }

    /**
     * Subtracts an amount from another, returning the result
     * The returned Amount will have the same Unit as the Amount the method was
     * called from
     * 
     * @param subtrahend - the Amount to subtract
     * @return the difference between the two Amounts
     */
    public Amount subtract(Amount subtrahend) {
        Amount convertedSubtrahend = subtrahend.convertTo(unit);
        return new Amount(this.quantity - convertedSubtrahend.quantity, unit);
    }

    public Amount multiply(double multiplier) {
        return new Amount(quantity * multiplier, unit);
    }
}
