package edu.ntnu.idatt210602.matsvinnbackend.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Tip {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    @Setter
    @Getter
    String content;

    public Tip(String content){
        this.content = content;
    }

    public Tip(){}
}
