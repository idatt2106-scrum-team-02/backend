package edu.ntnu.idatt210602.matsvinnbackend.repo;

import edu.ntnu.idatt210602.matsvinnbackend.model.WeekMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface week menu repository responsible for persistence of account week menus
 */
@Repository
public interface WeekMenuRepository extends JpaRepository<WeekMenu,Integer> {

}
