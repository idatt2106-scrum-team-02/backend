package edu.ntnu.idatt210602.matsvinnbackend.repo;

import edu.ntnu.idatt210602.matsvinnbackend.model.ShoppingList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface ShoppingListRepository to provide storage and persistence for ShoppingList objects
 */
@Repository
public interface ShoppingListRepository extends JpaRepository<ShoppingList, Integer> {

}
