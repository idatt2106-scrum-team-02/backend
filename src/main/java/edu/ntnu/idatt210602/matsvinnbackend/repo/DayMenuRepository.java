package edu.ntnu.idatt210602.matsvinnbackend.repo;

import edu.ntnu.idatt210602.matsvinnbackend.model.DayMenu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DayMenuRepository extends JpaRepository<DayMenu, Integer> {

}
