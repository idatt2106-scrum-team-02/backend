package edu.ntnu.idatt210602.matsvinnbackend.repo;

import edu.ntnu.idatt210602.matsvinnbackend.model.Fridge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FridgeRepository extends JpaRepository<Fridge, Integer> {

}
