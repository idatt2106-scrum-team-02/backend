package edu.ntnu.idatt210602.matsvinnbackend.repo;

import edu.ntnu.idatt210602.matsvinnbackend.model.FoodPreference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface FoodPreferencesRepository to provide mechanisms for several operations on the FoodPreferences class
 */
@Repository
public interface FoodPreferencesRepository extends JpaRepository<FoodPreference, Integer> {

}
