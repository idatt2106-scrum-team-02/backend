package edu.ntnu.idatt210602.matsvinnbackend.repo;

import edu.ntnu.idatt210602.matsvinnbackend.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Integer> {

}
