package edu.ntnu.idatt210602.matsvinnbackend.repo;

import org.springframework.stereotype.Repository;

import edu.ntnu.idatt210602.matsvinnbackend.model.Item;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {
    @Query("select i from Item i where i.name like '%' || :name || '%'")
    Page<Item> searchByName(@Param("name") String name, Pageable pageable);

    Item findByName(String name);
}
