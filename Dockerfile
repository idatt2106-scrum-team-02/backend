FROM maven:3 AS backend-builder
WORKDIR /build
COPY . /build
RUN mvn package -Dmaven.test.skip

FROM openjdk:17-alpine as backend-runner
COPY --from=backend-builder /build/target/matsvinn-backend-0.0.1-SNAPSHOT.jar app.jar
ENV MYSQL_HOST=db
ENTRYPOINT [ "java", "-jar", "app.jar" ]